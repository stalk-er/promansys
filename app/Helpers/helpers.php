<?php

function generate_status_class($status) {

    if ($status == "urgent")
        return "label label-warning";
    if ($status == "immediate")
        return "label label-danger";
    if ($status == "high")
        return "label label-success";
    if ($status == "low")
        return "label label-default";
    if ($status == "normal")
        return "label label-primary";
}

function getPriority($id) {

    return DB::table('priorities')->where('id', $id)->first()->name;
}

function getStatus($id) {

    return DB::table('statuses')->where('id', $id)->first()->name;
}

function getAuthorAssignee($id) {
    if ($id)
        return DB::table('users')->where('id', $id)->first()->name;
}

function isActive($url) {
    $active = "";
    if (Request::is($url))
        $active = "class='active'";
    return $active;
}

function timeSince($time) {

    $time = date_format(date_create($time), 'Y-m-d H:i:s');
    $time = strtotime($time);

    $time = time() - $time; // to get the time since that moment
    $time = ($time < 1) ? 1 : $time;
    $tokens = array(
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {

        if ($time < $unit)
            continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
    }
}
