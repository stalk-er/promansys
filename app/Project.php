<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Project
 *
 * @author Mimas
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Project extends Model {

    protected $table = 'projects';
    protected $fillable = ['name', 'description', 'slug'];

    public function tasks() {

      $query = \App\Task::select(DB::raw('*'))
      ->where('project_id', $this->id)
      ->where(
        DB::raw('(uid, revision) IN ( SELECT DISTINCT uid, MAX(revision) FROM tasks GROUP BY uid )')
      , true);
      return $query;
    }

    public function addTask() {

        return $this->tasks();
    }

    public function members() {

        return $this->belongsToMany('App\User');
    }

    public function addMember($user) {

        return (!$this->members()->get()->contains($user)) ? $this->members()->attach($user) : true;
    }

    public function removeMember($user) {

        return ($this->members()->get()->contains($user)) ? $this->members()->detach($user) : false;
    }


    public function isMember($user) {

        return $this->members()->get()->contains($user);
    }

    public function wikiPage() {

        return $this->hasOne('\App\Wiki');
    }

}
