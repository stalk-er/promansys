<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

require base_path() . '/app/Helpers/helpers.php';

class HelperServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
        
        
        $this->allProjects();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    public function allProjects() {
        return view()->composer("master", function($view) {
                    $view->with('all_projects', \App\Project::all());
                });
    }

}
