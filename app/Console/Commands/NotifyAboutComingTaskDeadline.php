<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use DB;

class NotifyAboutComingTaskDeadline extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasksdeadline:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command sends emails to the asignee of a ticket which deadline is close';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $tasksWithADeadline = \App\Task::whereNotNull("deadline");
        $statusClosed = 5; // TODO: Refactor

        $tasksWithADeadline = \App\Task::select(DB::raw('*'))
                        ->whereNotNull("deadline")
                        ->where('status', '!=', $statusClosed)
                        ->where('sent_reminder', '=', 0)
                        ->where(
                                DB::raw('(uid, revision) IN ( SELECT DISTINCT uid, MAX(revision) FROM tasks GROUP BY uid )')
                                , true)->with('project')->orderBy('priority', 'DESC');

        if ($tasksWithADeadline->exists()) {
            foreach ($tasksWithADeadline->get() as $task) {

                if ($task->sent_reminder == 1) {
                    continue;
                }
	
		if($task->getAssignee()->first()->email == config('mail.username')) {
			continue; // TODO: remove after setting up the Linode mail server
		}

                Mail::send('emails.task-deadline-reminder', [
                    "task" => $task
                        ], function($message) use($task) {
                    $message
                            ->to($task->getAssignee()->first()->email, $task->getAssignee()->first()->name)
                            ->subject('Task #' . $task->uid . ' deadline reminder');
                });

                $task->sent_reminder = 1;
                $task->save();
            }
        }
    }

    public function fire() {
        // the command has been triggered
    }

    protected function getArguments() {
//        return [
//                // ['example', InputArgument::REQUIRED, 'An example argument.'],
//        ];
    }

    protected function getOptions() {
        return [
                //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
