<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Task
 *
 * @author Mimas
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $table = 'tasks';
    protected $fillable = ['type', 'subject', 'description', 'status', 'priority', 'type', 'created_by', 'updated_by', 'assigned_to', 'project_id'];

    public function project() {

        return $this->belongsTo('App\Project');
    }

    public function assigned_to() {

        return $this->hasOne('App\User', 'foreign_key');
    }

    public function comments() {

        return $this->belongsToMany('\App\Comment', 'task_comment', 'task_id', 'comment_id');
    }

    public function attachments() {

        return $this->belongsToMany('App\Attachment', 'task_attachment');
    }

    public function getStatus() {

        return $this->belongsTo('\App\Status', 'status');
    }

    public function getPriority(){
        
        return $this->belongsTo('App\Priority', 'priority');
    }

    public function getAuthor() {
        
        return $this->belongsTo('App\User', 'created_by','id');
    }

    public function getAssignee() {

        return $this->belongsTo('App\User', 'assigned_to', 'id');
    }
   
    public function getAuthorUpdated() {

        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    public function getProject() {

        return $this->belongsTo('App\Project', 'project_id', 'id');
    }

}
