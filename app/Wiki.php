<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wiki
 *
 * @author Mimas
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Wiki extends Model {
    
    protected $table = "project_wiki";
    
    protected $fillable = ["wiki"];
    
    public function project(){
        
        return $this->belongsTo('App\Project');
    }
}
