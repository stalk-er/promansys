<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Comment
 *
 * @author Mimas
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
    
    protected $table = 'comments';
    
    protected $fillable = ['subject', 'content'];
    
    
    
    public function task(){
        
        return $this->belongsTo('App\Task');
    }
    
    public function attachments(){
        
        return $this->belongsToMany('App\Attachment', 'comment_attachment');
    }
    
}
