<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract, HasRoleAndPermissionContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword,
        HasRoleAndPermission {
        HasRoleAndPermission ::can insteadof Authorizable;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'profile_image',
        'description'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function projects() {

        return $this->belongsToMany('App\Project');
    }

    public function tasks() {

        return $this->hasMany('App\Task', 'assigned_to')
                        ->where("revision", "=", $this->hasMany('App\Task', 'assigned_to')
                                ->max('revision'));
    }

    public function userRoles() {

        $a = array_map(function($obj) {
            $obj = (object) $obj;
            return $obj->name;
        }, $this->getRoles()->toArray());
        $a = implode(", ", $a);
        return $a;
    }
    
    public function hasAtLeastOneRole() {
        
        return $this->belongsToMany('\App\Role', "role_user");
    }
}
