<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attachment
 *
 * @author Mimas
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model {

    protected $table = 'attachments';
    
    protected $fillable = ['name', 'filesize'];
    
    public function task() {
        
        return $this->belongsTo('App\Comment');
    }
    
    public function comment () {
        
        return $this->belongsTo('App\Comment');
    }
    
}
