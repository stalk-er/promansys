<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagementController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;

class ManagementController extends Controller {

    public function getManagement() {
        $users = User::all();
        foreach ($users as $u) {

            $a = array_map(function($obj) {
                $obj = (object) $obj;
                return $obj->name;
            }, $u->getRoles()->toArray());


            $a = implode(", ", $a);
            $u->roles = $a;
        }
        $roles = Role::all();
        $projects = \App\Project::all();
        return view('management')
                        ->with('users', $users)
                        ->with('roles', $roles)
                        ->with('projects', $projects);
    }

    public function getUser($id) {

        $user = User::find($id);
        $all_roles = Role::all();
        $user_roles = array();

        for ($i = 0; $i < count($user->roles); $i++) {
            $user_roles[$i] = $user->roles[$i]->id;
        }

        $a = array_map(function($obj) {
            $obj = (object) $obj;
            return $obj->name;
        }, $user->getRoles()->toArray());


        $a = implode(", ", $a);
        $user->roles = $a;

        return view('user-profile')
                        ->with('user', $user)
                        ->with('all_roles', $all_roles)
                        ->with('user_roles', $user_roles)
                        ->with('settings', \App\UserSettings::all()->toArray());
    }

    public function getRole($id) {

        $role = Role::find($id);
        $all_permissions = \Bican\Roles\Models\Permission::all();

        $role_permissions = array();
        for ($i = 0; $i < count($role->permissions); $i++) {
            $role_permissions[$i] = $role->permissions[$i]->id;
        }

        return view('role-single')
                        ->with('role', $role)
                        ->with('role_permissions', $role_permissions)
                        ->with('all_permissions', $all_permissions);
    }

    public function assignRole(Request $request, $id) {

        $user = User::find($id);
        $all_roles = Role::all();
        $all_roles_id = array();
        for ($i = 0; $i < count($all_roles); $i++) {
            $all_roles_id[$i] = $all_roles[$i]->id;
        }

        $roleToAssign = $request->input('roles');

        $unassigned = array();
        if (empty($roleToAssign)) {
            $user->detachAllPermissions();
        } else {
            $unassigned = array_values(array_diff($all_roles_id, $roleToAssign));
        }

        // Detaching permissions that are not requested
        for ($o = 0; $o < count($unassigned); $o++) {
            $user->detachRole(\Bican\Roles\Models\Permission::find($unassigned[$o]));
        }


        // Ataching the requested permissions
        for ($i = 0; $i < count($roleToAssign); $i++) {
            $user->attachRole(Role::find($roleToAssign[$i]));
            $user->save();
        }

        return redirect('management/user/' . $id);
    }

    public function assignRolePermission(Request $request, $id) {
        $role = Role::find($id);
        $all_permissions = $role->permissions;
        $all_permissions_id = array();

        for ($i = 0; $i < count($all_permissions); $i++) {
            $all_permissions_id[$i] = $all_permissions[$i]->id;
        }
        $permissions = $request->input('permissions');
        $unassigned = array();
        if (empty($permissions)) {
            $role->detachAllPermissions();
        } else {
            $unassigned = array_values(array_diff($all_permissions_id, $permissions));
        }
        // Detaching permissions that are not requested
        for ($o = 0; $o < count($unassigned); $o++) {
            $role->detachPermission(\Bican\Roles\Models\Permission::find($unassigned[$o]));
        }
        // Ataching the requested permissions
        for ($i = 0; $i < count($permissions); $i++) {

            $permission = \Bican\Roles\Models\Permission::find($permissions[$i]);
            $role->attachPermission($permission);
        }
        return redirect('management/role/' . $id);
    }

    public function updateUserDetails(Request $request, $id) {

        $user = \App\User::find($id);
        if ($user->update($request["data"])) {
            return response()->json([
                        "ok"
            ]);
        }
    }

    public function lastActivity() {
        # code...
    }

    function savePersonalSettings(Request $request) {

        $id = $request->id;
        $data = [];
        if (!$id)
            return false;

        $user = \App\User::find($id);
        if (!$user->exists())
            return false;

        foreach ($request->input() as $f => $field) {
            if ($f == "_token")
                continue;

            $newArr = [];
            $newArr['user_id'] = $id;
            $newArr['key'] = $f;
            $newArr['value'] = $field;
            array_push($data, $newArr);
        }

        foreach ($data as $d) {
            $userSettings = \App\UserSettings::where('key', '=', $d["key"]);
            if ($userSettings->exists()) {
                $userSettings->update($d);
            } else {
                \App\UserSettings::create($d);
            }
        }

        return redirect("/management/user/" . $id);
    }

    function getPersonalSettingsJson($id) {
        
        $user = \App\User::find($id);
        if(!$user->exists()) abort(404);
        
        $settings = \App\UserSettings::where('user_id', '=', $id);
        if (!$settings->exists())
            abort(404);

        return response()->json($settings->get());
    }

}
