<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectHasRelations
 *
 * @author Mimas
 */

namespace App\Http\Controllers\Traits;

trait ProjectHasRelations {
    
    public function members(){
        
        return $this->hasMany('App\User');
    }
    
}
