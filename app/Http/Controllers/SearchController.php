<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use Illuminate\Database\DatabaseManager;
use DB;
use \Illuminate\Support\Facades\Response;

class SearchController extends Controller {

    public function autocomplete(Request $request) {

        $term = $request->input('term');
        $results = [];
        $project_names = \App\Project::where('name', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($project_names); $i++) {
            $value = preg_replace('/<[^<]+?>/', '', $project_names[$i]->name);
            $v = html_entity_decode(str_replace(array('\n', '\r'), ' ', $value));
            $results[] = [
                'value' => $v,
                'label' => $v,
                'url' => url('/project/'.$project_names[$i]->id),
                'type' => 'project'
            ];
        }
        /*
          $project_descriptions = \App\Project::where('description', 'LIKE', '%' . $term . '%')->get();
          for ($i = 0; $i < count($project_descriptions); $i++) {
          $value = preg_replace('/<[^<]+?>/', '', $project_descriptions[$i]->description);
          $v = html_entity_decode(str_replace(array('\n', '\r'), ' ', $value));
          $results[] = ['value' => $v];
          }

          $project_wikies = \App\Wiki::where('wiki', 'LIKE', '%' . $term . '%')->get();
          for ($i = 0; $i < count($project_wikies); $i++) {
          $value = preg_replace('/<[^<]+?>/', '', $project_wikies[$i]->wiki);
          $v = html_entity_decode(str_replace(array('\n', '\r'), ' ', $value));
          $results[] = ['value' => $v];
          }
         */
        $task_subjects = \App\Task::where('subject', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($task_subjects); $i++) {
            $value = preg_replace('/<[^<]+?>/', '', $task_subjects[$i]->subject);
            $v = html_entity_decode(str_replace(array('\n', '\r'), ' ', $value));
            $results[] = [
                'value' => $v,
                'label' => $v,
                'url' => url('/task/'. $task_subjects[$i]->uid),
                'type' => 'task'
            ];
        }

        $users = \App\User::where('name', 'LIKE', '%' . $term . '%')->get();
        foreach($users as $user) {
            $results[] = [
                'value' => $user->name,
                'label' => $user->name,
                'url' => url('/management/user/'. $user->id),
                'type' => 'user'
            ];
        }

        /*
          $task_descriptions = \App\Task::where('description', 'LIKE', '%' . $term . '%')->get();
          for ($i = 0; $i < count($task_descriptions); $i++) {
          $value = preg_replace('/<[^<]+?>/', '', $task_descriptions[$i]->description);
          $v = html_entity_decode(str_replace(array('\n', '\r'), ' ', $value));
          $results[] = ['value' => $v];
          }
         */

        //dd(Response::json($results));
//        foreach ($queries as $query) {
//            //  . ' ' . $query->description
//            $results[] = ['value' => $query->name];
//        }


        return Response::json($results);
    }

    public function fetchResults(Request $request) {

        $results = [];
        $term = $request->input("q");


        $project_names = \App\Project::where('name', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($project_names); $i++) {

            $results["project/" . $project_names[$i]->id] = $project_names[$i]->name;
        }

        $project_descriptions = \App\Project::where('description', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($project_descriptions); $i++) {

            $results["project/" . $project_descriptions[$i]->id] = $project_descriptions[$i]->description;
        }

        $project_wikies = \App\Wiki::where('wiki', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($project_wikies); $i++) {

            $results["project/" . $project_wikies[$i]->project_id] = $project_wikies[$i]->wiki;
        }

        $task_subjects = \App\Task::where('subject', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($task_subjects); $i++) {

            $results["task/" . $task_subjects[$i]->id] = $task_subjects[$i]->subject;
        }

        $task_descriptions = \App\Task::where('description', 'LIKE', '%' . $term . '%')->get();
        for ($i = 0; $i < count($task_descriptions); $i++) {
            $results["task/" . $task_descriptions[$i]->id] = $task_descriptions[$i]->description;
        }
        return view('search-results')
                        ->with('results', $results)
                        ->with('term', $term);
    }

}
