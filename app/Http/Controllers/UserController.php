<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author Mimas
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;

class UserController extends Controller {

    public function unlock(Request $request) {

        // Validate that the users password is correct
        $validationRules = [
            'password' => 'required',
        ];

        // This will automatically redirect back to the form with errors if fails
        $this->validate($request, $validationRules);


        $password = $request->input('password');
        $credentials = [
            'password' => $password
        ];

        if (Auth::attempt($credentials)) {
            // If correct unlock the user and redirect him
            $user = Auth::user();
            $user->inactive = false;
            $user->save();

            return redirect('dashboard')->with('flashMessage', json_encode([
                "message"   => "Welcome back " . Auth::user()->name,
                "type"  => "info"
            ]));
        }
        // Password validation failed, try again
        return redirect()->back()->with("incorrect_password", "Incorrect password");
    }

    public function lock() {

        // Set the user inactive
        Auth::user()->inactive = true;
        Auth::user()->save();

        // Show the Lock Screen
        return view('lock-screen');
    }

}
