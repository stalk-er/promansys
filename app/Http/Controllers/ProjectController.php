<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use DB;

class ProjectController extends Controller {

    public function getProject($id) {

        $users = User::all();
        $project = \App\Project::find($id);
        $statuses = \App\Status::all();
        $priorities = \App\Priority::all();

        $tasks = $project->tasks()->orderBy('created_at', 'DESC')->paginate(10);
        $project->tasks = $tasks;

        return view('project')
                        ->with('project', $project)
                        ->with('users', $users)
                        ->with('statuses', $statuses)
                        ->with('priorities', $priorities);
    }

    public function getTasks($id) {

        $project = \App\Project::find($id);
        $statuses = \App\Status::all();
        $tasks = [];

        for ($i = 0; $i < count($statuses); $i++) {
            $id = $statuses[$i]->id;

            $tasks[strtolower(str_replace(' ', '', $statuses[$i]->name))] = $project->tasks()->where('status', $id)->count();
        }
        return json_encode($tasks);
    }

    public function addMember(Request $request, $id) {

        $requested_user = ($request->input('user-id') !== null) ? (int) $request->input('user-id') : (int) $request['user-id'];
        $project = \App\Project::find($id);
        if ($project->addMember($requested_user) == true) {
            return;
        }


        $user = User::find($requested_user);
        $user->roles = $user->userRoles();
        return $user;
    }

    public function removeMember(Request $request, $id) {

        $requested_user = ($request->input('user-id') !== null) ? (int) $request->input('user-id') : (int) $request['user-id'];
        $project = \App\Project::find($id);
        $project->removeMember($requested_user);

        return redirect('project/' . $id)
                        ->with('user_added', 'User has been removed from this project.');
    }

    public function addTask(Request $request) {

        $task = new \App\Task();
        $task->uid = uniqid();
        $task->subject = $request->input('task-subject');
        $task->description = $request->input('task-description');
        $task->status = $request->input('task-status');
        $task->priority = $request->input('task-priority');
        $task->type = $request->input('task-type');
        $task->created_by = $request->user()->id;
        $task->updated_by = $request->user()->id;
        $task->assigned_to = ($request->input('task-assigned-to'))?$request->input('task-assigned-to'):null;
        $task->revision = 0;
        $task->sent_reminder = 0;
        $task->project_id = $request->input('project-id');

        if ($request->input('task-deadline')) {
            $task->deadline = $request->input('task-deadline');
        }
        

        $task->save();

        if ($request->hasFile('task-attachments')) {
            $task_attachments = $request->file('task-attachments');

            foreach ($task_attachments as $attachment) {

                $file = new \App\Attachment();
                $file->name = $attachment->getClientOriginalName();
                $file->filesize = $attachment->getClientSize();
                $file->save();

                $attachment->move('uploads', $attachment->getClientOriginalName());
                $task->attachments()->attach($file);
            }
        }

        $task->status = \App\Status::find($task->status)->name;
        $task->priority = \App\Priority::find($task->priority)->name;
        $task->created_by = \App\User::find($task->created_by)->name;
        $task->status_class = generate_status_class(strtolower($task->priority));
        return json_encode($task);
    }

    public function getWiki($id) {

        $project = \App\Project::find($id);
        $wiki = \App\Project::find($id)->wikiPage;

        return view('project-wiki')
                        ->with('project', $project)
                        ->with('wiki', $wiki);
    }

    public function fillWiki(Request $request, $id) {

        $project = \App\Project::find($id);
        $wiki_data = json_decode($request["data"]);

        $project->wikiPage->wiki = $wiki_data;

        if ($project->wikiPage->save()) {

            return response()->json([
                        "success" => "Successfully updated."
            ]);
        }

        return response()->json([
                    "error" => "An error occured. Please refresh the page and try again later."
        ]);
    }

    public function getAllTasks($id) {
        $project = \App\Project::find($id);
        $tasks = $project->tasks;
        $statuses = \App\Status::all();
        $priorities = \App\Priority::all();
        $users = User::all();

        return view('task-list')
                        ->with('tasks', $tasks)
                        ->with('project', $project)
                        ->with('priorities', $priorities)
                        ->with('users', $users)
                        ->with('statuses', $statuses);
    }

    public function getAllProjects() {
        $all_projects = \App\Project::all();

        return view('all-projects')
                        ->with('all_projects', $all_projects);
    }

    public function editProjectInformation(Request $request, $id) {

        $project = \App\Project::find((int) $id);
        if ($request['deadline']) {
            $project->deadline = $request['deadline'];
        }

        if ($request['title']) {
            $project->name = $request['title'];
        }

        if ($request['description']) {
            $project->description = $request['description'];
        }
        $project->save();
        return redirect('project/' . $id);
    }

}
