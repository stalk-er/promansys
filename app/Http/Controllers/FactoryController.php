<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Storage;
use League\Flysystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;

class FactoryController extends Controller {

    public function getUsers() {

        $users = User::all();
        $roles = Role::all();
        $permissions = \Bican\Roles\Models\Permission::all();
        $projects = \App\Project::all();

        return view('factory')
                        ->with('users', $users)
                        ->with('roles', $roles)
                        ->with('permissions', $permissions)
                        ->with('projects', $projects);
    }

    public function createUser(Request $request) {

        if ($request->input('user-factory') == 1) {

            $validationRules = [
                'username' => 'required',
                'email' => 'required',
                'password' => 'required',
            ];

            $this->validate($request, $validationRules);
            $user = new User();

            $user->name = $request['username'];
            $user->password = Hash::make($request['password']);
            $user->email = $request['email'];
            $user->profile_image = '/images/user/default_avatar.jpg'; // The default avatar

            if ($user->save()) {

                return redirect('factory')->with('flashMessage', json_encode([
                            "message" => "The user has beed successfully created",
                            "type" => "success"
                ]));
            }

            return redirect('factory')->with('flashMessage', json_encode([
                        "message" => "The user was not created due to an error in the input. Review your input and try again.",
                        "type" => "warning"
            ]));
        }
    }

    public function createRole(Request $request) {
        if ($request->input('role-factory') == 2) {

            $role = Role::create([
                        'name' => $request->input('role-name'),
                        'slug' => $request->input('role-slug'),
                        'description' => $request->input('role-description'), // optional
                        'level' => $request->input('role-level'), // optional, set to 1 by default
            ]);

            if ($role->save()) {
                return redirect('factory')->with('flashMessage', json_encode([
                            "message" => "The role was successfully created",
                            "type" => "success"
                ]));
            }

            return redirect('factory')->with('flashMessage', json_encode([
                        "message" => "The role was not created due to an error. Review your input and try again",
                        "type" => "danger"
            ]));
        }
    }

    public function createPermission(Request $request) {

        $permission = \Bican\Roles\Models\Permission::create([
                    'name' => $request->input('permission-name'),
                    'slug' => $request->input('permission-slug'),
                    'description' => $request->input('permission-description'), // optional
        ]);

        if ($permission->save()) {
            return redirect('factory')->with('flashMessage', json_encode([
                        "message" => "The permission was successfully created",
                        "type" => "success"
            ]));
        }

        return redirect('factory')->with('flashMessage', json_encode([
                    "message" => "The permission was not created due to an error. Please review your input and try again",
                    "type" => "danger"
        ]));
    }

    public function createProject(Request $request) {

        $project = new \App\Project();

        $project->name = $request['project-name'];
        $project->description = $request['project-description'];
        $project->slug = $request['project-slug'];
        $project->save();
        $project->addMember(Auth::user()->id);

        $wiki = new \App\Wiki();
        $wiki->wiki = "This is you project wiki page. Put additional information about the project here.";
        $wiki->project()->associate($project->id);
        $wiki->save();


        if ($project->save()) {
            return redirect('factory')->with('flashMessage', json_encode([
                        "message" => "The project was successfully created",
                        "type" => "success"
            ]));
        }
        return redirect('factory')->with('flashMessage', json_encode([
                        "message" => "The project was not created due to an error. Review your input and try again",
                        "type" => "error"
            ]));
    }

}
