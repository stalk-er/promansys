<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use Input;
use Validator;
use Request;
use Response;
use Illuminate\Support\Facades\File;
use DB;

class DashboardController extends Controller {

    public function getDashboard() {
        
        $statusClosed = 5; // TODO: Refactor
        $my_projects = Auth::user()->projects;
        $my_tasks = \App\Task::select(DB::raw('*'))
        ->where('assigned_to', Auth::user()->id)
        ->where('status', '!=', $statusClosed)
        ->where(
          DB::raw('(uid, revision) IN ( SELECT DISTINCT uid, MAX(revision) FROM tasks GROUP BY uid )')
        , true)->with('project')->orderBy('priority', 'desc')->paginate(10);
        
        return view('dashboard')
                        ->with('my_tasks', $my_tasks)
                        ->with('my_projects', $my_projects);
    }

    public function uploadAvatar() {

        if (Input::hasFile('file')) {
            $user = Auth::user();
            $user_name = $user->name;

            if (!File::exists('uploads/' . $user_name)) {
                File::makeDirectory('uploads/' . $user_name);
            }

            $file = Input::file('file');
            File::cleanDirectory('uploads/' . $user_name . '/');
            $tmpFilePath = '/uploads/' . $user_name . '/';
            $tmpFileName = $file->getClientOriginalName();


            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = $tmpFilePath . $tmpFileName;
            $user->profile_image = $path;
            $user->save();
            return response()->json(array('path' => $path), 200);
        } else {
            return response()->json(false, 200);
        }
    }

}
