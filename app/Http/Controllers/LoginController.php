<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class LoginController extends Controller {

    // Return login page
    public function getLogin() {


        if (Auth::check()) {
            return view('dashboard');
        } else {
            return view('login');
        }
    }

    public function postLogin(Request $request) {

        $validationRules = [
            'username' => 'required',
            'password' => 'required',
        ];

        // This will automatically redirect back to the form with errors if fails
        $this->validate($request, $validationRules);

        $username = $request->input('username');
        $password = $request->input('password');
        $credentials = ['name' => $username, 'password' => $password];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            return redirect()->intended('dashboard');
        } else {

            return redirect('login')->withErrors($credentials)->withInput($credentials);
        }
    }

    // Logout
    public function getLogout() {
        if(Auth::user()->inactive) {
            Auth::user()->inactive = false;
            Auth::user()->save();
        }
        Auth::logout();
        return redirect('login');
    }

}
