<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TaskController
 *
 * @author Mimas
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;
use Bican\Roles\Models\Role;
use App\User;
use DB;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller {

    public function getTask($uid) {

        $task = \App\Task::where("uid", "=", $uid)->orderBy("revision", "ASC")->get();
        //$task_revisions = \App\Task::where("uid", "=", $task->uid)->orderBy("revision", "ASC")->get();                
        $status = new \App\Status;
        $priority = new \App\Priority;
        $project = new \App\Project;
        $user = new User;
        return view('task')
                        ->with('status', $status)
                        ->with('task', $task)
                        ->with('priority', $priority)
                        ->with('user', $user)
                        ->with('project', $project);
    }

    public function addComment(Request $request, $id) {

        $comment = new \App\Comment();
        $comment->subject = $request->input('comment-subject');
        $comment->content = $request->input('comment-content');
        $comment->save();

        if ($request->hasFile('attachments')) {
            $attachments = $request->file('attachments');

            foreach ($attachments as $attachment) {

                $file = new \App\Attachment();
                $file->name = $attachment->getClientOriginalName();
                $file->filesize = $attachment->getClientSize();
                $file->save();

                $attachment->move('uploads', $attachment->getClientOriginalName());
                $comment->attachments()->attach($file);
            }
        }

        $task = \App\Task::find($id);
        $task->comments()->attach($comment);

        return redirect('task/' . $task->uid);
    }

    public function updateTask(Request $request, $id) {

        $uid = $request->input('task-uid');
        $last_rev = DB::table("tasks")->where("uid", "=", $uid)->orderBy("revision", "DESC")->first();

        $task = new \App\Task();
        $task->uid = $request->input('task-uid');
        $task->subject = $request->input('task-subject');
        $task->description = $request->input("update-task-description");
        $task->status = $request->input('update-task-status');
        $task->priority = $request->input('update-task-priority');
        $task->type = $request->input('update-task-type');
        $task->created_by = $request->user()->id;
        $task->updated_by = $request->user()->id;
        $task->assigned_to = ($request->input('update-task-assignee')) ? $request->input('update-task-assignee') : null;
        $task->revision = $last_rev->revision + 1;
        $task->sent_reminder = 0;

        if ($request->input('task-deadline')) {
            $task->deadline = $request->input('task-deadline');
        }
        $task->project_id = $request->input('project_id');
        $task->save();

        if ($request->hasFile('task-attachments')) {
            $task_attachments = $request->file('task-attachments');

            foreach ($task_attachments as $attachment) {

                $file = new \App\Attachment();
                $file->name = $attachment->getClientOriginalName();
                $file->filesize = $attachment->getClientSize();
                $file->save();

                $attachment->move('uploads', $attachment->getClientOriginalName());
                $task->attachments()->attach($file);
            }
        }

        return redirect()->back();
    }

}
