<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


Route::get('login', 'LoginController@getLogin')->middleware('inactive');
Route::get('logout', 'LoginController@getLogout');
Route::post('login', 'LoginController@postLogin');

Route::group([
    'middleware' => [
        'auth',
        'inactive',
    ],
        ], function () {

    Route::get('/search/autocomplete', 'SearchController@autocomplete');
    Route::post('/search-results', 'SearchController@fetchResults'); // TODO: implement
    // Dashboard
    Route::get('dashboard', 'DashboardController@getDashboard');
    Route::post('avatar/upload', 'DashboardController@uploadAvatar');

    // Factory
    Route::group(['prefix' => 'factory'], function() {
        Route::post('users', 'FactoryController@createUser');
        Route::post('roles', 'FactoryController@createRole');
        Route::post('permissions', 'FactoryController@createPermission');
        Route::post('projects', 'FactoryController@createProject');
    });
    Route::get('factory', 'FactoryController@getUsers');


    // Management
    Route::group(['prefix' => 'management'], function() {

        //Route::get('user/{id}', [ 'as' => 'user/{id}', 'middleware' => 'permission:manage.users', 'uses' => 'ManagementController@getUser',]);
        Route::get('user/{id}', 'ManagementController@getUser');
        Route::get('role/{id}', ['as' => 'role/{id}', 'middleware' => 'permission:manage.roles', 'uses' => 'ManagementController@getRole',]);

        Route::post('user/{id}', 'ManagementController@assignUserPermission');
        Route::post('role/{id}', 'ManagementController@assignRolePermission');
        Route::post('role/{id}/edit', 'ManagementController@assignRolePermission');
        Route::post('user/{id}', 'ManagementController@assignRole');
        Route::post('user/{id}/settings', 'ManagementController@savePersonalSettings');
        Route::get('user/{id}/settings/json', 'ManagementController@getPersonalSettingsJson');
    });

    Route::get('management', ['as' => 'management',
        'uses' => 'ManagementController@getManagement',]);
    //Route::get('management', 'ManagementController@getManagement');
    // Project
    Route::get('all-projects', 'ProjectController@getAllProjects');
    Route::get('project/{id}', 'ProjectController@getProject')->middleware('is-project-member');
    Route::get('project/{id}/tasks', 'ProjectController@getTasks');

    Route::group(['prefix' => 'project/{id}'], function() {
        Route::post('new-member', ['as' => 'new-member', 'middleware' => 'permission:add.project.member', 'uses' => 'ProjectController@addMember']);
        Route::post('remove-member', ['as' => 'remove-member', 'middleware' => 'permission:add.project.member', 'uses' => 'ProjectController@removeMember']);
        Route::post('new-task', ['as' => 'new-task', 'middleware' => 'permission:create.task', 'uses' => 'ProjectController@addTask']);
        Route::get('wiki', 'ProjectController@getWiki');
        Route::post('wiki', 'ProjectController@fillWiki');
        Route::get('all-tasks', 'ProjectController@getAllTasks');
        Route::post('deadline/edit', 'ProjectController@editProjectInformation');
        Route::post('title/edit', 'ProjectController@editProjectInformation');
        Route::post('description/edit', 'ProjectController@editProjectInformation');
    });

    // Task & Comment
    Route::get('task/{uid}', 'TaskController@getTask')->middleware('permission:view.task');
    Route::post('task/{id}/new-comment', 'TaskController@addComment');
    Route::post('task/{id}/update', 'TaskController@updateTask');

    Route::get('tasks/new', 'TaskController@getTaskByStatus');
    Route::get('tasks/feedback', 'TaskController@getTaskByStatus');
    Route::get('tasks/resolved', 'TaskController@getTaskByStatus');
    Route::get('tasks/closed', 'TaskController@getTaskByStatus');


    // Change user details AJAX
    Route::post('user/{id}/details', 'ManagementController@updateUserDetails');
});

// Lock screen
Route::get('/locked', 'UserController@lock');
Route::post('/unlock', 'UserController@unlock');
