<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status
 *
 * @author Mimas
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

class Status extends Model {
    
    protected $table = 'statuses';
    
    protected $fillable = ['name', 'description'];
    
    
    public function task(){
        
        return $this->belongsTo('\App\Task');
    }
    
}
