<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('statuses')->insert(array(
            array('name' => 'Resolved', 'description' => 'Status assigned when task is complete'),
            array('name' => 'In Progress', 'description' => 'Status assigned when task is still under construktion'),
            array('name' => 'New', 'description' => 'Status assigned when task is new'),
            array('name' => 'Feedback', 'description' => 'Status assigned when task needs approval'),
            array('name' => 'Closed', 'description' => 'Status assigned when task is closed'),
        ));
    }

}
