<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrioritiesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('priorities')->insert(array(
            array('name' => 'Low'),
            array('name' => 'Normal'),
            array('name' => 'High'),
            array('name' => 'Urgent'),
            array('name' => 'Immediate'),
        ));
    }
}
