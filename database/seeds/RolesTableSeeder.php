<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('roles')->insert(array(
            array('name' => 'Master', 'slug' => 'master', 'description' => 'Master of the application', 'level' => 1)
        ));
    }

}
