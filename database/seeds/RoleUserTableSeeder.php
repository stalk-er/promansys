<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleUserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('role_user')->insert(array(
            array('role_id' => 1, 'user_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime)
        ));
    }

}
