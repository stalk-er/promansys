<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminSeeder
 *
 * @author Mimas
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder {

    public function run() {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        DB::table('users')->insert( array(
            'name' => 'Master',
            'email' => 'dimitrovovich@gmail.com',
            'password' => Hash::make('123456789'),
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ));
        
    }
}
