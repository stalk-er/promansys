<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() { 

        DB::table('permissions')->insert(array(
            
            // Task Permissions
            array('name' => 'View.Task', 'slug' => 'view.task', 'description' => 'Allowed to view specific task', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Create.Task', 'slug' => 'create.task', 'description' => 'Create task', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Edit.Task', 'slug' => 'edit.task', 'description' => 'Assign, add attachments, change status, priority', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Delete.Task', 'slug' => 'delete.task', 'description' => 'Allowed to delete tasks', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            
            // Comment Permissions
            array('name' => 'Add.Comment', 'slug' => 'add.comment', 'description' => 'Add comment to a task', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'View.Comment', 'slug' => 'view.comment', 'description' => 'View comments of a task', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            
            // Task Permissions
            array('name' => 'Edit.Deadline', 'slug' => 'edit.deadline', 'description' => 'Edit deadline of a task', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            
            // Project Permissions
            array('name' => 'Create.Project', 'slug' => 'create.project', 'description' => 'Can create projects', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Edit.Project', 'slug' => 'edit.project', 'description' => 'Can edit projects title, description, deadline', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Delete.Project', 'slug' => 'delete.project', 'description' => 'Allowed to delete a project', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Add.Project.Member', 'slug' => 'add.project.member', 'description' => 'Allowed to add a member to a project', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Delete.Project.Member', 'slug' => 'delete.project.member', 'description' => 'Allowed to delete a project member', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            
            // Administration Permissions
            array('name' => 'Create.Users', 'slug' => 'create.users', 'description' => 'Allowed to create user accounts', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Create.Roles', 'slug' => 'create.roles', 'description' => 'Allowed to create user roles', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Manage.Users', 'slug' => 'manage.users', 'description' => 'Allowed to change user information and assign role', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            array('name' => 'Manage.Roles', 'slug' => 'manage.roles', 'description' => 'Allowed to change roles information and change permissions', 'created_at' => new DateTime, 'updated_at' => new DateTime),
            
        ));
        
    }
}
