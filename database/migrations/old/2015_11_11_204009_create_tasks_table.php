<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid');
            $table->string('subject');
            $table->longText('description')->nullable();
            $table->integer('status')->unsigned();
            $table->foreign('status')->references('id')->on('statuses')->onDelete('cascade');
            $table->integer('priority')->unsigned();
            $table->foreign('priority')->references('id')->on('priorities')->onDelete('cascade');
            $table->string('type');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->integer('assigned_to')->unsigned();
            $table->foreign('assigned_to')->references('id')->on('users')->onDelete('cascade');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('deadline')->nullable();
            $table->integer('revision')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tasks');
    }

}
