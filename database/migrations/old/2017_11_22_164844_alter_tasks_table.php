<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTasksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        //Schema::disableForeignKeyConstraints();

        Schema::table('tasks', function(Blueprint $table) {
            // $table->dropForeign('tasks_assigned_to_foreign');

            $table->integer('assigned_to')->nullable()->unsigned()->change();
        });

        //Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
