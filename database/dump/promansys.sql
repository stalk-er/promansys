-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `promansys`
--

-- --------------------------------------------------------

--
-- Структура на таблица `attachments`
--

CREATE TABLE `attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filesize` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `filesize`, `created_at`, `updated_at`) VALUES
(1, 'learnRegexMF.PNG', 29166, '2017-03-23 19:19:01', '2017-03-23 19:19:01'),
(2, 'learnRegexMF.PNG', 29166, '2017-03-23 19:28:00', '2017-03-23 19:28:00'),
(3, 'learnRegexMF.PNG', 29166, '2017-03-23 19:57:19', '2017-03-23 19:57:19'),
(4, 'date-format.PNG', 70634, '2017-11-08 17:49:46', '2017-11-08 17:49:46');

-- --------------------------------------------------------

--
-- Структура на таблица `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `comment_attachment`
--

CREATE TABLE `comment_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_01_15_105324_create_roles_table', 1),
('2015_01_15_114412_create_role_user_table', 1),
('2015_01_26_115212_create_permissions_table', 1),
('2015_01_26_115523_create_permission_role_table', 1),
('2015_02_09_132439_create_permission_user_table', 1),
('2015_11_11_075609_create_statuses_table', 1),
('2015_11_11_133627_create_priorities_table', 1),
('2015_11_11_203943_create_projects_table', 1),
('2015_11_11_204009_create_tasks_table', 1),
('2015_11_11_204048_create_project_task_table', 1),
('2015_11_14_141556_create_project_user_table', 1),
('2015_11_15_151504_create_project_wiki_table', 1),
('2015_11_16_193818_create_wiki_table', 1),
('2015_11_18_181719_create_comments_table', 1),
('2015_11_18_181751_create_task_comment_table', 1),
('2015_11_18_232515_create_attachments_table', 1),
('2015_11_18_232528_create_task_attachment_table', 1),
('2015_11_19_204140_create_comment_attachment_table', 1),
('2015_11_28_142351_alter_table_users', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`) VALUES
(1, 'View.Task', 'view.task', 'Allowed to view specific task', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(2, 'Create.Task', 'create.task', 'Create task', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(3, 'Edit.Task', 'edit.task', 'Assign, add attachments, change status, priority', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(4, 'Delete.Task', 'delete.task', 'Allowed to delete tasks', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(5, 'Add.Comment', 'add.comment', 'Add comment to a task', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(6, 'View.Comment', 'view.comment', 'View comments of a task', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(7, 'Edit.Deadline', 'edit.deadline', 'Edit deadline of a task', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(8, 'Create.Project', 'create.project', 'Can create projects', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(9, 'Edit.Project', 'edit.project', 'Can edit projects title, description, deadline', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(10, 'Delete.Project', 'delete.project', 'Allowed to delete a project', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(11, 'Add.Project.Member', 'add.project.member', 'Allowed to add a member to a project', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(12, 'Delete.Project.Member', 'delete.project.member', 'Allowed to delete a project member', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(13, 'Create.Users', 'create.users', 'Allowed to create user accounts', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(14, 'Create.Roles', 'create.roles', 'Allowed to create user roles', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(15, 'Manage.Users', 'manage.users', 'Allowed to change user information and assign role', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(16, 'Manage.Roles', 'manage.roles', 'Allowed to change roles information and change permissions', NULL, '2017-03-23 18:49:07', '2017-03-23 18:49:07');

-- --------------------------------------------------------

--
-- Структура на таблица `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(2, 2, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(3, 3, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(4, 4, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(5, 5, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(6, 6, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(7, 7, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(8, 8, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(9, 9, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(10, 10, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(11, 11, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(12, 12, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(13, 13, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(14, 14, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(16, 16, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(17, 1, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(18, 2, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(19, 3, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(20, 4, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(21, 5, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(22, 6, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(23, 7, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(24, 8, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(25, 9, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(26, 10, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(27, 11, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(28, 12, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(29, 13, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(30, 14, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(32, 16, 2, '2017-03-25 08:45:21', '2017-03-25 08:45:21'),
(36, 15, 1, '2017-08-12 10:37:31', '2017-08-12 10:37:31');

-- --------------------------------------------------------

--
-- Структура на таблица `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `priorities`
--

CREATE TABLE `priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `description`) VALUES
(1, 'Low', NULL),
(2, 'Normal', NULL),
(3, 'High', NULL),
(4, 'Urgent', NULL),
(5, 'Immediate', NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `slug`, `deadline`, `created_at`, `updated_at`) VALUES
(3, 'Mandala', 'Mandala new website', 'mandala', '', '2017-04-01 07:35:31', '2017-04-01 07:35:31'),
(4, 'Zing Shower', 'Zing shower landing page redisign', 'zing', '08/09/2017', '2017-08-08 18:28:17', '2017-08-15 16:28:41'),
(5, 'Just Crm', 'This is my other project that I''m quality testing and I''m find a lot of bugs, so I need a place to log the issues that I find. What a perfect place to test my other project along with this project for bugs.', 'just-crm', '', '2017-11-06 15:29:04', '2017-11-06 15:29:04'),
(6, 'PromanSys', 'This project is for the current ticketing, project managing system. What a good place to start logging it''s own bugs.', '/promansys', '', '2017-11-06 16:08:59', '2017-11-06 16:08:59'),
(7, '3D Aframe game', 'This is a FPS 3D, VR JavaScript game based on the AFRAME library. The skeleton of the game is implemented in AngularJs and SocketIO.', '/js-game', '12/31/2017', '2017-11-08 17:19:37', '2017-11-08 17:39:04'),
(8, 'NBU ( New Bulgarian University )', 'This is a project dedicated  to my responsibilities in NBU.', '/nbu', '12/20/2017', '2017-11-08 17:54:35', '2017-11-08 17:55:09'),
(9, 'Music Catalog ( MSociety )', 'This is a simple website/app that allows users to upload a song and listen to other people songs. Other cool functionalities are also part of this project like, see what other people listen at the moment, see who''s online, create playlists etc.', '/msociety', '', '2017-11-11 09:42:27', '2017-11-11 09:42:27');

-- --------------------------------------------------------

--
-- Структура на таблица `project_task`
--

CREATE TABLE `project_task` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `project_user`
--

CREATE TABLE `project_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `project_user`
--

INSERT INTO `project_user` (`id`, `project_id`, `user_id`) VALUES
(6, 3, 1),
(7, 3, 2),
(8, 4, 1),
(9, 3, 3),
(10, 5, 1),
(11, 6, 1),
(12, 7, 1),
(13, 8, 1),
(14, 9, 1);

-- --------------------------------------------------------

--
-- Структура на таблица `project_wiki`
--

CREATE TABLE `project_wiki` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `wiki` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `project_wiki`
--

INSERT INTO `project_wiki` (`id`, `project_id`, `wiki`, `created_at`, `updated_at`) VALUES
(3, 3, '<h2>Live access</h2><h4>\n\n<b>Lorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</b></h4><p>In commodo sem urna, quis hendrerit velit elementum vel. Vivamus mi leo, aliquet aliquam sem ac, consectetur laoreet enim. Nunc aliquet sodales est, a auctor justo commodo sit amet. Morbi varius feugiat rutrum. Sed est ante, aliquet quis porta quis, mollis ac magna. Phasellus pellentesque nec ex ut accumsan. Aliquam eu eleifend lorem. Suspendisse in quam quis nisi commodo pretium. Nulla massa metus, fermentum et turpis nec, maximus venenatis nulla.\n\n<br></p><h2>Staging Access</h2><h4>\n\n\n\n<b>Nunc id auctor mi, finibus sagittis dui.&nbsp;</b></h4><p>Mauris commodo imperdiet augue, et mollis ex vehicula id. In rhoncus ligula at mattis interdum. Proin sit amet libero id lacus porta mattis. Vestibulum non consectetur felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris rutrum est vel velit molestie, in feugiat urna tempor. Maecenas vel diam ut orci tristique placerat id eu dolor.\n\n\n<br></p><h2>Test access</h2><h4>\n\n<b>Vestibulum at porta diam, euismod pulvinar lectus.&nbsp;</b></h4><p>Vivamus tincidunt sodales dapibus. Cras pulvinar metus ut aliquam tincidunt. In quam nulla, tempor ut purus et, consequat tincidunt risus. Vivamus convallis tempor quam, et commodo arcu pretium cursus. In sollicitudin ullamcorper cursus. Integer a velit dui. Phasellus eget nibh placerat, gravida mauris ac, dictum neque.&nbsp;</p>\n\n<h2>Client area</h2><h4>\n\n<b>Vestibulum at porta diam, euismod pulvinar lectus. </b></h4><h5>Vivamus tincidunt sodales dapibus.&nbsp;</h5><h6>Cras pulvinar metus ut aliquam tincidunt.&nbsp;</h6><h2>In quam nulla, tempor ut purus et, consequat tincidunt risus.&nbsp;</h2><p><i></i></p><blockquote><i>Vivamus convallis tempor quam, et commodo arcu pretium cursus. In sollicitudin ullamcorper cursus.&nbsp;</i></blockquote><p></p><p></p><ul><li>Integer a velit dui.&nbsp;<br></li><li>Phasellus eget nibh placerat.<br></li><li>Gravida mauris ac.<br></li><li>Dictum neque<br></li></ul><p></p>\n\n<br>', '2017-04-01 07:35:31', '2017-08-08 17:28:30'),
(4, 4, 'This is you project wiki page. Put additional information about the project here.', '2017-08-08 18:28:17', '2017-08-08 18:28:17'),
(5, 5, '<p>User login:</p><p><b>username</b> cvetan</p><p><b>password</b> 12345</p>', '2017-11-06 15:29:04', '2017-11-06 15:52:29'),
(6, 6, '<h2>System login</h2><b></b><b>username </b><b></b>Master<div><b>password </b>123456789</div>', '2017-11-06 16:08:59', '2017-11-06 16:10:38'),
(7, 7, '<h1>Source control</h1><p>GIT</p><h1>Repository</h1><p>﻿<a href="https://stalk-er@bitbucket.org/stalk-er/jsgame.git">https://stalk-er@bitbucket.org/stalk-er/jsgame.git</a><br></p><h1>Technologies&nbsp;used</h1><p><ul><li>AngularJS<br></li><li>SocketIO<br></li><li>AFRAME<br></li><li>ThreeJS<br></li><li>HTML5<br></li><li>CSS3\n\n﻿<br></li></ul></p>', '2017-11-08 17:19:37', '2017-11-08 17:24:15'),
(8, 8, 'This is you project wiki page. Put additional information about the project here.', '2017-11-08 17:54:35', '2017-11-08 17:54:35'),
(9, 9, 'This is you project wiki page. Put additional information about the project here.', '2017-11-11 09:42:27', '2017-11-11 09:42:27');

-- --------------------------------------------------------

--
-- Структура на таблица `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`) VALUES
(1, 'Master', 'master', 'Master of the application', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Developer', 'developer', 'Developer has access to project repositories, wiki and servers.  His main responsibility is coding.', 5, '2017-03-25 08:44:03', '2017-03-25 08:44:03'),
(3, 'Cleaner', 'cleaner', 'It''s responsible for the hygiene of the office.', 1, '2017-11-11 11:32:08', '2017-11-11 11:32:08'),
(4, 'Coffee Supplier', 'coffee.supplier', 'The Coffee Supplier is responsible for the coffee supplies at the office so keeping the programmers awake and happy.', 0, '2017-11-11 11:35:45', '2017-11-11 11:35:45');

-- --------------------------------------------------------

--
-- Структура на таблица `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-03-23 18:49:07', '2017-03-23 18:49:07'),
(2, 2, 2, '2017-08-08 17:32:56', '2017-08-08 17:32:56');

-- --------------------------------------------------------

--
-- Структура на таблица `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `description`) VALUES
(1, 'Resolved', 'Status assigned when task is complete'),
(2, 'In Progress', 'Status assigned when task is still under construktion'),
(3, 'New', 'Status assigned when task is new'),
(4, 'Feedback', 'Status assigned when task needs approval'),
(5, 'Closed', 'Status assigned when task is closed');

-- --------------------------------------------------------

--
-- Структура на таблица `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `status` int(10) UNSIGNED NOT NULL,
  `priority` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `assigned_to` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revision` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `tasks`
--

INSERT INTO `tasks` (`id`, `uid`, `subject`, `description`, `status`, `priority`, `type`, `created_by`, `updated_by`, `assigned_to`, `project_id`, `deadline`, `revision`, `created_at`, `updated_at`) VALUES
(32, '58dfabbb304d0', 'This is ticket number 5', '<p>This is ticket number<b> 5</b><br></p>', 3, 2, 'Ticket number 5', 1, 1, 1, 3, '04/08/2017', 0, '2017-04-01 10:31:39', '2017-04-01 10:31:39'),
(33, '58dfabbb304d0', 'This is ticket number 5', NULL, 3, 2, 'Ticket number 5', 1, 1, 1, 3, '04/08/2017', 1, '2017-04-01 10:32:00', '2017-04-01 10:32:00'),
(34, '58dfabbb304d0', 'This is ticket number 5', NULL, 2, 4, 'Ticket number 5', 1, 1, 2, 3, '04/16/2017', 2, '2017-09-04 13:01:18', '2017-09-04 13:01:18'),
(35, '58dfabbb304d0', 'This is ticket number 5', NULL, 2, 5, 'Ticket number 5', 1, 1, 1, 3, '04/16/2017', 3, '2017-09-04 13:02:24', '2017-09-04 13:02:24'),
(36, '5a009e4d153d8', 'Index page for products - modify layout', '<p>Hi Ceco,</p><p>We need to change the layout of the products index view where the products are listed, so the design follows the rest of the backend design.</p><p><br></p><p>Close the ticket once ready.</p>', 3, 4, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:39:25', '2017-11-06 15:39:25'),
(37, '5a009eb1b2b60', 'Orders dashboard Pie Chart', '<p>Hi Ceco.</p><p>The orders dashboard does not seem to do anything. Fill the Pie Chart with dynamic data.</p><p>Please, validate before closing the ticket.</p>', 3, 3, 'Major issue', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:41:05', '2017-11-06 15:41:05'),
(38, '5a00a0161c908', 'Creating new order', '<p>Hi Ceco.</p><p>When creating a new order the frontend validation doesn''t work politly. Make sure that the fields are validated correctly and</p><div>when an error occur make sure to return the added products in the response as well.<p></p></div><div>Test the bug before closing the ticket.</div>', 3, 4, 'Major issue', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:47:02', '2017-11-06 15:47:02'),
(39, '5a00a1073b150', 'Search partial', '<p>Hi Ceco.</p><p>Make the search a global partial and share all the data with it. That way we can use it throughout the while system.</p><p>A separate page for the results isn''t needed at the moment. You can show the results as a autosuggestion and depending on the entity found, forward to that link on click of the result.</p>', 3, 2, 'Minor Issue', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:51:03', '2017-11-06 15:51:03'),
(40, '5a00a246566d0', 'Full calendar js implementation review', '<p>HI Ceco.</p><p>At the moment we have a really full featured full calendar js implementation. But some of the JS events are not dispatched. Please review your code, optimize if needed. The common goal is to make it work without any glitches.</p>', 3, 2, 'Major Issue', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:56:22', '2017-11-06 15:56:22'),
(41, '5a00a2aebbfd0', 'Quote create view', '<p>Hi.</p><p>On the quote create view there''s an error. It seems some variable is undefined. Make sure you fix this. It should be easy fix.</p><p><br></p><p>Close once ready.</p>', 3, 1, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 15:58:06', '2017-11-06 15:58:06'),
(42, '5a00a36a0a028', 'Menu item expanded when on single page', '<p>Hi.</p><p>When in a single view for example: product, order, quote or whatever it is, the menu item is closed.</p><p>If I''m on a page for example&nbsp;\r\n\r\n<i>quote/1/edit&nbsp;</i> I need to see the `Quote` expanded in the menu.</p><p>Close once it''s ready.</p>', 3, 1, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 16:01:14', '2017-11-06 16:01:14'),
(43, '5a00a3b29db70', 'Make the title on each page dynamic', '<p>Hi.</p><p>It seems that no title is put on the pages. A unique title should be put on each page template and displayed as a title of the page.</p><p>It''s a minor stuff but have to be done in order to complete the project.</p>', 3, 1, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 16:02:26', '2017-11-06 16:02:26'),
(44, '5a00a42a01770', 'Remove Notifications from the menu bar', '<p>Hi,</p><p>Can you please remove the Notifications section in the menu bar. It seems that after we migrated all notifications inside the calendar, we don''t need this `Notifications` item in the menu.</p><p>Close the ticket once ready.</p>', 3, 1, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 16:04:26', '2017-11-06 16:04:26'),
(45, '5a00a477324b0', 'Organizations entity', '<p>HI,</p><p>At the moment the `Organizations` entity doesn''t seem to work. Can you proceed with that once. Take a deep thought before starting the work.</p><p>It''s important to do it once, no rework allowed here :)</p>', 3, 1, 'Minor issues', 1, 1, 1, 5, NULL, 0, '2017-11-06 16:05:43', '2017-11-06 16:05:43'),
(46, '5a00a62948828', 'My Profile view', '<p>Hi,</p><p>On the my profile view, I need to be able to change my password as a user.</p>', 3, 1, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:12:57', '2017-11-06 16:12:57'),
(47, '5a00a70181268', 'Implement a lock screen', '<p>Hi,</p><p>We need to track the inactive users and after some time of inactivity we lock the current screen. The screen lock markup is inside the theme flat html. The unlocking will be with password and username. After filling the username and password the screen will go off and the system must show the last thing that the user was doing before that lock happen.</p><p>Make sure you tested it properly, so no rework is needed.</p>', 3, 1, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:16:33', '2017-11-06 16:16:33'),
(48, '5a00a769e57e0', 'Information messages inconsistent', '<p>Hi,</p><p>It seems that the information messages inside the system are not consistent. Each message has a different styling and some of them have no styling at all.</p><p>Make sure you use the <b>PNotify</b> or other pre-compiled library for this.</p><p>Test well before closing the ticket.</p>', 3, 1, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:18:17', '2017-11-06 16:18:17'),
(49, '5a00a8a338658', 'System settings', '<p>Hi.</p><p>As a last priority we need to implement a system settings page where we can put configurations. The settings page should consist of different fields (check boxes, radio buttons, text fields).</p><p>Example settings:</p><p>Color scheme chooser<br>Lock screen inactivity timeout<br>Widgets that can be added on the dashboard</p>', 3, 1, 'Last priority', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:23:31', '2017-11-06 16:23:31'),
(50, '5a00a968a96f0', 'Project view - Pie Chart', '<p>Hi,</p><p>On the project view there''s seem to have a very nice pie chart displaying the tasks by status and priority.&nbsp;</p><p>For some reason the tasks with `New` status are not listed there. Please add this logic to the chart.</p><p>Test carefully then close the ticket.</p>', 3, 3, 'Major issue', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:26:48', '2017-11-06 16:26:48'),
(51, '5a00a9cca3160', 'Dashboard view (My page)', '<p>Hi,</p><p>When I''m on my page, I can see how many tickets do I have, so I have to count them by hand which is a hell.</p><p>Show the count of all my tickets.</p><p>Close once ready.</p>', 3, 3, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:28:28', '2017-11-06 16:28:28'),
(52, '5a00aa5e66c88', 'Dashboard view (My page)', '<p>Hi,</p><p>When I''m on my page, I can''t sort my tickets by priority so I can''t start work on the most urgent tickets.</p><p>Please implement a sorting by column. The best scenario is to have sort by all the columns (including date).</p><p>Also, another thing is that when the tasks are listed I can''t see which task is from which project. Can you add a column to the table showing the project that this task belongs to?</p><p><br></p><p>Thanks</p><p>Close once ready.</p>', 3, 4, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-06 16:30:54', '2017-11-06 16:30:54'),
(53, '58dfabbb304d0', 'This is ticket number 5', NULL, 5, 5, 'Ticket number 5', 1, 1, 1, 3, NULL, 4, '2017-11-06 17:21:29', '2017-11-06 17:21:29'),
(56, '5a00a968a96f0', 'Project view - Pie Chart', '<p>This isn''t resolved. I''ll put the proper status of the ticket and continue working on it.</p>', 2, 3, 'Major issue', 1, 1, 1, 6, NULL, 3, '2017-11-06 18:00:49', '2017-11-06 18:00:49'),
(58, '5a00a9cca3160', 'Dashboard view (My page)', '<p>Hi there.</p><p>I finished with this task. Now you can visit your dashboard and see the count of the tasks for the current page. So you''ll see something like `10 / 12`.</p><p>I''m closing the ticket, and if any issues will open it again.</p>', 5, 3, 'Minor issues', 1, 1, 1, 6, NULL, 1, '2017-11-06 18:54:48', '2017-11-06 18:54:48'),
(59, '5a00aa5e66c88', 'Dashboard view (My page)', '<p>Hello.</p><p>I implemented the jQuery data tables. So, right now everything is sortable. You can sort all your tickets by project, priority, status etc. Really nice I say.</p><p>Thanks for this report. I''m closing the ticket.</p>', 5, 4, 'Minor issues', 1, 1, 1, 6, NULL, 1, '2017-11-06 18:56:27', '2017-11-06 18:56:27'),
(60, '5a00d3a6bc3b8', 'Create user profile page', '<p>Hi.</p><p>Can you create a personal user profile page where he can change his password and other data. Also this page will serve as a public page for the other users. For example I want to see Sara''s page I can see her email, personal description and optionally the last activity.<br></p><p>The last activity is the last activity on tasks and projects.</p><p>Please close the ticket once ready with this.</p>', 3, 2, 'Phase 2', 1, 1, 1, 6, NULL, 0, '2017-11-06 19:27:02', '2017-11-06 19:27:02'),
(61, '5a035b1ebbbe8', 'Implement Physics Engine', '<p>Hi.</p><p>In order to complete the game we need to implement the physics engine supplied by the aframe extras package. This engine will allow the proper collision detection inside the environment.</p><p>Aframe physics engine:&nbsp;<a target="_blank" rel="nofollow" href="https://github.com/donmccurdy/aframe-physics-system">https://github.com/donmccurdy/aframe-physics-system</a>&nbsp;</p><p>Follow&nbsp;the humble&nbsp;documentation&nbsp;there&nbsp;in&nbsp;order to&nbsp;integrate the&nbsp;engine&nbsp;inside&nbsp;the&nbsp;game.<br></p>', 3, 3, 'Integration', 1, 1, 1, 7, NULL, 0, '2017-11-08 17:29:34', '2017-11-08 17:29:34'),
(62, '5a035d39aba18', 'Implement game environment', '<p>Hi.</p><p>Integrate an environment inside the game. This should be buildings, threes, cars etc. We need game object that the user will interact with. I found a pretty good library with a lot of already defined environments. The problem is that this library isn''t integrated with the AFRAME physics engine which means that we have to do it or we find a better library for environments that can be easy extendable. Here''s the reference to the library, maybe you could find it useful:&nbsp;<a target="_blank" rel="nofollow" href="https://github.com/feiss/aframe-environment-component">https://github.com/feiss/aframe-environment-component</a></p><p>Here''s the demo of it:&nbsp;<a target="_blank" rel="nofollow" href="https://feiss.github.io/aframe-environment-component">https://feiss.github.io/aframe-environment-component</a></p><p>Good&nbsp;luck&nbsp;with that&nbsp;one.<br></p>', 3, 4, 'Major Integration', 1, 1, 1, 7, NULL, 0, '2017-11-08 17:38:33', '2017-11-08 17:38:33'),
(63, '5a035e8f52080', 'Find 3D rigged models', '<p>Hi,</p><p>In order to create a better user experience we need to find better 3D rigged models with animations. The models needs to be exported with the animations and action name so we can use these actions with the <b>AFRAME </b><i><u>animation mixer</u></i> component.</p><p>Once you get into that, please speak to a designer about that models or in the worst case scenario find rigged models in the web. I guess the last one will take a lot of time, finding the suitable models that will work our case isn''t easy, but do your best.</p><p><b>NOTE</b>:</p><p>The models needs to be in format <i>.json</i>, <i>.fbx</i> or <i>.gltf</i></p>', 3, 4, 'MInor Integration', 1, 1, 1, 7, NULL, 0, '2017-11-08 17:44:15', '2017-11-08 17:44:15'),
(64, '5a035fda09858', 'Single task - date format', '<p>Hi,</p><p>Can you format the data on the single task page, in another, more user-friendly format. Check the screenshot to see what I mean.</p><p><br></p>', 3, 2, 'Minor issues', 1, 1, 1, 6, NULL, 0, '2017-11-08 17:49:46', '2017-11-08 17:49:46'),
(65, '5a06f4dd376b8', 'Spinning issue on home page', '<p>Hi.</p><p>Please fix the spinning issue on the home page. It seems that for some users this spinning is eating a lot of memory and does not succeed at all.</p><p>Please review and close the ticket once you implement a proper solution.</p>', 3, 3, 'Minor Issue', 1, 1, 1, 9, NULL, 0, '2017-11-11 11:02:21', '2017-11-11 11:02:21'),
(66, '5a00a968a96f0', 'Project view - Pie Chart', '<p>Hi.</p><p>The Pie chart thing is sorted now. I''m resolving the ticket.</p>', 5, 3, 'Major issue', 1, 1, 1, 6, NULL, 4, '2017-11-11 11:45:39', '2017-11-11 11:45:39'),
(67, '5a035fda09858', 'Single task - date format', '<p>Hi.</p><p>This is sorted now. Closing the ticket.</p>', 5, 2, 'Minor issues', 1, 1, 1, 6, NULL, 1, '2017-11-11 11:50:36', '2017-11-11 11:50:36'),
(68, '5a00a769e57e0', 'Information messages inconsistent', '<p>Hi.</p><p>I used the theme messages. They seem satisfying. I saw the <b>PNotify</b> lib, it''s \r\n\r\ngorgeous but I decided to use what I have for now. The implementation is quite interesting.</p><p>In the back end I return a <i>`flashMessage`</i>, and on the front end I check if this variable is <i>undefined </i>or <i>null.&nbsp;</i>If the variable contains a valid <i>json encoded</i> message, I show it with <b>javascript</b>.</p>', 5, 1, 'Minor issues', 1, 1, 1, 6, NULL, 1, '2017-11-11 11:57:07', '2017-11-11 11:57:07'),
(69, '5a00a70181268', 'Implement a lock screen', '<p>Hi.</p><p>The lock screen is implemented. I''m using a separate <b>middleware </b>for that. It looks wonderful.&nbsp;<b></b></p><p>I set the inactivity time to 15 minutes but this value should be dynamic in the best case scenario.</p><p>I''m closing this ticket.</p>', 5, 2, 'Minor issues', 1, 1, 1, 6, NULL, 1, '2017-11-11 14:11:03', '2017-11-11 14:11:03'),
(70, '5a00a8a338658', 'System settings', '', 2, 4, 'Last priority', 1, 1, 1, 6, NULL, 1, '2017-11-11 14:11:38', '2017-11-11 14:11:38');

-- --------------------------------------------------------

--
-- Структура на таблица `task_attachment`
--

CREATE TABLE `task_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `task_attachment`
--

INSERT INTO `task_attachment` (`id`, `task_id`, `attachment_id`) VALUES
(1, 1, 1),
(2, 8, 2),
(3, 20, 3),
(4, 64, 4);

-- --------------------------------------------------------

--
-- Структура на таблица `task_comment`
--

CREATE TABLE `task_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` longblob NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `profile_image`, `password`, `description`, `remember_token`, `inactive`, `created_at`, `updated_at`) VALUES
(1, 'Master', 'dimitrovovich@gmail.org', 0x646174613a6170706c69636174696f6e2f6f637465742d73747265616d3b6261736536342c6956424f5277304b47676f414141414e5355684555674141414d67414141444943414141414143494d2f46434141424365306c4551565234326c31396835395531624a752f325076392b363952342f4f644f376533514e4454674b436f4b4b696f716a48444f61736d444d715a70536a594259566b434153464554794248717631486e77584e396556665774336277524a33545976554b46723736715654746a6c4e62474b4b574e74735a5a6e58785a612b672f3678397a4a766d7574624f78535235547959754d30636f6d762f69584768306e4c2f51585342354f6e6b39656e4478722b436e364c626d38395a2b516643576659667937364358477843704f50736e517039437a7a6c38686556345a6570756d692f6e784f616635626634436c693652444346357a74464c2f505674786c394730786a387861336d73666876786972366f5256665576506e682b4853783875586636576c415355765538624b527871614e72315478714634546a7777667470676d736e41614c6a47306f4c36575359586f646471576d4e2b5a33674c4434537571453147385843536c7a6e614266396f4d674e446f3866725a55413842703456686d4c35732f676c2f4477396b6d796c30533735772f6c584f61746c6d576a35654c466f4a42702f304164625867372f34596f3278574a35726177792f576f3170414d7270544b4b5070755842695047304d50412f4557556b576e4b347a617350413348306a426f772f336557697736375466396262457a686f544a3869627932355649455933554b68554551385a6965586d536e7862377958736e773745367777395a2f6f746d71576a594a49762b4937416a32677a4d516f5839535758526b6b377761794232504130764a30613053675a684453626c426c624c69436149706d7053494e356a7839656a4439623836534954504b634d4c3163734d3757614253583269695171347569436d70625258312f7a396258463950306173745451587a62736235416b32554b7650736b2f783049413653535234746c3564566657687132554e62526552576a615373595252456230526955544559585438744e4368486d69546d61485a6459516f324341634531615244382b52352f74654263746d342f59734c3268617a762f6d314b6b6943497231736f6e696d6a3774644769447954515272487434492b6a6c394f694b393747524e6e35773257506e47616a412f766b313078703258552f4f4a646f725a4b786934314b6c78395079446277687672314a3948692f62417368565a4555786e2b6542765756374542554a415a65677662546246623947596c6c747643754765384766565863694a307365696f374b356953665475524b3743417362726f6c68624c593151467450426b4c424673794a6371534c70415150482f6d4c41324d573048697738796342554d6d44465a6b49506d426b5a6e395669557049335a6a5238422b2b725659374e4e32756955724c653742504976447465436c4a414b307445713664695757787259516c4a487879474c524a71594848736f49326b46564b304e4e7253656a6d2b6c6863674264486e58544442304f44394b754e2f4b69556977595a5a61566875445a6b554b36526447434c636e54564275525031304c4b576d6b56497736635a2b445373472f6b4870654677485575794d764a534b782b6b525557536a58464269594d6d6b3065774c43486b5234494256736b37426c77327a59427448612b556f7356686d5168655273784757475865514a59477933736b61303543614d58583032737762774f6f77765a58735a7553685a5346682f4a4447455836484d744a7873696b4e526264706b3541653946697042446b6b3430675065626b36746179526643476c78635a674553736f546377374b46785a567079476a65704f79324c747a4e61334555735874734f6d676c5252457432314969486b66564c6c46334568422f5376464d385354676443347365304668594f684e57314f395645435852497057364f5034496674714a2b415a6b5935554f786879614b4c4967316b523041556a41794e67414c5a4c764763616b4b523752345463627741324c6f6b31667039683630524a376e38307179552f494a4451305752367946714d3265486d777552712b67636346584f4c686c71557455786f61374947734a5438506e387959576d566b536a772b72564f444671616a78576d4c38426773736b7642446f77654f572f7969426f376a4b755159744b62764a36774665665857514f7a6f4747466a516944467357476b5659386c714175413275665955767172617259432f46564d6950576451572f704164736d722b436b396742656950585456366a574831677443795769335a4779535168622b513641704c78303761445330616d6a5a593068714c4c6b6873785855596769674b495a4a7a444c31444f50367a34455175506f634f69776533366e303547624f47474e54354b6f4936566a7858597a6a67676f4a503079384c3379336a4573384c2f774463476e32394536573147695366775164384659684b737376626169594169344b32676d6b61754c793554454a55523632736f49734579706b464e4768426f676430366547306f475a6257677a4a6e4d587865416d646c6c694b684e42474e754243656d6c6465466758674941516a4e7352313273442b42304133344b3244584c476846626b3342496d55504d674b726943714147434d4641517830476778636c454e55566c42734235524d4e595363772b484a76594955524a4d4862416e527777385244556f637471495164537756315a51494c79584756416b736b674359494c375449326d6b546a567742635a4f416f494635414137586a476d4f447767376c5355444f50683255384c716a7542554776364b376c4353745244594d6f6a6b4f555743356e2b5148524c6f36656f4e5359426d4a79516430475a675a784b483443434245336b567772492f356d7745526f69763877335744744a56494e36434a564e5767542b7a31454c684c4e593732686773414f346a58674d6c4a37426b6b6d5a4d6457526161736f4e34364943414a42354f335a5453484f6b48725a4f666f6d2b4d31494c4e454d51434851784a46656e69684957374b49684342624d4e4d7056624a76353044673368676e6a7045502f4c4a736a764d6d39694235516f45436b414164697978525451525161394b4670704836534f426752635052766f6945757a5151533559697a4659524f36515644646f73746741797864374d486c737741646241464167477733336569466a777a5142625734476737434d496d7736596f74345241583677514c70307962364b45677a554d49727441367278786245453048427a724244516c415863466c594b5765434379557568574638654373375a72477162496a597568483559494f76543555777944304a686e42713469556f346b324e727861356b7a633677504e414c6f6f78434f41536b6230427a32474146447763554a413245356773694254484b774478694e4a6f4d4637634d777a4a52454d54366658504f4937724653624974426c7066386f4c6b523471746a307375307247704e6a755354775759674b525141757649364e6e4c496a78704f5a446879304c66776376773271724a617969787a4978724a62595851506f4c715a657a496c4b706873594379764965414171612b6142744131774169705057496f435977343862426953424e31704c433553704e68393634486f33694e426934477851436f5266714559694b447a712b54535a53626a4b3245626977456961513375774d4c744236314a676e316e774b376f775a5745493963446977536c34326b52762b445669514a494a32764d4f79544f577155714a515a6566444257495150626847424f554c77656c41304f34336a745366316a4d455342306b4c456330467377303871416c49433934533564435a38597468324d4b4957534179755559597645525637326b47576c36517041324743733443432b5139514168306475785369686d4a6b4843776f6c7143554e7577566236674c344d594954386d4c357068736b4e6769684c444136694a6953707968536548506f4a564c5553565051456a73414f4d31596b714f51646a6b69335832356c62636d4149634e366c7779654b456f4d6553566a457338616254446f44613150426f5958785935424243427643505542645061304b4978426a44475245323468325261465270434245526a7279716f727a6b66776c54613168584864593652646745525a32496f454c737753717352434a6b34335251653866786a4d4a69436f555362456e4936365457525a5242345445505551626965797970754143683948795741355a4b577733734636774b67745442774e4d456b364d516245737979456f714b796749556850426b367030756c705a4852535433616b577567416f4648346d457a433463794879457032566b536e344a383065485a53736c534132745846594c65477072456b33416167303157577955665341456e7370566c2f3966394d4c46733641334a494549426758476c71476455414478536b62454b582f7a773261423443706c4e556d4d795152764778666e4f59766d74426a7841655335494d57442b676c63325173393778622b4a414c75574769716531416c426e5347526b594b70326d4152453342795936674b52675577557843532b6f773674354470514f4a4f7849744c6e6a5154766e5771376a4271494d6237384d303363702b4f445a42506246423543387354786770554d414259544466695146443045706c4c4c49464d6c4879744b6c6b687a7961577745574267553943546f423058587a5a5a66794b343950546c352f4d796579573633312b2b3357324b464c4f4a624362657452424e4f544949537a6958495253416a695061424a46746d476f50706b55754c6d3856415a61585a477172554456724d42315367596d71626856536f734b622b2f55696e5a663436634e32304264664e6d7a6e30794f455848746a3433692b482b36322b4856417370753345437345657333716c6f6149454b546249676f4269537a74696d515469475153504676524336526775547764716d5755564e73453668436f6b34745a59784f484f4e6875506c4e5a7558333362756c712b57693557713145304b366f4f5a3664486e35375946726352775444654833534a5969704d32414e42534b52667a67554d6b554b5551577952576d6f4e4f3653744d504741535242657a5836524a71516b49784a6f46586f544f6150654b376c4b765a6f725a4b74526a623669537131617131574b432b2f347879306e4f787142366143375239696f72566849423839767752615a514252425454495976435357426d477446664f42744b6332776672676861444c676b49354d423063765a3765644856557279567a69455a71555653766a34794d314b755657764a514e543939326f4c7448566759534644676850417041733759664247344248336838626851444e346859764f41547957634577414b76474944307869636a4272594f4531684f434146584a5a75397235636d6b386d5566662f2f455438743251577457716c4f6d31307a747a707731762f6b6e6a585348787345634944347a7161516850576e7543395a4c4c43494a6b4f436c6d4752466d35384753414b586667725748346739464d4b547878524a78464857436d624f2f592b50354b745a626f52565250786c39502f74565a764a5a64506e746161586a52633366666538674756686836473467686a3749486243516b4c3131524364447067517963696b583036454c4569784265695a73414254524165417249747169633052706868477672373763756647654f333459614a6c416e4d52755a643933436d392b2b383435336e706b64506130303448785945694835417555616b6d6a4b614d6c6d4b42415348434959546f5a614463435a777542423151667a7a5236593935326f4c3256416b32425744726e30386248444239637647363251584a4636654d576f4a33394e71342f4f7170584c6f3873662b76536a62522f756d585147534962747079416669656b4a6170417667464e4d41774944496646574d6a4f416c4d546e4b7937676b46415078534f387a41486561665a57417767495044377471724f2f4c4b6c574b744e4745353377737058386e76777246484c3558436b712b77647256312f2f2f4f6448762f6e6771384f6d493256494a6f57535a68444b6942616b4a5341694b6c687a476b774743427875546b433552666f415353345a6357715177464a782b59716c39424a635a4564397572546b625656696262504647665679666662434f517375323744703335382b745837357246493256366955533947437453393974763256353362704945592b6e445469326d78497547725178704a645178515235736965585574344d454154733265437a3941577049517a5575566a73574b345572687973702b32643378356f566850644c78574c4d792f3734756a4237662f65754c4d7954506e5775317572394e724850336d3763657558566a4f6c63724651766d4f6c392f64483274454d78612f6d485134476e445443506b7536696a3649376d6131507943774464595a7876416f6b6c334e2b7756554267543869454e34442b77505835316a6e78486f586a64783265376e58617230327261706d4e347157797a302b7535737a383973574a6b75464c504456333855724d46716f4175716a7a31703756413361437246444b6f674d50354363566f52686c6955574974526b715a6c4257326754563076447a4d513276786b69615147454939774771703974695349572b7069746d62747a643737445a3936554b6a6f5a6d6b3568436c30374f2f764846464c6c2b737a583533306f664657677953696d575246494a3179334148556276314741552b4261504a3846755947673065425945316154396e784255756a69304d63535962452b487574656c303177335661794f566f5a58626d6830746951615a674e53574a5239486a7a656e4a6a352f65486d6c64504839765947715255324152596e454b364a63724231413172424e43464c494e57645376326245534348786f31504c414a6f384256324d414252306a384d722f336a33314e707372546f39502f50315270393251696c454c5252614b4670497862557257766636453539636e61746564394a785459564277592f506577696b5a4334376d4c53413550326e4b71342b38747776596e414c37485268526c3279685572555043684d344a635939494130624239624f624e61713152764f7a496c68596b7165434751493334324d57562f2f4e6a646c507267386e39733638536356706642436b757642417370687473436b5654675430527a69476d6b31366270477a477a426b6e743143384b3235696164454f786e394e73366c68356d6f38737230576c2b68757470754a614c3856466c72516c704f7630702b7755726170537663614772397473596f6778635347355a4248397036794e5461745854596f674361494d6f46645770794239576b6f4a57567042416341306868704743795855626e784a725a61647637756e656456695037675954434d5479477a2f735645304a39666863464c3057345a68775a54726c4b4554715a6243757869636a792b7141644c6e41452b44646b644f697268354d6d4f684b43546c6643566143316a4739765a556f2b7979773332524a63587864717a5a6c70427951453345594c44654a4d5a6d4c413649546733534c72797570444b5363676d4b51705a486356795855536a746f31636c69774d3470695634425a325470725739755759344a415053434441374538734c325a744f74635539456f7476347067316862473679425474466f306e39672f474e4f7551475554686775516b6c6154687845597035494c3841496c5a5464366153526d314749465a674a514236694c6f6b436f6134614a53596c34596f57626e6a6b76796a376d57436c59574a41622f382f5644776d6b72465575526c364c6371394b7963794a56536f6c4253584e7645446d444644424d503430786f36576f4952323452736f704b4c2b4d48627147684751612b584d7330476f39646d6e7579553654644679736e525554624b484c79656f7a4e51756f7072694f6b59595a7738527270694a6b4751597153345355496f5633477669564344714b57594c574b704d4f2f59493479514b7249366b546b714e4f386776743571325835462f7657554c50707548334e5935354778526953552b454a337468615638556533776a4d6d6245494174346b45565030617846566c423469764133505a5778674f3969766f554a4466566c5269797468473444644a6477354562675a744d386b537539312b64305047733632646f59344a7356335774447a446b4a32716530366c3845517571765542484372476a59646c70715869715242736e645a4e4b776c63336442616e4b63476841584b414652344b794d7a454d54756c4f3938352f356c36666b7068425268614c69564a77476c79374834734851646a4a6869766d4449464f3939726a375a4153426c6c6c4b5875596d6a51476746546b72304d654c41314551456d794e55485a44462f4a4269796e744a77633050314436365a6e3137557453444e3266796f45446d4b6c6c61557071646a796c75457078586d4f57423553634655366c58467245585368767033714d6f3149553461645531714a4a54376649487552696956694b67756d497a6836593372375a325a7a7979634a2b724858567149476b686852577279486b666c704a73546b654155397a46744370326467642f5146695a4a304838493546346e434568324a512b43684743774c705a386d47517879536468775075637a6f44656d4e62596b58353778617a50323273314b456967714f4477577252694d747747365a2b3168444b5056514768744f4b36414e5855776a6747597749665232444a43385768594b71426a46777774556e6d4d444669506843595346584b74667731585a2b7a70424c55325274615a6c74642f4766474774474f4e41427a4563476d754a6d61706b37386734635a4b6e6b573255485266632f594e61515975425a515953754f5544436837456a755470725731364a396d6e5163363066334e5131483234796b7648596b62353171486d45664b6179594b546c4f4a67776d6a6a6642716f51525347735171434e66684269576364634b49435073596e4b532f666f61314f6b7941306f796f547454425869414c5a564838326b544270722f2b7563584634627461737445694c2b51684575646e754f7156704333574d6b7574596b614c516a6a5157324d6a7746306d70564346617347657054597070494330464848706c4556525175616f6b484d7a345067436c6b6257514d496f4b66376f62786f75586a336535485746676656464d556f4173456c464b456141676f644d4d413851533670686a41315834456b6544426c4d726e577849544d59672b72783855684b43456d7059374349556b38754b6b652f78746f45397472497759624a4a5958717271344d685243674e52494d776843726d50516b454e354b584172507a704362354d644e43437146414c556735304a324733537036445072615959444e745a724b79355649647267697751537742687764357a30382b62416d616e4e3266787462563558706946595879327642565a6342624246415170505646524a664b50332b7769334a434b694d414c474a6f5744476751766c36306e4a6930546a6755465a3572614f3831654e4467646b356f434b6836697858474e4b2f5056373973652f4d586164657a6b75473533652f327056727672724f41707355673666496366496c365835632f79374a67497364344b73447a624e4a7365716978734b464f434b75734d654b4c426b3059795a68334f734b513743446b306d4a7a75765a4f747a6a35425a374536586676545578767632722f396f786466653354313756386437545973786f2f49586f6c6a30634839302b6245736a3130366770686c3152622b636d354548504a3536764163664e4d4d686f386941356c4f3941485952336b53306b6841377358476f6848396d353851546e2f5149644d783534584635646e58662f387a61737175654673495a386265656a654d545a53616e412b7a4b434a7152416e777679517142557870306944556357696763334330514b64356d783462544d4d4e4f5341436c77464e454c77566d6f2b5347596c346961735a7335767a4561566e5a336b4a6563667a4f614b3164724d65746c6e51364a36386e392b39663566473459424330745a4c4e714141376b68484659496a6c6b2b4b4a386a333032415866446f484273616a647138324474456c595a6573414a47546b77476368527748696575544d68704e4d63576c6b73722f4372336e73703766724657716672456c43514d3132382f645a547a426a6a624c45597956696c3444324d5042336f4454523367314158306d346749776d4e79393077485364687630794d3334745144727a4c4159694453704766374c313961473937515638337865346572745367715a724f3558445a627975644c796235553571312b763846426834336a414b38593145394f3646617a31326e3132395a776f416862344165705441756d6b6c4d41494164646d6a544163433370794541477930704e6a526a71415a4a4949386f5a2b434b72505056554e6970386c517a6c316b747174574b75744f4b65743765382f3948473739362b5a6b57355642676458664c556e39615339776b4f554178732f4e6e4c447a37382b754e33724e7079344579727959557a6b717568525433315a53694e31364c624770476948625135724f4f77576e4351426c5853677574564f76435530672b315773373058737457352f335a374e3539616231617575725a375847333330754d6237766e314b37742f39352b3876754a435346693259367936342b396739536e6a2f2f2b566d49557375585232302b6362346d58357933527072316a2b627364457852486e6b464644362b694f46355333777732414f6447675641514e7370475341624c444569696635647258463471334e5271623639456c66723972742b6978564d55714863367662617650514735797158637a4144353044327844306c345845707351696b372f3832546251714476554d782f4f54446c7a636b636d4c32462f46336b5030306e69543065344855704c6c494330597654415a4a4f73454164413669733638615a5a2f757556583557756e4b4d343747794461447135734e6a43365464436768596e49732b577239736d516f58356c544c5630382f3069376f535a56444d426c394a48614b79315555344c32385165666e43522f6e495166444c30795a4a6955536e4d4b52696f4b4d586e5176596266716f495a38492f31583868473254656e5471307356425a73626a4a6e355a30433055457831414c47436b366434525670646e6650327256625035302f4c62706b52647870373933546b75672b626d395a50547237624a4d6c42557074466372466262726b744f4a3857457844646a67305a71636c7a4c7a41546152626b4b3133624d6d366a325a7268632b6d507077377637727a504a4f59716654455770676750747048664a306d78685a637062397170394f4f547833642b744463793934396569412b463875782f506148513956354a356f6870715031643669614d536831457239424246304b386b556544526765436369435063592b634c37596a377239723378553239382b7357586c316364645041436e384e4d5a5153412b5a525859654958496862584e4e6c75642f756e5031333939313436545a7832397758592b4b315a47446e59644f68744934554a6131575548684567542b6144524c6f4150767348444f36626b6343376668445262434b4f54366252754c45627a7a746850726e7443677679416e386a3978593164536c496b4744744e68365a433669517853374947727464556e3733572b6d56726b324241643075756d6e322f5a304e6f6b627043346e5547696a78705a7a4944626f4a4a5a7879564e43596344512b344d515130677047625a7861576939633438384b4b72523078724f4c50684e395765386444427736534a306133415054425153744b576c683131707a616144702b4e5474376f796a37516f2b694b526d77525a676e4f693738756f6179687978436543573977684554794d4e58416531493343492b7337327a5573732b314936627a56624d394b465772747471646c7274566949742f6134396374514e69427172436f566e7754554b737933644d5a52722f37624e47352f576b656e56334d4e6454737a59774f514d734b385754416b4e4f774d666a37524432414a553557706b513754595043766c6a386c582f3933684b506468313538392f334f387363644e39616273735a3248397533363574766476323766394e483371734f345057624848634974327151597555516a3052614c332b665474726353455066376144583757462b375644646c4b34496a30656e705953504b626a54714761774a79435431686b692f70365661637533654134567166582b62566e62486d723248587637676f57766e354772465569355871685347737066637549745668756b4e5755754b65354636517a756247434769627236325045484d37745463537662356e6f6141684957574a496b633248597956446a45734d70616a6f4659706e306b326c49364f444f4655787a654a4530734c42645745597979385a7253796c6d584447647a6959387635664c35584736346b437464664f6d474469665a565278547743754d6962685762414f484a575334335a4633586d6e457968326656383239336d4e79797a4871462b61456a3156725062436b57764c736f6e517777514d745153794b584c69304f43672b65647a5738566e56374a4d646a6936654c47514c7458716c576877705850335530302f662f39536258322f2b6274737659377a42374151704869474f56377252514f466a7a316b6f5433727235692f3376336375325a477879307535743370414b4a4c75453148536944766b4b4b61696c6a754f716a75313649476b7577595943303776794d6b7472675231504e5032675670552b71376a7257747265326e617175747149364e3366336277737a506462674b30757431754f30466267586f6a41544b53334f33336b6c636b3378794b3172687133487551492f6438364458786a7a6e6c346a73395078376b7a345465744147307072364e495171625573732b474d64505170374934735342676534772f6b742b3657334a563263663839536c6376732f766d6e3173367366507454382b362b2f75705246553642474645684842627656326e58393674553350506a6d3570394f326e37585557676247386d324e653265526f4a2b743566712b552b37686c4a6c63754b6152594a51675761504c44586b796e4f2f776a52723644474842516f3857496a757055515a344e492f323339694f486462697850427266386375626f574661355966664f4439332f6c6e32584f4a394477697568744548515448363237615548756e78666c7079392f374b4d5453577a6c64417a434b4a477535502f75522f6c612f73337a4a43514f5664654253394134474b4f6c766c6c6c59705246344767616b364b69303467534a634e73705944596370314e37374663626c4e5077646534452f752b2b2b4b7a545a2f662f386c767533372f3845414d462b6e4851744f4b6d524878462b7a314f3433447532367356484c2f2f632b5256533866615853625174797a4c6448644e2f4f31346a58493757745a5862515549707a4c4c6b38536f786b3558575654536b7a4f31316c706873454b6767414e3741573578763647374a772f6d79795a667343323030332b61333236376365395a38594e773139535942554945794f533573576b3254452f372f3379725373584451334e6e50764d49645754366b6e616d7536473456706c3174456d5469346f5165453252485744345169564175707750706f7a75564a77773131744a42414c33744941337672596565724a693237764f734f72544a77307955326e335734787979453541677131664c6f395a6c31706b4c7931504d5a74643662552f686e35596e6c342b4b70394861736b6b5a4c73794f5046576e583634535954486c614b48424236423159364a41597941646969596b5a59566d6d356b764a6135452b6b395137696b7873756572334c6967437157714e4b51367551774f4736644836514342574b7553595a764b6a594e766666766967587a6672484a64636461786f68345a7248353163546b2f683156387452576f4d6d496e4b45463431666b447649424b6f4356584136486265337454466e4473546c572b6443366965353144585a6e52304e5243716b72555175384f4b423241307a5a4737656e763256653559306b70436b4e62372b33734f76586e2f334f45644f79577536572f50565771323272774f2b4d4c434734636952684a706958544e794f4533634e6c592f786b7454426c574b5043777953636d50356e557a547a714c4955713049625336436b526979434c495267707942332f49304c6656625056612f6c6f2b62766431584f38566f6e7131386e5048444d5a303041714f656f48464661635677765341734c514a62305a565565416844422b756c43697a66647571706b464b67306663694275536c346258344a777a56574e5266475543665a71732f746a52796551796e5a594636764669357a577539306f757174657a6e3359313443756243446d4a6a576f366848324b417173512b4e4650446b4b3454416a6b4b39514b4775496b536e33333051345958566c37516b2f734e6b4c396a4f497662574970524973357765616650486e776a32506637396a7a427966674f4d506c50376e7a5a4b356571396233744932637749626830636a473879466e59537234394c514a55696a6f672f6444425a4b59725a6b4c39566f4931317148647a6d446a4c6f53746c30462f65433059524e356b6e41453130676553316f546e667474313465665470726a44584175766d436f2b30672b69716f6a68316f47655271636838464958536a775a7867764f71536b4d6b7a774a5a573367504153664279434b716e4d533734337a71565a4d343878754a6f6b787549703165785048476f6b594d79474e492f30624975524976457067326176625653445462684961656578664c31654b337848746b52715a4b3252773673474f5552524f6b33354555757754495832564444584e71684b6f4a6d525635634464396f303145434f4978412b456c6f6b51326f6665334a68656547504852362f3766593646724737774b4b30716f726d5a484331396e322b6444693370512b2f45556a516b45756d655955792b517771415a5452417a6b4671442b63686761355168454a43593373554f4245564b6946356652484d764c6d652f564c7931463235696e723162777a7365324458357064356956696b55624a7a62456753714c586633626e3062777671742f62516657696642673455303430613154723034464b535241696657366b61304c7167706771355a6d4450777a476d645034556c577073444730344f33395135365372772b39367446342b36634651356357563333566271656355577a45446e6a426a4648363466397333354749566d6e706849474e5256556475695749774973333849654f5566386f354967594a4c2b3644754737484b635032524d6b4657674b424a38674a52356271676166444737594536734b3256777079742f51546e7a4f73586f6869714c43384532482b75526c417a6b71624c734b6473442f3362347a6e32447057317669614530594144497a59562f6b534849477043383843584c7431436d42367a5546757967554e6b6d7336774e6d7063444b475a45334156344e356337632f4e335570342b7476574930643033795950666c34636a6e6757725a386b66394f496171536a6446784373786b74786d64624557355237735765352b6c5a4969467156436367355279763273354565345a615778384c37496f57677a344d693573516b4f41786b4e4c6c53306c39625939564546594c71502f73396e663364612b73692f563735366f6e74386362356379705771556131633845456c45304e6365596f634a70496879586a4f4c53314755666244726d412b776230326c42476b4a2b696c42526d665a3966634d635346426f796772726a4d6c434e49576f525134616e527930716a2b4d6350726e2f71736363673979352b5a64366d3544557476614b77624d7661422f66732f76473542615056664f4769577a73537168755975466a69344a41636e627938574d766478363065324d5261574e31423978314f787974716775543067474532412b5635686b74566b64586d5144363148446837594153517450574732754b6a546355686862483973642f395662756268765031306d56377a336436343064336650766c463739314f515569626c49784b7445575343333536397979636a6b3630456d545031592b6d7141735143336f787551746d584353485355793842666775454a357439686d446945702b6753356f396b4e645865742b4f392f4e647177644235764f43354c2b584c75706357682f336d326157797a32662f72372f50627a7872513255796d6f4b614467584579753862793072546e5456714b416b4d706d36456b585257613658476e47746741485377724369394a7956476a5461636c42773432387952697a69496f33546f77656b6c785a36644a4a4851436270567a5465634c67707a742f5862336c58652b756f2b453776443337313462485773794546545351305636573341324a566b4364327065647530554943476e6e44516255764276345741505479556a4870507250766d7778662f5859637346316b564c4c61546a346b30585943485a754f627153325a734f646e623958724c784b37584f7435716a4230356c507a362b31766a717455796e56374c6a396b3973585468565675546c334367593431434551747642345831756e4e6f5a4f6a2b5470414854747a41396c77495057526f475370376c674e30456e65496352586a6777343161516b744473597a4638326c44496d39326c576163334476356d32563958754f62786e37355037356d355a644e76663974753538656655474b52426e2b6a63426d32324b585550464458424b774a2b6d73367330665864544a684c4f6a516b325959756b454a6577426d5773436b52584f6d574a314c6d4579776e584d41686332413645436a332f6e755a4e77383864573333547a4f724b6135592b76507169584356584c4534376c457a6b71662b3772514e436e69326342327371526f776c466b4e6147444a33314833336f75733753586a6e704d3458426a3945476a623452536e74794442324668756d4d52305a4c6d6476634169543751554f3656707535687069774d37617039373734655a4c716c4635397652634f516c5443394d724e2b78327072333138704f6352746368716a5a555163662b6b367541685a35586e424a57336363756572596a6b454a347356436e685379685254474859665172544c65565968796b45454b484c62466e5574344d74303535624d2f6834687846382b42742b73774c325871746676316c644b4131756d706d397636506437765733672b366a436154456265552b335653792b6c783842464351757151637261366664382f58756879725243614134574b427a67526a516951667374416a7a6766376f6d76554f416c5942466b6c784b37696c595849554646414b4f7a2b5a482b4a2f34515a61302b733136644f524a6c762f31333564615054356a4f747430745471394e61767648497966484f4d47765548584b4252744b574a6559596d6e542f4c5838584273494d4856376c7276624272737061546b753451697968784e4c495349427a7058447372695951384d4f32684945726331504435362b743068486a4b4f6f4e6c4b727a397678392f6150396b3559665a617645357547376a377758302f3076702f6f745a684d4564544d4a79386f706d395141616d76476e687a53307572324952754465456b366f56786f6b627855305a736b3862785555744a7439426869344d585867336e6b4c546a2f47704b6b38546146383864583147713175722b78487279667a54336e6866765762622b3932367672574e6d66436430652b334636303475656e624841616d714973553566734a5374453769624145666d773256577472306f4b6f6536486f4c4f3061474b424e494232336c4a4979384c42774d5461476e6d462b30455741633474575469705a30372f5873534331584b65567a766b426f614b67594c627a69706a64652b4d4643736c734852724a333759744779364e4857374670537347326a35473563466c4b415458535559684d4463445451466d50634c646546616a574f6f4e416c70735578534639485470736355375570506e35415377444a6f6872393775507672482f78582f763372486c7966587248336e692f58314854307861632f7a6e6e38395232616e7239502f2b4e46743436764e695a5769524f2f486e397431743031524e7830473543746772354f525572494a6253436367797875435669304857444a70425930595968767957774951513077644968704f4f796946304a383561647436622b65706656507462712f583753622f3275326d7634342f694a736f3865485848333575335970436466384832654b6a62372f31726e722b7a66383973764c71332b4b576e546a68597545716d517454357467523657786c6b555351757a31516353737a4b68593931386b6f5a41435054616a315339505a61452b767054324d68612f5277766f704e6d6f53525977644f54505a39756e454e505146516b37654d7662726c6f63575653395a64554e356b667638796f3274472b2f632b4d52567777742b334e4f6375504f506c6b70354a503878762b3266354a4a626b78353952434d4c48727957413338535a74674d44725678704534644246415378426d524a4c4377664542613875784f436a7934644d4741457156527835783655734b75534e70465548326e482f2f7930764c38384d4a626231333836746938616e375749394e6e336633686c39336e482f5769547658486a46526336797a7644547376677851544a324a454f775967752b587a374d46766937506a486f594f724951784b4271787162344a5553306c6f6e4a6d357478334452795946436e584f4b45676a4536375a37592f744f44693465483154382b2b4c497279705656767a5868772f35333134793351536d536f2f7a7a5330677156307a62305232476e59776461507568776b6953444f4a3570376e434f4a2f5437686b4f79734f476349714846346e72646c4d7256347a4434345751566c36466f4165702b494f332b326333586c67716c6d544f715558326b2f4e49317a78387150647952436e5443384e7039736667504a33425578466c524b55656f41655379446a326746356b5556686f304154495778427a4f662f4f37347544766735654e7559384e6c787a486b77544f5158564a566a2b6371744934304e507332682f577a79766d4b683448544e2f3939376d46622f594361764f766964632b323844434165745a4855772f496b4f51626852775a48526f735a70716b5a57674250735867474e674951476f73657273714363624a496473784251794159495675615a4f37464b6e653372486b77747975584a55765079337a746f506532795565482f643455643631696f306b32657769304a4c48476154397059614f354f68563767774753566e6579536c36384a47616361393948534d6f4559625468585163474f47586a5a4161595861707552564c52436c5276676a322b6d4e66666e636f7168343862712f4e722f627066737943484a736231743578706b3451454d303365436a364170396f7369554f6879327a4969483861594a724458616e524847516e4f626755624b71417568613144424b45644e354a79564a4c6e344341574c3073524a2f5731446449436a575a716b37586248663771392f6c553778746b537476537458395a7354435a2b4f6f5a2b3876454f336c6d4e33486f4b68736c4f5a5743594c46503176496a574e7030775a796b79674665587145577a313865614334744f50527634554b4668764b487338544f367532394d6b6838636949425654494c4641324f47336268584a65734e5866656a3463742b61617166343952567979396f2b7356696a755a6e4a456b5a48546f7568676757364e494b38634a356453574648467769476243784841777844534d6c75434730514b75657a7564502f3769764934696438346f34704f6a48306e617765574a666450755465317571706151716c3651697a5a334a316f6953477652693462706669795164314e4c706b4d4a6d33784a4d4c337a70344d45666f38487434455963576b6f5347497264396c2f2f584857774931337851634a705650727230425843786730537275617663332f7137687a5873767378626d63677853554f2b454a6b69346563435241516873366b70746769463650784369336e73314e715251664b3034684173624446484e7a35547a6d39366630742b3359334f4a6855494665523677565071765659513235483037746e376e5776743447327852645970564d786c39754c306249714b5155554d544a4d4159666563634c326f6d514976524654436b4a365931446d7a313932557073516f524156616d4a4f54366c457162763230426e35624959776e712b576b536f2b6e43394a426a39696436772b4a365a37736d6b4467654a6c784e334f55426167455365716a4a53697571416a616d437430586242434a6649302f4837515577775468684c2f746d774565614943556c524f59477639446d4c6e465a445364347a6e485154536f6a5255485066574f2f773457596170714a586851316f565943577538427134647974484e424c4f313459615677496643474f7730724e4a794d73455738354869323356694f4d597631393771527355576852503674596839504763704976526e616244344472397363627a6e6161754645536836554470676a484b707951687a4c425443783567384464575934343061744f536d6755456b516d704976594d6e4742587877726e47766a54494e7978312f61597756424e5353744b2b665861666541784f55494c46624b503933342b53774666554c545371385a67384d3969427656674151706267456157486f6a7477674a5a2b563053443461435a2f774a4e78696247557274476d3262544a6b3136612f4f7a2f2f65464a4f6659556141545a7a4b5076464f5353656a5a5a54645034555931736151754a47472f414f345259563546476c793672494e6a4a57516c53684e73446973424e616e2f676a4f6649496d6e6f67326d56504e326c50482f336d6f394f366557536e55366f3376764e666233524157786c70575750786d7753325542474e2b79456f6c415347554e31414e43516c54747274416a55766a47726976544e697969775457327971564b706146456e4a76516a544f4e6777624a444653535178666e5076784632313531357a63667656643372506e3378792b5a7244597946507750364f63704b6337725479526950556454415a326f3774625271636c3943776e6b48724551476d50544f4d6948384770454a6f676f347563676a49726149574c774356494a674d3878305547437033364d336d4b394e4b6a78324a342b62486e3732303962335252664f32396d53417650374a63492b307857736f554e616d516430736f46762b75624e486e595838426d4e765135383746686b56756d63776761363443346556726d41572f44524f496f625476584a4b6d69616e5168674d686c6a70646d4973313555716d2f724a746537644f4f7533505a39475177394f7851704b376e476c2b32545a6a725a47527745787a6f642b463151672f5347306d32796b7a62754d374c306b466f5534535966476b49525a3741794f47664f3877346c344b415a4b49445758314952414231386354582f33794a6f7253376d372f33616d2b394f696d3961387348464e4a662f682b48484e61527866743252553837372f38362b65374931536a556262364f622b39532b31724a556276506c36546a4d5734785a625a7441496d63437a495541797163736d4863766f55483065646a53556c364c61495951776f494c674e7230324e535a632b385a2f356f70526673336d2f586244517a646d6e2f357865716b366665634c593959495476464332766c71356f7a44665654456d773037456a693535582b2b36556f436a304a30742b2b426b4835683779306f6846646251324555377061547473724c4744486641376371344131784b467a327a307276436f325178614a2f6762456e5473536e37364f7a6c4e4d7566652b3944627662533064584c5931793935312f654e7655514c6348642b5451756b74754f6f586a53743348627a33654d7464657279786e332f7a38346b5a3830334f39594f343534365451786b4679624f694f6a55325262614f3256467053594a4a7a454e7772646970302f4130696943737857444e75372f4e667a7131534c384f46787a6576654862646a4f4b76722b534c613774766c4864357569706d7465692f585674534c5a6166356b74327a6e2b35354c3364343756486d7a486431595363634f666c2b59736d4858384d57686d456e76357972304c634b595764484973487561734d6d416a776a36525a7a67626b6152444f6d3542686f5267594654576d383031755564457a312f584b677258587a523961385069385531666b613274756e7a6237797a59646676627946336432562f5031614f616d33303432577433667674377a344f6a3846543963752b43555535786a5366354e62507a764c5a7a643058596751635969454b6f536452414d4f756f6c745874795a7a484678512f425545674a756b686b2b6b6270696b2f5a4d357849625031324f5a566d314f7252794a78703963496e7a312f2b7a654a7053342f506e37336d5a46767a735549506b452b7579566272307837633374683033533837686775723175536d722f70322f673954676865314f2f626b346874626b715a4a7956364e5930554931485677456d4a336150506f32495647524a4c47666546556952586d4657313341624e4352774a742b302f6d6b6f6e345070397a5a70664c633837644e3266687a58752f327a623768733048446b364b6b585a78665076546c38386f54367650335765583354472b4b482f6c2f4f7273525465505872585848776c4e49726e326e362f4d6d482b796252444f656f725170674257703350687644624f5430475a4d6b787261794f6e464e425546727167306d5232756b464961544f54336e7372572f4f53566139587931453063753371522b5a6c74353563393953735a33613966464b4d654f76736f545548587077376f7a7172767143786675584730576f6c696d6255537558732f596e666248636d7a7531343673624b356a37386e456f6a6e6f41534e53524e584f57414561443869494b524451337841767779664f635245533642614335304a2b596455383144737975554e5053395675736a307a636366696733352f7154332b63754754316d425a2b656575587337574e6a7136737a52306547506f302f4c4656384d71673647745675576e7638544f65334c353462662b757938705a2b796e4a59394338526569456759457849366d7730646958443845526a324961504d3267304f355332676f6946485868372f3567546a65732b5861414b4a6b6f67316d664d506656547554686e374f627031397a5a6c70594e6e546675376a35337576664a744158545a35526e4839745a546178635649316d5639655a3139355a2f3368743174726e5677342f4d65557365414c6374464e793544594656796d6531364878413457364773574d754e6d7a43593148474e53466244444f6e766e6469776438714f6c38357130576453564f766c5957582f71312b732f48667237307366304876355a7771666e54357562484462506d756e73714d3063716c792b7579503456582f37507379766d3555626e7a597a79393752634941785975734974713450796f74354a5139546b506a724a38786d52484d64484547586d695939314345594d61486a4e57794a7953493035475259326a3978617069374c69625455617458436e4258464b343975792f3769346e3074705472554f5058382b632f662f7666496c66645652306153726543477a4a57354939632f5559307131526d7a613947305879574a6265516d4f4b47364e56516f57433538464667432f6b46385841624a6a714246536e4c41364f327464637253572b417753657979365967374a3264556f6871313761364e464e657571533835337475393868743139735437316e34313776373866654c6a7a56666450307237554b3158713358796e386d2f537062626c306635577a7653766c504d305144317a7738366768314b4151394c5053584c6a3649576f4f4c3551766b63376d4d52364737524871424b5a3948666a5439434f58744850754a43763170552f47376a7066643154667a4c5656644e3333704937376f386e72703531754c6834756a43364f6c5233772b4358695839796e6c7645716e4d7664446c5269347333303467726b49735a384c5a5a77316f4b385349314b5a6c7745684169725131364f6b7a7748494678364c6b4247786f50554175706e4e34646f58464a66487669322b5a763147334a3639637354672f2f2f7076332f376e78743356636d6c61765661342b6f2b5a45666635726d4e4c49702f4a396e4d612f71526a685875466673504b4971506b705336394a5a4954764969626b6d5a3053497345766a70556f43676a39774c6b68596a4e674f5045376e48495066584b73437830725634757a4c3375706248547438793462475a2b654e6b5678656e7a4b72547131586d726c34373437744b3145642f757579614e4f736a65465a6165744347466b6f4a474c5777572b416152744842585949502b48416e57596b767444456f657850466247546b586d306f6e79685158494a6f32557676685474325a6c31474e4a4336696d4c76332f74723176782b35756c517131694e764366777a6c656a75615a48594b356f516d57792f5361587050375352776d73674d7266426c5132794e723651545877306b497a6a30395061684e745770387870434963526e476a7162515449494532574547306d434c612f6b305a457a6456395a2f56387272446c502b504c2b49344b455139375a4d566f6c58574432722f517635702f5258586d41784e4f78636763575330332f324b586b6e5a57744d4569495a38452b4d494f55616374554841433054442f466b35614934625849415253396b69596c4f5a744f544c4172436d2b526679735663764b4e573535372f7647523956707465713035472f66506957536c766738782b4c4362385973382f6f7857746749675a52473668596b417537425a766a75385172487742474e705534514a566c4b374b3830636b5a57322f474e7567446c35476865353574796d536643745369316572565947574764356c73727349443567687457384c6f38454a55585066424664304b5950716d6e6b2b5077577471644d556f684b704a75515266366357676f66776248707742714c5667744c54326f754441497032454364574651484f6f2f722b485635497446705169327459366d4f335857434c717451694a46565972413672787256562b436b7a7852655765714e666e6e57524c65754b454330795670433446367257352f616d72712f506d7066712f666e2b70335730303452784b4b44494a6a626f624e2f77616965726d4b565146555176327367474e68385a54717635627a617833567350716b436e35714a4667316d524276554a32314a336c564e50784179796e4b346e72434f355a712b3954304a682f5750542f6c6a76333430517376507650556932397432667a2b523575336658766f724f314f54585545394f7367576b703057716f6f516b7359794938526c6b6a7557496744494878556b7271454e632f63584b52624b5a446b314d4f51672b656f732b5a58795830494d717656386d744f4e583242624178656949387263694e64786f423279314d6262717a6e736b5044512f356262766a5335476330652b567444333577774a3666616e76306d6b6d5a6e59416c5552594d502b67432b4e55445959725542436a754455445475377551534a4a334666366d4c7a795a524a7049706c693565626638625654454a69547a75484a335635744a66585350316e487152394241685162543350584a4439635056586d6a2f6257386d6c584c7857787571487a46672b2f7650396566616d55304743444341306f4f57456959785a4c4667446a4e6f364c7665787853685a624c686f2f4f4c6767456f61454b734b66626a3054305133616e786e72752f38746465384c377344672b48614d7a42357055796a465350366c6d5a2b71317051396353753863715657443170484c47716f73764f5039507a4a70654d79716f6269524753724a795850374b463044586d6e304a6c4d786c304b772f592b544a5768387371535972425a39694f69342f793878564779456555336c63572b4861376d3134363134734e395a63412f344c6c4d783432642b75446171314f713131477a5852497a7235647a515345626a6c424c756551696d704e6c714e38566c704d6b766858704b6a545936584a6b6f596e5a2b32306946346e6636474a3544424354696f5845743246327978506c374a6c744b6f54474f526a4a4f4b6b42554b50716e61764a572b2b6635656262772f7676637557544c76552f3148356c4276414a615739765731506c7572362b4f486a77794e71343758705573494a7952726759347347656b44497a7277424d483965765732635771324f413678497a2f696d4352425a676b50347233546e544943396d4a63326b625571465644644c454767317154572f4837487978784f5766744268794a662b4666723849554a7054377368486a362b392b38346c6961374f6d6276303172642f4e3632707475524b4e504e79364432685448767135436e2f4a394e4c53726e473175766e73636d747375466c707167716973345079675a6c72356c3055747431706d456c43635858565749556758773579524c33502f6e366832574643426367753865334c346f79677451704332493676542f6575714a363658412b353976695663755653696c58583772367958304352506a4d4750682b625a7566337a4c37367736794e5052356e5731334a5973574d51775237425656772f3253784f3337785378663551754371564e534841393450326b41785565686b50326e76753161747a70546830667a7047756566784b6b343163726b375a694e4c5a2f35506236454c746e37384d6959676969556d6c6f654775485256696c6144725a76623150663378743557667134554a6a385a625975586575766e7134574a74564b4a59713557716c7772496b3136757a532f547a7243375a327a4c49386a535547454171766f35446254626e5a7a54567648424f794c58584c4a70627241506d52414a7a6b6f6d4938334f323133356d5a46686b4c6b716c4f366f586870614d43386a555849556c41634c3436662b632f2f676f33574f4b6d35717777647a3930357650332f4c4b47343966732b7979325573585275575243423678467442694e62727a362f5463745438576d683549344b4d584d65435731356b594362764549522b4f6a3135446a6f67305a5553554c6f4d55516e74712b39585a4d714c514341374d51364631647a2f58696a464d365762416c724635654f7a353757325965783179664650712b7864336e6a76393831642f2f6e6e6d30327548692b5871534e577253536e35586b373074546f792f635a58753149715345572f44593244674e7a46686c7446534c30526541504f4344644e73376c324f4675746b316d4d65456231444466484d4c315436334a357874396b4e6573634979574f4f56733530586563696f32354e54384d7347373975584c78307963546f4b5167337168333165326a5231797a3366614b642b4c4e42352b354b6a64614c75514c69305a6d6c686666654d75792b6f7576376c4d4130496e513242504865587643385845424b3449437555455854676f6d4c7576456a68646e56714a6155505845617445685a4e3337655534695666554b7765386f7651565972625234302b4e6a364e6c7475596d44324368747a3231392f5049584a386d37784a5462745568396d68686355577863767a2f32302b5a3958332f77334e75483937332b2b5648542b76627a63386450473771317256424134334c7a5762714f6858707a5955474d316939617a705835453358742f2f78514c70576f5053646251634a612b712b646f3456494c417159476c4c30537547722f33536c3761326d6e6a676d56496e703375747a4c3875566a3752302b6e5642786c6e4a5758746a4a6e356f756e6133323232316d68316e546f7a706337484247526779733866335465707a4d49676349556f704a4745357154305339305578692b6d2f762b437161614138457448797835372b2b6e363048504245564930457743622f7a62366a526134514e54796873344f336d6d646e7a37686d2b7652444c5a4234716537454f4856456b56494363466f53396846366d4a5263723159685836305066336a6c396b3762555a38456730496f50755a764d424f45776c7878594e3235316e4f566b6e414155574b31576d70446f56516e63614e376d4e55466c396172467a3358614d694e4461565142495866586b2f62342b2f65632b587938733532326d4c47784e4c496b3655427a5a706936537367673444666c733779766f543234417672562b6a653768314a73445275573279395644674445786f73344b41476e355a4c6c766a33662b57715050434d61702b39366c4a76706d726576555168716f747135566b6254375143677945576e5a4750312f334a6653317a533745347371754e315170724c4b6659597552776a427850746e78305038596865595761726536503259736666325a6c6164502f646b36307a7631306b5063564e554e69677a5843426f45752f7433647870306a374441796e544e4c387950524342417038454d6959646e482f3235716e4363324b4365546e704b6d7159393248736e56717450324e4e4657484e6f7142623947437064515879387854434157704a6b38596130646f386e6e6a53353666732f44562b684439782f70324667734c2b647a4663374c385331586a4b78584d69375833464d66726954447a5578636e30755766385a4950585661795978476f757a69335a4e63705358726649466b614c66766f4a6c59563069413961343234694842594b4b6a61436d704a4c386c2f614d4e477467413468494638324e694b36767a6270707a52335458363762665066423869337162346c344c3475766f59324935517362706464324d5037392f657156657a31795439354a5570566c55366f52686b69307056742f2b595570776a6b615a68494a6e39332f386365445a6d517571396169387663746c5338454f344f42724b4e4f4d7059785753304d6b41547447683576327441374e5346786c31547575682b5974662b436a5a7a34633239636b314a4e655641344f784b6a3177723334584b2f33322b4a696c436b774a42454f5456426c62736c646a545a2f484b714568503353416f6955375a3635724f6970337330643670475a396b6e58714d7253494d2b3559495876784e5a494852734b554a52784a79345458426b567173566339644950483534314f6455374f79486e686c693575596d355956466a74304d4a657450644542557967634f55614d73376d4e77742f69787471476d6c58494e4376474e6b785032543838702b39707662354e6e356c68734b554a796867497044594b2f416331716341565534654a6a4d354d7a7963695473684565716c575731306e74767666483463537331714849764149584f554e4a5a4443613076583135706b35306868426f336f76557171565a422f74537543656444376739526f774571362f514d346666392f63316a516f767452564341546b6d685a765778416f39704a6c71437a666a55734b4d616e4130707247714743574b4b63456633624e3279654a4c766b6b7475306d62646b425a777331786b6e2f74385177686178457469725172746164507446686a30596b62647a44534276374c6e4e5058306d47445775574f38434547527345614577353961594f694c736954556147534e6d61546c4744764f2b654f526b774c313456796e5431792b6152723974744544735255736436415677366d575a70684a5a70434579477747374554715a613374567157477132703148774c646863376c47444d543236396f75526e483557665165634b4d6636304c58484d35634851424132437a43417178676b5a7474444e4a2b3565483153564a4c30616c5a6672655038444735555a724c614c2b55774f37746241382f44436b4b6e5767724954534377736d47696d6841614e4935623845452b65626a6658506c516935716336557276684e413763387532625544436e77683167324c3267367761734239387253624c6672652f752f6e6c754a524c6969686e4a36717731432b66657343507735325134596d426a756d7973554a316e4d6e4d48736d4731656d6e6c6c377650326a54416f474f4f507577527667593362757638554758436345376c79676e4862586249706777572f51632f6a31504435466c696f526143335070503676793079743564384b7856505a6a4f6b657277364c707a5064734675793669784644596845694d4558466d446d3847555769317973774466657166707069496c525163444667347a6d59364c2b5a5a4d65766c56654d7964435767794b4c37476e3258553646384f6c7273514779776162466f556e5050386c4e627653667774396d75426649724b6d395345792f2b776630505357677446543748476e49754e6a365a69504162524b7a557339636f437a66476b7a464734373448484343513072696a39544c5275694f6a315376474c6441636b674769447a5a6d7545383641784b425855494d6e645679666d6c387852655456355a53696d4b6b4e754b44796e707032386e58783846496138455a374655593642767048357970673158787a6d33526e70594a665635354f324d424f634b6738497132647058706a746965736c7a744c397041644b476b6a6977414c3762614d51563773624b436171587842612b72306f31472f4f6a577158654b744a6f2b45504943556c34344f78486647644e66377675724e6951424a454c666743325036615a333539542f412f49524769615445774a374141414141456c46546b5375516d4343, '$2y$10$IcRJUkwKRj/5qHbEPDyLvup0na/ia.fAbG94YutonLpgvf7cntIza', '<p><i></i>Hello,</p><p>My name is <b>Tsvetan Dimitrov</b>. I''m a <u>web developer </u>from <i>Sofia/Bulgaria</i>.<br></p><p>You can contact me at any time <small>here</small>: <i>0988924287</i></p>', '2ONdZVnMOvtgJP63lBZwxeiyEqvBxZI2W1HxQbmLw6iSroy7bpuLG1tU1EA0', 0, '2017-03-23 18:49:07', '2017-11-11 13:55:32');
INSERT INTO `users` (`id`, `name`, `email`, `profile_image`, `password`, `description`, `remember_token`, `inactive`, `created_at`, `updated_at`) VALUES
(2, 'Cvetan', 'cvetan.dimitrov@sofyma.com', 0x646174613a6170706c69636174696f6e2f6f637465742d73747265616d3b6261736536342c6956424f5277304b47676f414141414e5355684555674141415377414141457343414d414141424f6f333548414141432f56424d5645554141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141446d6e7a7362414141412f6e5253546c4d4141514944424155474277674a4367734d4451345045424553457851564668635947526f62484230654879416849694d6b4a53596e4b436b714b7977744c6938774d54497a4e4455324e7a67354f6a73385054342f5145464351305246526b64495355704c5445314f54314252556c4e5556565a5857466c6157317864586c396759574a6a5a47566d5a326870616d74736257357663584a7a6448563264336835656e74386658352f6749474367345346686f65496959714c6a49324f6a3543526b704f556c5a61586d4a6d616d3579646e702b676f614b6a704b576d703669707171757372613676734c4779733753317472653475627137764c322b763844427773504578636248794d6e4b79387a4e7a732f5130644c54314e585731396a5a3274766333643766344f4869342b546c3575666f36657272374f3375372f447838765030396662332b506e362b2f7a392f676d4c67736f414142724b5355524256486a61374a304c654654464659445076587554514a43585142415539454e4243677174386b5a464d6467574549564b4b31416c5342477462564742696f4a46564634744b4436684b4a5469477757526c7a79454b684241456370444151463542776a464a41514932623133352b755a7537765a78494233546e5a325a2f637534326553586562357a7a6c6e7a7379646d5174524342706f36356a466c41574c72644f77456f6b524c734b364343733634534b73693743694537436d3263786b796f4c4a73684d4b316a706d2b7632577a2b737a4c622b667853526765535957694f575a43535a5a3638716f68632b4d4d6a44736c394a716e3069774d4c546f334b56377a2f7366487a64392f7259437932364f4c3270477a504c353752494b762f6e6b6a6646442b2f6671666b666e4670435977616a6470752b4556542f77356b53446c39386d6c623936306e33744d7778497a4b42374d42694734516c71512b30753437647a3079735a6c3857486b5632547532594539643875456f4d4f69526b30545466734c6a66616a54386f4678644864587a4b4c576c32376f61754a5a435a756b416f36585038575833515636694d6b6f793933386659396946314d467550787a576379764b366477655434344a684a67634759593647783232675172797763576d506e5a49685844376d653659616f6e497071554241584931586f3138556f5171616246737274365069307055433844794c5a4f706f4a353661366e3555504867414271444e69596a5658774553316175696d363775785257574c5a3577414f694a366b7252517770307262416d3473523851484b6f594a6a5751477831425832476b636e4669687575736378584d5a39684a756a4a785971766f4b7974674a48484a4c765349586e7356596c6f4e54764c71497249453979534c4f4e67576664304f466b524d63476b5a475346696c68354e334645784f676e4d704a5043514f694e59426f7454443645306b70574679305046744a6f6f575244315650704d56316d6347416f535372685a48484a366c6741527166426757454152476a6570736c7038554b754139765555544c5a4a386b4c7974557158755a52594831594e4a71495466783963583145434d57585a33456b6f57306c6a4254584c4257517a4948413059494779324d2b47775361794733384e334631644269765446423867594e476f5a6e3038364f51354e6b4e6c6b49792f684763447a45614876536b745639443947614b326a684d647243354761464e756756515175503061596c70636e534d4f6736626e6c4a7151525069734d6142576c474374386d6f2b7559676576324e35534478416d56585548764b32367a2b705862505a4669654e797a65655a486c456f2b706c624a614e71683233312f476633537a41314d4f4877356539724555554f79656d57326156712f577557555574426377777a335a3557596d73705833706f31657562437454754f6e793732522f42497576684d336f4674617a353861555457486466565367316d6a75556b386d4d66766f3874384564366f397348506a743739614769556b3232647a50374c4d726d555a2b3948626e4d6c3935544f78652f386e6a506c6a5831344e5075524e797370586d43616c65317a554e54312b61634b39566930374c3847434c59524d4f445a5a6d6c554a7435472f38317650745641536e7a47416b6b59696852646d58546d766164734f78596b4170766d6c2f2b706e68372b33746f77316652376e6d6a7539554c4145754970396161787a5a5239626f394e5765504e3441704b70544b4d5173684b317a2f636c597a757737784c57416f556a717635432b4766566f514f6938516255796c41356379757a6a66317064376f3454463853354b3362626d6c2f6159737433504b3436324b596163796777634a76396473504c70647278476e766a6a70646b326f756b6a63334e3552614e2b41455877664d72327639395769637458584b3157324861712b544e666557325255694e52355465652b7669765061396d4774695838574c766266577231583870723573704b6c4c42776439724568355965494e65422f56637a2f612f4e593850386449384f76376f4e50575966546a484c34534a6a2f58686a384b53556b706d374b4e354246362b35514e7259712b7158625851754641314772354a554b62733854333435376e387665732b6d546c36687269597a426739593337326e72796955446d6d36464272635a6b2f4d714570316c5764736566365a2f53616438592b6e4f554d4b68696e2b4d697161555076626e394e6a545265385a7645746b767957446678336b6d72337168743934636d4c39787a46723855486b7238706f6b467a2b6b43716a6250633647752f764257572f304551525775656532784f35756b6c39345533304e3844623448704959626d746f673834395446683851422b626e3472582b64356f4b5846797172766a6251583432792b2b6f425146514533765744773265526d41424435632b75347244366f7252673875476f66616d747830364e79646b6d5a7a46437a7631363334656950474b4b3764564453666e423437454f5a4f79316b384967697137394553475658714a4c4953735771656e467030514e4a70634737646b3662453039527a5635524d4c6e50585062387655786c45743757546831524d4a734d704f32532b39353450386b4431775071533473527441724135774971714d73586e327955466e6f647279624b76536f4354444369344738617a72443135657a466b493456703459327a4f7332435847454f4f6331544f64637037745a306e37413547435a594e7a4336693659694e474d384a6c3230372f4e507167525a7458655161655064574a31514257375a2f31425768645a4a6f7779715a6e485a6277657a4b4f522f747a42305937584552556631384362634e7a6c4b312b6347716f526c2f54474346356c3264467752674f4676545a63326a4f53376947464c704f5a4e5a416d6239693776306f5072464546625151322f2f766d5858306246446930647055624e636d4f2b76647a434868364c32762b2b37502b677178787857594a68724f6466575265654b5a6a654e7a6d45384e49635a73357a72674850704d324f7142697949456c694239614c652b385230386379676141794c6d4f4e644f536a647a7233317a6a574238705842737157723267744953305334337173706d78594f67756d764f326f675a2f6c745a31734231634b79653674744e75495373467a3732736864693844522f366164516d493150643032477370683254374f5935624172684d763832574278463179574f3454694d4c5a417554317353504841797a624b2b6834324c48577476524e6b7564444746426a72724e456f3969747649714c56627a41346774416456633536774f5073625336484d4f466f324462765467494f7666504b46757334676557585a307049714f696c323172674f65324977346f4b5950396a734b4d456277396564766a43785a50384943506d514b47363944316b6373576c6a594a2b38615a31636d4f6642434d4e3168634c7a6f5559503264473144514b554a616d4c7a53504e52703534375a63793058342f6944785739466148314b694a62564e534a616d4c6a684a714635773459364744632b595347744e694b30634d36624765727769685854496f64354254706c55527179696c64593249793268554b307a6e584575425757712f6235516c376470377a563851734c436251374c5553723845614d57304657585971635757474d4e616e59696e6947685151366e475757414b32547a53756b49706a6f62722b51596478654452735233374351316d2f35656f687a59334b757151417454484b6e674d7541335a5866474f50474f797973346e67424c63456f2b78746758484a585a46724f72486844753250632b4965464c7541794d567137366c4b4c4d61446a4f57614b35443053347959414c4578613536424969377a737631564170374736345a515971382b77307849434675392f50792f4c6d64594362424f6c4356666b694d697378516f62592b5445674957302f695169414e6a7779595343644b6a30745167724c506f6872454b69774e4a30574370437932384a4e38735777586e4d4b3854714d3479634d4c41773966552b757a546e6c626c627752444e63374b495850466957344d6e67574168674f63464e655a49384f597035787a37422f77335a39312b4254775272774446457059473664387853366870697a47325350576246346c6b69484679363061387a68396257487857456d69624d36326e4d62497a2f4c51745846536a37324b70674957642b79457a7855784d4a686a4f745839446a4a5846636d704278492f6259672b7238526d68416b3232767a72476470445466686850544c4247524335594d59654656583564564848656443684f68387479685a5161497832394e484c42696a3073485a7037417955367437416e47442b64315874693344485752416d43465874593451764f6e4745647151503654386c6f4832594b31727934616352446f534a59725445763059754a5054386856786c4845616859526e4e6b7346494143367339583141694c48623742557645663369562b5552722f6b73704e56634179774f3343525a70737330366142664b70525657536654476f556f537a4c735357466a764663496a2f6a41774c70544a386c416d7a726c4d6b564e784662414d2b4c32775a53363438767a5778684f77376f4b353343366e346970676156416e46334f72794d314b3453785364346a666f624e626a685971675957357a47492b77544c4e46714366547a67486c65546744507746616656574175744f5a6f6d32394150517a7a6542336c6d5367334f3962354e57627757774e4b69794c39525735314a764c6c2b7141512b4b576977735a356573322f6555774d4a73586d512b3863767a39484b774b36466769562f334c362f61616d446458464b71633748744d50353568744e596a345771594f454a3269336956757664554c4868354638512f4959544e5356706f534a594b42762f454a2b722b4b344876614a796956435867617967434a5948666b4d774f6c504c6c4973665a6c50754f48354f7875714d536c68345866677038584a2f77484f63705366696a5535546275376f49613357696d426845446337506a616b6c4844676e3850774b3246576866566c6d53786c734177594a3978696933316474726e727735796430363644684964467553346349326147533959446934664355766d697a45717267615642526c354a776334746e6855714f6277725476676d576d6e3258526b7344437349766c4a756264424b706f573745494677796e4f4e7061776f7134566c77426a6d49386c48324d6d693350652f493057617956494a7177383252566750507749396c4f345a2f436a756b73365878306f644c42317573416a7a77344c4c3755627a48396c63666358587375535a4c4857774e4b68786b6a4a706551434d344f502f6335684d484e61663351464c323044527737644144326a687777544238737574737a4a5932505a2f55787a78773156745064547435396e6979627a587968734d46634979594351326d3751715a542b6f774e3177684654484c6e47446763657365684f613757506a774f43433151783370524a63306d7a4e46624230614746534c5077612b3855336b49562f4568432f4a56454c46634c536f476f755a587059554a6376734d4a72424e334671474d6b446f5a4b59586b326b3878504e39746f72614f395937432f4f324268574570794c30667a6474636b4f4763385a69655a565659495334665a70496e4c596d3671572f744a7349702f356736626858497969586b4a44632b70676f6e366f78595330707a4f6b446759716f5356416b2b51484331664d307a453131634a6159366d75775357415139513341434c33594f4a5071596c32535a78675559704c48746c6d54496e486f6d414e394d47772f2f495734425843307548747268495133497734524a307a53697750704c4a53693273713470495535634e4f6c7a4e31326349665038707463594b59576c516a655930356152444a2f35624f486a5a4f4a6b2b715670596c512b53585069694b3645664a714249316c44337745726254594b46627a6f59537645634d504d42376f475667692f507054533944347931335667562b787955772f4a7359685a4e716162695435497379707a747149564657304c777372457768356b55574f614e376f45466e7a4f5435676973704d48797458414c4c417a4c6d456c7a4d554f624b324e366369347559476d77674462542b78792b783170516e4931474c6f4c3149513357646a684d673357326756756d4f396a7062394d47742b38686c77627264463058775a704667335545386d6d77546b6e6231523048734e366877546f4a5a2f453349554665566466413075416a74466d456342704d47717a6a4d703948713461316741624c704d49364c48565657536b73394c4f6f7345347a306a7a3665306d6e4d745844436e7277684841476341474d416d746e716e746738626b687956354444764f726531366846705a6e457733574d646848672f57746979517235527361724950774c573042624b2b4c62466261486871733362434757636b354767625834436d506432416562596b6d317a562b466834725034365a6b726147544b65352f506e5633414f72576a37744b65447377426f383466686362666641716c5649672f5569444b564a31706e4c3351494c6e7938583052365a506f326e3771336b58507a546f5a32665a724d473479456e6858765a564d494b6e4d2b6b6e5a5a6f5745534335577670466c6934545a756d564c37726f504a684569782f652f6641476b597a31336d3437726d5757556e35524a7166332f57535a6e6f7077452f75554b7a63483979793138454462394a6176686754505555374d2f436b6a46666278514d7344656254486b6850776b5139695a365a5379524c347762494a4f6b55706d7053544e705a2b625a4c444477754f6e78483235375641564e56326b394959374c6c49444f6f684558634a5a6c6669796461524a4247693230785a4d3533564d4a71654a61302f2f59726a592b676f306b484459355763516373485472345366626e4e6644676637386953654f5a657536415a63443974414d4157634231716b3465355a43693255716d6856634a61777a74614868777239564b6b7448714b394e335541664c336b4e44383939746f7a55575578454f4b626f436c6762366c36514435544e343466682f4a736e517665734b4e55545034582b55775a41663441307333422f4144384b704e6e6b6b576e686c7348543756566945355854637842656f38417a4b33526e35457434676f783657423370686a754a6a345970676f7732346c334a396c722b6452443155427375413453524c50527854424e533337672b452b374f6b487239584273734430796a71354c38684b4348343633334b4c615850757741573674526e7a425458776730385366672b5750474563313167737a533435416a6d534e48436b70525644784c75676a386b6358616f4370594f37544648306a57483453712f7a48794568523135466c34564c414d6544626659575a6d5767466171797265515a464b653056494643392f7a5259443134304f5732666956614e4b504531344e636275523849735a4d4e3768477069436469464565456c4c336b3461526244774c6b6b2f34514c63435743556e536e686c4566423354324b59426e774344504a4a2b4843715a386a76456a6d53576c47537845732b39674f2f55625838503662384f73336e5638596b7542716950636337694b597244742b5643352b6e43722b566f6554306f36477159474672382f306959764771704c4768744e666530343476627a37437454414d6d41773453564433636f566931394d463339787a364a456c797a52626363593759747962625776344378357336767a65565a5a2b2f2b55774e4c674d75466e4e4262664e6854526f6f584a4870576b683070674766784f47664572734c547a796d614438465a6e3579744f3551516c734f776a724b4b46336e4c2b516f32533355664f57566953646b75716749574c6e536545587762355472444d38706d6b3437552f735830677067495766354f6a4b626f3575776e6f4658355a61666a636f5a77445479706761634a506c583173504267587a6d594a5268423839433946744254417768756f4c64455875752f46342f4d587a71664a576446384e757379524573424c414d6d4d4a2b7732324449324f3573736e746b5650332f375a313963425658466344507658743562306a54524b536b464b5a4e625474306c4a6c2b41474e686c45784644514b686c5644616353704374422b44554c47446b6e46416d325a47555159452f3269686756426c687459574b77536c6f48395178524a725157415331464367704b41465351704462416c76392b345a373932587833734a6c486332623366666268376e4c386a637a39383935397a7a37743650344745782b4f522f55464a504e686c584c516d6130535247443948554c4146506f555854717a504467562b3938574d737169464f3861447467634e69594c5367704b6e444e30426b412f383056625632656842714251354c77437930714562497335466e38447330413373664f576859444f42744569774c6a35566d68615553334851614a616d34762b582b6d6b5851734b6968704772542f53416f355431497536445a394f446e644d4377474968576c4b532b316448365a68445834323038567734385572436f4879704d334b374945753136473572554a35616a424575356d444d5578624c773642446731444a4c44364e4a5848495645594a46334c596e3865495965723845334e324e6b765457595178595a47414a6d45537054446e7332534463464675744d675877436e3651734267552f524d7469684857672b4832523447304b5950774e52415267555838326d6669426d444d5a636e4c614f6239305767774967467245465254584573436477427731364875526b785164505a514566414977444c677467365542466237693978586f7744386e6b4c4c784a654273394444346a4434414a71453368793571542b316344442b534b5031505242686838554258694f784f6c344f6f6e3864696231426f4b583657673069334c41596739556b56696476423948666e67782b6b30424c496a3449497379774f494f666f6b56676466724f5849796b2b43383057744e42684263574236676e4245496d766a2b362f3677635339784255463831614655677767704c5a567046594a58415935665a6f4f754b4e714e705a36646c543456424c4a5377424d522f53324c564f6b4b7a797046574930715a6e5a593148546750496178424d484976775467532b4e59517853726e62354b77584d4567574f496931666577775749437870374d376e5a74433138767970325651327565386e34454c37387844694a637346543647526449625839654a2f5a416d414654756b696a382f6337514c447777464a715a66794d5942576d597857356636704b2b636a52527768753373537a5653707857474278427666755131746d62336233544a33594b784577644475705771777a774f4268674b58554368615a616f697a4730546250534338592b576f797a4d3078335877697970312f6d454a674674333069616d587864373464723775506c7048595278556a7a586c536b336c31395953726d4e707a3467655135384772786d356168312b5a386f5132586a71526f46313867664c46333356773953444548697578584176584c746654543742354932576a767630354e6f666d42784166445a4855687236507053623931567236686c37443643636b6d56597576394768634c476862547338757444567070434b3138663659504a70675a755078596a59676b75453338777853746a54784957457a332f4e376e7a2b7347454a7a724b7a65433459634a5a696a582b4431495759685143485a5847376f444c4268597a4e43315464746d5579785177587a3359522f564b6a3134437a7056644a49646c34346a6c6f353276416a7a47355a44436a37783544366b6f4649574b4a64663734396e763079355276354b737944684d6e64392b3261487363483867735753704b36623874774a7a5345374b6d555875386234726c5a70355a723042676d58626865652f38306a513550354f504d61467550432b567678314458486b384e446356622f654254386d675376484d673870414d5a517473736262416472792f35516d6d536a6a41343877495759397849676f4b524d3961304f7868734571702f6679634f7a4a4d6c4271496f6f7a4b655045596153725354695535742f66376e696c4f36715a4278787068375741705342695977376e683031562b37694b516352542f37374a43414c4c4233694872642f48387058425a536543586e7a354d3766374767736a7965566f385950454348395144454d725279384b652b504f2b466c6d344867694a465133587132524542576d4276317857764f5a4363666c77417777744874362b61567a582b74744b346276546e615873796461714a6a677549587a39697a4b7a61686c3376646663676b445953316676343468767967697146697a2b79692b5a596e525972594662713339316e6a7a51334e5437546947525a762b5335545476656571666a7731515a5667386f326b537a6633364a5175562f7548413158484466326b37744d2b695875306d467a45372f6c357776732f4f714247704770326e64723336463555327255754c382f68762b3366316b7a3548574d6c737161676b337434776e544d755330715a6953732f47522b7448516435524f654c387a4a69347572324856346a45646f7a2b7a432b6e446462685467685158624c4734686b766479615650685441624f6b34687639746e7a734d3875717150753633326243484739397a37435850774f796b5375462f582f6e6d4c56723177364a556d62793431712f4b6c51637635684f593069676e524a47485836676571707356426b39315a584e3037682b2b3831754e683258535a7968696753477a39545362724b313930344b375977444177306f713078346864732b3874572b66543056446c732f49464b5a4c41647948427a6373484638453462512b67493962435744444a396475326e2f4f7868517a55382f37336d4872435434557052377061743338772b6b334f33567a346546585537386c4251794d4779624d2f636c72427a6f75396f6c4b4536595353346d62687a3531657030766f62426e4d6b3930746d78642f746a4547775645446c54476f6c78717868596c6f37355538364f477257386537767a49516f394558766a676e543362317463394e766e5470654b5330325452413556426a4975306c3258786b6c7647567336634d372b322f75634e4c32353836645574572f625336657a64736d587a537874666246685658377467376b4f5478355758784e4d6c4338476a7a416e36724e52646357575a2f68696c784b396671575264376b444231426561787162364e38685a2f754d73426b7652704635557542526954475871796137797377454a3653706276396253596132464b4151442f6f6b2b3632384666346c6c4a4d584e6b2b6f536a38514c6d68614855516e36476e78696c47643747614d6f42737879387a444c4c47383279555a55424e53376552436950764276574f47535037743553574933464c433465314864786775334637445445764345753264526e7968674f2b545135413557552b46716c6f7362696c50427732634b6c7062497541346839367377423759774b443242306730736953644b437a53494631434c4c6c63434c617774544e5869554862476e5749354e7a795746615458457244436863644b33785255694b6f6c6f454c50627935465a61676f50466f636974725151746469595674526f526b69343744427052476d62327a793869526c4249514a574a4c534b2f6536745351554f36324345735771427676376156726c71796b6757707772566969786e364979316752786c435155496741573932626c6e7462693450657935304f5543636257584d624b4e6130317359467669676f566a477642724b655273682b5a62786b586a683231766f6c4756564a6e6f6f6b356979716b726d5467346d4b4751695565623066305a422b4e4b715439635633696750744b7a586f3236433573526652713937632b344e573663426741474a48655a33545a6e694e514571395966646f35754f4b5a36495072703164587845464a6c48636363554f4a534f38794b70753673673239525a5843685730727035616c3978384a5662477678385439464645325966614b336565303358686c67476c4a465870753934725a45387169476e6e644e616d79716e724f6f6d58726d67353132636c4f65617855615a466d736f6175513033726c693261553131564f656b7569497777594d3239657550372b5146623968364c357478663177684b48466957725871513848304c664f2f4e384b7043565a38564d56683776416d6d2b68754337596b55724762307a55566c46786b787a626f4736786f73482b51617247757741503450654b34594f4a547a376d6341414141415355564f524b35435949493d, '$2y$10$nTFlwNoUcbag5//6SecceuzCfCmccTnKPs.0jNU1tmo1nNAF97Usq', NULL, NULL, 0, '2017-03-23 19:13:24', '2017-08-12 10:32:54'),
(3, 'Ivo', 'ivo@sofyma.com', 0x2f696d616765732f757365722f64656661756c745f6176617461722e6a7067, '$2y$10$mrOTjvbubua7XXyD1hVdU.wYICJe/k7hom0OMDh.tWXbwHKGZAqwm', NULL, NULL, 0, '2017-03-23 20:01:49', '2017-03-23 20:01:49'),
(4, 'Trainee', 'trainee@promansys.com', 0x2f696d616765732f757365722f64656661756c745f6176617461722e6a7067, '$2y$10$sfob3YkuHM8KuM9aBJrYKOz6fIf7I7W3h/mpxqu9lS65KHGs09mjq', NULL, NULL, 0, '2017-11-11 11:06:27', '2017-11-11 11:06:27'),
(5, 'New Member', 'new_member@promansys.com', 0x2f696d616765732f757365722f64656661756c745f6176617461722e6a7067, '$2y$10$PaB.zv.2eJ49x4p3gNrn.umidEcAaaWfPFZBkqOFPHvtebklRlE6O', NULL, NULL, 0, '2017-11-11 11:11:53', '2017-11-11 11:11:53'),
(6, 'Test member', 'test_member@promansys.com', 0x2f696d616765732f757365722f64656661756c745f6176617461722e6a7067, '$2y$10$KbifBhYAn/mFJx0uvsNRt.yet8WIPef9RDKZ3gykbxBlimo4THPUi', NULL, NULL, 0, '2017-11-11 11:15:06', '2017-11-11 11:15:06'),
(7, 'Coffee Man', 'coffee@promansys.com', 0x2f696d616765732f757365722f64656661756c745f6176617461722e6a7067, '$2y$10$jd99wmzuHu8/Rs9YyqLvFO6wifXAo6kLgwknJWqzRolohMwf0Tw0.', NULL, NULL, 0, '2017-11-11 11:33:53', '2017-11-11 11:33:53');

-- --------------------------------------------------------

--
-- Структура на таблица `wiki`
--

CREATE TABLE `wiki` (
  `id` int(10) UNSIGNED NOT NULL,
  `wiki` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_attachment`
--
ALTER TABLE `comment_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_attachment_comment_id_foreign` (`comment_id`),
  ADD KEY `comment_attachment_attachment_id_foreign` (`attachment_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_task`
--
ALTER TABLE `project_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_task_task_id_foreign` (`task_id`),
  ADD KEY `project_task_project_id_foreign` (`project_id`);

--
-- Indexes for table `project_user`
--
ALTER TABLE `project_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_user_project_id_index` (`project_id`),
  ADD KEY `project_user_user_id_index` (`user_id`);

--
-- Indexes for table `project_wiki`
--
ALTER TABLE `project_wiki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_wiki_project_id_foreign` (`project_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_status_foreign` (`status`),
  ADD KEY `tasks_priority_foreign` (`priority`),
  ADD KEY `tasks_created_by_foreign` (`created_by`),
  ADD KEY `tasks_updated_by_foreign` (`updated_by`),
  ADD KEY `tasks_assigned_to_foreign` (`assigned_to`),
  ADD KEY `tasks_project_id_foreign` (`project_id`);

--
-- Indexes for table `task_attachment`
--
ALTER TABLE `task_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_attachment_task_id_foreign` (`task_id`),
  ADD KEY `task_attachment_attachment_id_foreign` (`attachment_id`);

--
-- Indexes for table `task_comment`
--
ALTER TABLE `task_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_comment_task_id_foreign` (`task_id`),
  ADD KEY `task_comment_comment_id_foreign` (`comment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wiki`
--
ALTER TABLE `wiki`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment_attachment`
--
ALTER TABLE `comment_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `project_task`
--
ALTER TABLE `project_task`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_user`
--
ALTER TABLE `project_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `project_wiki`
--
ALTER TABLE `project_wiki`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `task_attachment`
--
ALTER TABLE `task_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `task_comment`
--
ALTER TABLE `task_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wiki`
--
ALTER TABLE `wiki`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `comment_attachment`
--
ALTER TABLE `comment_attachment`
  ADD CONSTRAINT `comment_attachment_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `attachments` (`id`),
  ADD CONSTRAINT `comment_attachment_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`);

--
-- Ограничения за таблица `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `project_task`
--
ALTER TABLE `project_task`
  ADD CONSTRAINT `project_task_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `project_task_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`);

--
-- Ограничения за таблица `project_user`
--
ALTER TABLE `project_user`
  ADD CONSTRAINT `project_user_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `project_wiki`
--
ALTER TABLE `project_wiki`
  ADD CONSTRAINT `project_wiki_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_priority_foreign` FOREIGN KEY (`priority`) REFERENCES `priorities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_status_foreign` FOREIGN KEY (`status`) REFERENCES `statuses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `task_attachment`
--
ALTER TABLE `task_attachment`
  ADD CONSTRAINT `task_attachment_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `attachments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_attachment_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`);

--
-- Ограничения за таблица `task_comment`
--
ALTER TABLE `task_comment`
  ADD CONSTRAINT `task_comment_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_comment_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
