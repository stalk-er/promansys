@extends('master')

@section('content')

<div class="row">
    
    <div class="col-md-12">
        
        @foreach($tasks as $task)
        {!! $task->id !!}
        {!! $task->status !!}
        {!! $task->priority !!}
        {!! $task->type !!}
        {!! $task->created_by !!}
        {!! $task->updated_by !!}
        {!! $task->assigned_to !!}
        {!! $task->project_id !!}
        @endforeach
        
    </div>
    
</div>

@stop