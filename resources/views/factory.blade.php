@extends('master')
@section('content')

@if(session('flashMessage'))
<!--Alert Messages-->
<script>
    var flashMessage = {!! session('flashMessage') !!};
    console.log(flashMessage);
</script>
<!--End Alert Messages-->
@endif
<div class="row">
    <div class="factory-container">
        <div class="col-md-4 col-sm-12 col-lg-4">
            @if(Auth::user()->can('create.users'))
            <button data-toggle="modal" data-target="#create-user" class="btn btn-success" title="Click here to create a new user">
                Create user
            </button>
            <div class="modal" id="create-user">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Create User</h4>
                        </div>
                        <form class="form-horizontal" method="post" action="{!! URL::to('/'); !!}/factory/users" autocomplete="off" name="users-form-data">
                            <div class="modal-body">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="user-username">Username: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><input required id="user-username" class="form-control" type="text" name="username" placeholder="username" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="user-password">Password: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><input required id="user-password" class="form-control" type="password" name="password" placeholder="password" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="user-email">E-mail: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><input required id="user-email" class="form-control" type="email" name="email" placeholder="email address" /></div>
                                </div>
                                <input type="hidden" name="user-factory" value="1" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Save changes" />
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @else
            <div>You do not have permissions to create users. Please contact the administrator.</div>
            @endif
        </div>
        <div class="col-md-4 col-sm-12 col-lg-4">
            @if(Auth::user()->can('create.roles'))
            <button data-toggle="modal" data-target="#create-role" class="btn btn-success" title="Click here to create a new role for already existing user">
                Create role
            </button>
            <div class="modal" id="create-role">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Create Role</h4>
                        </div>
                        <form class="form-horizontal" method="post" action="{!! URL::to('/'); !!}/factory/roles" autocomplete="off" name="users-form-data">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" name="role-factory" value="2">
                            <div class="modal-body">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="role-name">Role name: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><input class="form-control" required id="role-name" type="text" name="role-name" placeholder="Role name" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="role-slug">Role slug: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><input class="form-control" required class="form-control" id="role-slug" type="text" name="role-slug" placeholder="Slug" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12"><label for="role-description">Role description: </label></div>
                                    <div class="col-md-6 col-lg-6 col-sm-12"><textarea class="form-control" id="role-description" name="role-description" placeholder="Description"required ></textarea></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label for="role-level">Role access level:</label>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <input class="form-control" required id="role-level" type="text" name="role-level" placeholder="Level of access" />
                                    </div>
                                </div>
                                <input type="hidden" name="user-factory" value="1" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Save Changes" />
                            </div>
                        </form>

                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @else
            <div>You do not have permissions to create roles. Please contact the administrator.</div>
            @endif
        </div>
        <div class="col-md-4 col-sm-12 col-lg-4">

            @if(Auth::user()->can('create.project'))

            <button data-toggle="modal" data-target="#create-project" class="btn btn-success" title="Click here to create a new project">
                Create project
            </button>

            <div class="modal" id="create-project">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Create Project</h4>
                        </div>
                        <form class="form-horizontal" method="post" action="{!! URL::to('/'); !!}/factory/projects" autocomplete="off" name="project-form-data">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" name="project-factory" value="4" />
                            <div class="modal-body">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label for="project-name">Project name: </label>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <input class="form-control" required id="project-name" type="text" name="project-name" placeholder="Project name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label for="project-description">Project description: </label>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <textarea class="form-control" id="project-description" type="text" required name="project-description" placeholder="Project description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label for="project-slug">Project slug: </label>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <input class="form-control" required id="project-slug" type="text" name="project-slug" placeholder="Project slug" />
                                    </div>
                                </div>
                                <input type="hidden" name="user-factory" value="1" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-primary" value="Save changes" />
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @else
            <div>You do not have permissions to create project. Please contact the administrator.</div>
            @endif
        </div>
    </div>
</div>
@stop
