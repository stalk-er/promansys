@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            403 Not Authorized
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="error-page">
            <h2 class="headline text-red">403</h2>

            <div class="error-content">
                <h3><i class="fa fa-warning text-red"></i> You're not authorized to access this page!</h3>

                <p>
                    You're not authorized to access this page!
                    Please contact the administrator or <a href="{{ url('/dashboard') }}">return to your dashboard</a>
                </p>
            </div>
        </div>
    </section>
</div>
@stop