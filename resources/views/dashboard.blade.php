@extends('master')
@section('content')
<div class="col-md-12 col-sm-12 col-lg-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">My Tasks</h3>
            <br>
            <br>

            @if(!empty($my_tasks))
            @if(app('request')->input('page') != $my_tasks->lastPage())
            <p class="text">Showing {{ count($my_tasks) * ((app('request')->input('page'))?app('request')->input('page'):1) }} / {{ $my_tasks->total() }}</p>
            @else
            <p class="text">Showing {{ $my_tasks->total() }} / {{ $my_tasks->total() }}</p>
            @endif
            @endif
        </div>

        @if(Auth::user()->can('view.task'))
        <!-- /.box-header -->
        <div class="box-body">
            <table class="tasks-table table table-bordered table-striped data-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Subject</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Project</th>
                        <th>Type</th>
                        <th>Author</th>
                        <th>Created at</th>
                        <th>Last update</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($my_tasks))
                    @foreach($my_tasks as $task)
                    <tr>
                        <td>{{ $task->uid }}</td>
                        <td><a href="{{ url('/') }}/task/{{ $task->uid }}"><span>{{ $task->subject }}</span></a></td>
                        <td>{{ getStatus($task->status) }}</td>
                        <td><span class="label label-{{ generate_status_class(strtolower(getPriority($task->priority))) }}">{{ getPriority($task->priority) }}</span></td>
                        <td><a href="{{ url('/project') . '/' .$task->project->id }}">{{ $task->project->name }}</a></td>
                        <td>{{ $task->type }}</td>
                        <td>{{ getAuthorAssignee($task->assigned_to) }}</td>
                        <td>{{ $task->created_at->format('jS-M-Y') }}</td>
                        <td>{{ $task->updated_at->format('jS-M-Y') }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Subject</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Project</th>
                        <th>Type</th>
                        <th>Author</th>
                        <th>Created at</th>
                        <th>Last update</th>
                    </tr>
                </tfoot>
            </table>
            @if(!empty($my_tasks))
            {!! $my_tasks->render() !!}
            @endif
        </div>
        @endif
    </div>
</div>

@stop
