<!Doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ Session::getToken() }}">
        <meta id="userId" content="{{ Auth::user()->id }}">
        <title>PromanSys</title>

        <!--
        <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
        -->

        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{ asset('/bootstrap/css/bootstrap.min.css') }}">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <!-- jvectormap -->
        <link rel="stylesheet" href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">

        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('/plugins/select2/select2.min.css') }}">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">

        <!-- Index -->
        <link rel="stylesheet" href="{{ asset('/dist/css/index.css') }}">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load.
        -->
        <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/dist/css/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/dist/css/jquery-ui.theme.min.css') }}">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="{{ url('/plugins/iCheck/all.css') }}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .term-highlight { font-weight: bolder; }
            .ui-menu.ui-widget {
                z-index: 9999;
            }
            .ui-datepicker {
                z-index: 1060 !important;
            }
        </style>
    <input type="hidden" id="user-id" value="{{ (isset($user))?$user->id:Auth::user()->id }}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">

            <!-- Logo -->
            <a href="{{ url('/dashboard') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>P</b>M</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Proman</b>Sys</span>
            </a>

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <!--                            <li class="dropdown messages-menu">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-envelope-o"></i>
                                                            <span class="label label-success">4</span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="header">You have 4 messages</li>
                                                            <li>
                                                                 inner menu: contains the actual data
                                                                <ul class="menu">
                                                                    <li> start message
                                                                        <a href="#">
                                                                            <div class="pull-left">
                                                                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                                            </div>
                                                                            <h4>
                                                                                Support Team
                                                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                                            </h4>
                                                                            <p>Why not buy a new awesome theme?</p>
                                                                        </a>
                                                                    </li>
                                                                     end message
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="pull-left">
                                                                                <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                                            </div>
                                                                            <h4>
                                                                                AdminLTE Design Team
                                                                                <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                                            </h4>
                                                                            <p>Why not buy a new awesome theme?</p>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="pull-left">
                                                                                <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                                            </div>
                                                                            <h4>
                                                                                Developers
                                                                                <small><i class="fa fa-clock-o"></i> Today</small>
                                                                            </h4>
                                                                            <p>Why not buy a new awesome theme?</p>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="pull-left">
                                                                                <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                                                                            </div>
                                                                            <h4>
                                                                                Sales Department
                                                                                <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                                            </h4>
                                                                            <p>Why not buy a new awesome theme?</p>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="pull-left">
                                                                                <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                                                                            </div>
                                                                            <h4>
                                                                                Reviewers
                                                                                <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                                            </h4>
                                                                            <p>Why not buy a new awesome theme?</p>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="footer"><a href="#">See All Messages</a></li>
                                                        </ul>
                                                    </li>-->
                        <!-- Notifications: style can be found in dropdown.less -->
                        <!--                            <li class="dropdown notifications-menu">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-bell-o"></i>
                                                            <span class="label label-warning">10</span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="header">You have 10 notifications</li>
                                                            <li>
                                                                 inner menu: contains the actual data
                                                                <ul class="menu">
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                                                                            page and may cause design problems
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-users text-red"></i> 5 new members joined
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <i class="fa fa-user text-red"></i> You changed your username
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="footer"><a href="#">View all</a></li>
                                                        </ul>
                                                    </li>-->
                        <!-- Tasks: style can be found in dropdown.less -->
                        <!--                            <li class="dropdown tasks-menu">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-flag-o"></i>
                                                            <span class="label label-danger">9</span>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="header">You have 9 tasks</li>
                                                            <li>
                                                                 inner menu: contains the actual data
                                                                <ul class="menu">
                                                                    <li> Task item
                                                                        <a href="#">
                                                                            <h3>
                                                                                Design some buttons
                                                                                <small class="pull-right">20%</small>
                                                                            </h3>
                                                                            <div class="progress xs">
                                                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                    <span class="sr-only">20% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                     end task item
                                                                    <li> Task item
                                                                        <a href="#">
                                                                            <h3>
                                                                                Create a nice theme
                                                                                <small class="pull-right">40%</small>
                                                                            </h3>
                                                                            <div class="progress xs">
                                                                                <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                    <span class="sr-only">40% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                     end task item
                                                                    <li> Task item
                                                                        <a href="#">
                                                                            <h3>
                                                                                Some task I need to do
                                                                                <small class="pull-right">60%</small>
                                                                            </h3>
                                                                            <div class="progress xs">
                                                                                <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                    <span class="sr-only">60% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                     end task item
                                                                    <li> Task item
                                                                        <a href="#">
                                                                            <h3>
                                                                                Make beautiful transitions
                                                                                <small class="pull-right">80%</small>
                                                                            </h3>
                                                                            <div class="progress xs">
                                                                                <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                                    <span class="sr-only">80% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                     end task item
                                                                </ul>
                                                            </li>
                                                            <li class="footer">
                                                                <a href="#">View all tasks</a>
                                                            </li>
                                                        </ul>
                                                    </li>-->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ Auth::user()->profile_image }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ Auth::user()->profile_image }}" class="img-circle" alt="User Profile Image">

                                    <p>
                                        {{ Auth::user()->name }} - {{ Auth::user()->userRoles() }}
                                        <small>Member since {{ Auth::user()->created_at->format('jS-M-Y') }}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center">
                                            <a href="{{ url('/management/user/' . Auth::user()->id) }}">{{ Auth::user()->email }}</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ url('/management/user/') . '/' . Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image" id="change-profile-image">
                        <img src="{{ Auth::user()->profile_image }}" class="img-circle profile-user-img" alt="User Image">
                    </div>
                    <input style="display: none;" type="file" id="user-image-input" />
                    <div class="pull-left info">
                        <p>{{ Auth::user()->name }}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="q form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li {{ isActive('/dashboard') }}>
                        <a href="{{ url('/') }}/dashboard">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li {{ isActive('/factory') }}>
                        <a href="{{ url('/factory') }}">
                            <i class="fa fa-book"></i>
                            <span>Factory</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ isActive('/factory#create-user') }}><a href="{{ url('/factory#create-user') }}"><i class="fa fa-circle-o"></i> Create user</a></li>
                            <li {{ isActive('/factory#create-role') }}><a href="{{ url('/factory#create-role') }}"><i class="fa fa-circle-o"></i> Create role</a></li>
                            <li {{ isActive('/factory#create-project') }}><a href="{{ url('/factory#create-project') }}"><i class="fa fa-circle-o"></i> Create project</a></li>
                        </ul>
                    </li>
                    <li {{ isActive('management') }}>
                        <a href="{{ url('/management') }}">
                            <i class="fa fa-pie-chart"></i>
                            <span>Management</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {{ isActive('/management#manage-users') }}><a href="{{ url('/management#manage-users') }}"><i class="fa fa-circle-o"></i> Manage users</a></li>
                            <li {{ isActive('/management#manage-roles') }}><a href="{{ url('/management#manage-roles') }}"><i class="fa fa-circle-o"></i> Manage roles</a></li>
                            <li {{ isActive('/management#manage-project') }}><a href="{{ url('/management#manage-projects') }}"><i class="fa fa-circle-o"></i> Manage projects</a></li>
                        </ul>
                    </li>
                    <li class="header">My Projects</li>
                    @foreach(Auth::user()->projects as $proj)
                    <li><a href="{{ url('/') }}/project/{{ $proj->id }}"><span>{{ $proj->name }}</span></a></li>
                    @endforeach
                    <li class="header">Search for a project</li>
                    <li>
                        <select id="search-projects" class="form-control select2" style="width: 100%;">
                            <option value="" disabled selected>Type here...</option>
                            @foreach($all_projects as $proj)
                            <option @if(isset($project) && $project->id == $proj->id) selected  @endif value="{{ url('/') }}/project/{{ $proj->id }}">{{ $proj->name }}</option>
                            @endforeach
                        </select>
                    </li>
                </ul>


            </section>
        </aside>



        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper"
             <!-- Main content -->
             <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>

    <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>



    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- jQuery UI -->
    <script src="{{ asset('/dist/js/jquery-ui.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('/plugins/fastclick/fastclick.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/dist/js/app.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="{{ asset('/plugins/chartjs/Chart.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('/dist/js/pages/dashboard2.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('/dist/js/demo.js') }}"></script>
    <script src="{{ asset('/dist/js/index.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <div class="modal fade" id="confirmation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to proceed ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id='yes'>Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>
