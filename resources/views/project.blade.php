@extends('master')
@section('content')
<style>
    .displayBlockImportant {
        display: block !important;
    }

    .wiki-textarea {
        height: 500px;
    }

    .wiki-textarea-area { display: none; }

    .wiki-page-content {
        position: relative;
        width: 100%;
        padding: 20px;
    }

    #edit-wiki {
        display: none;
        position: absolute;
        right : 20px;
        top: 20px;
        cursor: pointer;

        -o-transition:.5s;
        -ms-transition:.5s;
        -moz-transition:.5s;
        -webkit-transition:.5s;
    }

    .wiki-page-content:hover #edit-wiki {
        display: block;
    }
</style>

<input id="project_id"  type="hidden" value="{{ $project->id }}" />
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <h3 class="profile-username text-center">{{ $project->name }}</h3>

                <hr>
                {!! $project->description !!}
                <hr>

                @if(Auth::user()->can('edit.project'))
                <div class="form-group">
                    <label>Deadline:</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" value="{{ $project->deadline }}" id="datepicker">
                    </div>
                    <br>
                    <button id="saveDate" class="btn btn-info" style="display: none;">Save</button>
                    <!-- /.input group -->
                </div>
                @endif
            </div>

            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title col-md-12">
                    <span class="pull-left">Members</span>
                    @if(Auth::user()->can('add.project.member'))
                    <button class="btn btn-success btn-sm" style="float: right !important;" id="addProjectMember">Add</button>
                    @endif
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="list-group list-group-unbordered" id="projectMembers">
                    @foreach($project->members as $member)
                    <li class="list-group-item">
                        <b>{!! $member->name !!}</b>
                        @if(Auth::user()->can('delete.project.member'))
                        <button class="btn btn-danger btn-sm removeProjectMember" data-member="{{ $member->id }}">Remove</button>
                        @endif
                        <a class="pull-right">
                            @foreach($member->roles as $role)
                            {{ $role->name }}
                            @endforeach
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- /.box-body -->
        </div>

        @if(Auth::user()->can('view.task'))
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Tasks Overview</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="chart-responsive"></div>
                        <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-12">
                        <ul class="chart-legend clearfix">
                            <li><i class="fa fa-circle-o text-blue"></i> New</li>
                            <li><i class="fa fa-circle-o text-red"></i> Closed</li>
                            <li><i class="fa fa-circle-o text-green"></i> Feedback</li>
                            <li><i class="fa fa-circle-o text-yellow"></i> In Progress</li>
                            <li><i class="fa fa-circle-o text-aqua"></i> Resolved</li>
                            <li><i class="fa fa-circle-o text-grey"></i> N/A</li>
                        </ul>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding" id="task-statistics">
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">New<span class="pull-right text-red"><span id="newCount">0</span> / <span class="total">0</span> tasks</span></a></li>
                    <li><a href="#">Resolved<span class="pull-right text-red"><span id="resolvedCount">0</span> / <span class="total">0</span> tasks</span></a></li>
                    <li><a href="#">Feedback<span class="pull-right text-green"><span id="feedbackCount">0</span> / <span class="total">0</span> tasks</span></a></li>
                    <li><a href="#">In Progress<span class="pull-right text-yellow"><span id="inprogressCount">0</span> / <span class="total">0</span> tasks</span></a></li>
                    <li><a href="#">Closed<span class="pull-right text-yellow"><span id="closedCount">0</span> / <span class="total">0</span> tasks</span></a></li>
                </ul>
            </div>
            <!-- /.footer -->
        </div>
        @endif
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tasks" data-toggle="tab">Tasks</a></li>
                @if(Auth::user()->can('edit.project'))
                <li><a href="#wiki" data-toggle="tab">Wiki</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="tasks">
                    <div class="box">
                        @if(Auth::user()->can('view.task'))
                        <div class="box-header">
                            <h3 class="box-title">All tasks</h3>
                            @if(Auth::user()->can('create.task'))
                            <button class="btn btn-success btn-md pull-right" id="addTask">Add New</button>
                            @endif 
                            <br>
                            <br>
                            @if(!empty($project->tasks))
                            @if(app('request')->input('page') != $project->tasks->lastPage())
                            <p class="text">Showing {{ count($project->tasks) * ((app('request')->input('page'))?app('request')->input('page'):1) }} / {{ $project->tasks->total() }}</p>
                            @else
                            <p class="text">Showing {{ $project->tasks->total() }} / {{ $project->tasks->total() }}</p>
                            @endif
                            @endif
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <table class="tasks-table table table-bordered table-striped data-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Subject</th>
                                        <th>Status</th>
                                        <th>Priority</th>
                                        <th>Type</th>
                                        <th>Assignee</th>
                                        <th>Created at</th>
                                        <th>Last update</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($project->tasks as $task)
                                    <tr>
                                        <td>{{ $task->uid }}</td>
                                        <td><a href="{{ url('/') }}/task/{{ $task->uid }}">{{ $task->subject }}</a></td>
                                        <td>{{ getStatus($task->status) }}</td>
                                        <td><span class="label label-{{ generate_status_class(strtolower(getPriority($task->priority))) }}">{{ getPriority($task->priority) }}</span></td>
                                        <td>{{ $task->type }}</td>
                                        <td>{{ getAuthorAssignee($task->assigned_to) }}</td>
                                        <td>{{ $task->created_at->format('jS-M-Y') }}</td>
                                        <td>{{ $task->updated_at->format('jS-M-Y') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Subject</th>
                                        <th>Status</th>
                                        <th>Priority</th>
                                        <th>Type</th>
                                        <th>Assignee</th>
                                        <th>Created at</th>
                                        <th>Last update</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                    {!! $project->tasks->render() !!}
                    @else
                    <div class="box-body">
                        <h3>You're not authorized to see all the tasks of this project. Contact the administrator.</h3>
                    </div>
                    @endif
                </div>

                @if(Auth::user()->can('edit.project'))
                <div class="tab-pane" id="wiki">
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="wiki-page-content">
                                <div>{!! $project->wikiPage->wiki !!}</div>
                                <i class="fa fa-edit" id="edit-wiki" style="font-size: 25px;"></i></div>
                            <div class="wiki-textarea-area">
                                <textarea
                                    name="wiki"
                                    style="width: 100%;"
                                    spellcheck="true"
                                    placeholder="Put you documentation here"
                                    class="textarea wiki-textarea">{!! $project->wikiPage->wiki !!}</textarea>
                            </div>
                            <button class="btn btn-success btn-lg" id="save-wiki" style="display: none;">Save</button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>



@if(Auth::user()->can('create.task'))
<div class="modal fade" id="new_task">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New task</h4>
            </div>
            <form class="form-horizontal" id="newTaskForm" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class='form-group'>
                        <div class="col-md-3 col-sm-12 col-lg-3"><label for="">Subject:</label></div>
                        <div class="col-md-9 col-lg-9">
                            <input type="text" required placeholder="Subject" name="task-subject" id="task-subject" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3 col-sm-12 col-lg-3">
                            <label for="task-description">Description:</label>
                        </div>
                        <div class="col-md-9 col-sm-12 col-lg-9">
                            <textarea
                                name="task-description"
                                id="task-description"
                                class="form-control textarea task-description"
                                required
                                placeholder="What is this task about ?"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label for="task-type">Type:</label>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12">
                            <input class="form-control" required type="text" id="task-type" name="task-type" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label for="task-status">Status:</label>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12">
                            <select class="form-control" required name="task-status" id="task-status">
                                @foreach($statuses as $status)
                                <option value="{!! $status->id !!}">{!! $status->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-sm-12 col-lg-3">
                            <label for="task-priority">Priority:</label>
                        </div>
                        <div class="col-md-9 col-sm-12 col-lg-9">
                            <select class="form-control" required name="task-priority" id="task-priority">
                                @foreach($priorities as $priority)
                                <option value="{!! $priority->id !!}">{!! $priority->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label>Assignee:</label>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12">
                            <select class="form-control" name="task-assigned-to" id="task-assigned-to">
                                <option value="">No Assignee</option>
                                @foreach($project->members as $member)
                                <option value="{!! $member->id !!}">{!! $member->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label>Deadline:</label>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" name="task-deadline" id="task-deadline">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label>Attachments: </label>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-12">
                            <input type="file" class="form-control" name="task-attachments[]" name="task-attachments" multiple />
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary pull-left" id="createTask" value="Create task" />
                </div>
                <input type="hidden" name="project-id" value="{{ $project->id }}">
            </form>
        </div>
    </div>
</div>
@endif
<div class="modal fade" id="memberToProjectDialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Member to this project</h4>
            </div>
            <div class="modal-body">
                <select name="" class="form-control" id="availableMembers">
                    @foreach($users as $user)
                    @if($user->hasAtLeastOneRole()->count())
                    <option value="{{ $user->id }}"><strong>{{ $user->name }}</strong></option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id='addMember'>Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
