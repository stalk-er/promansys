@extends('master')
@section('content')
<style>
    .timeline-item.latest .timeline-item-description {
        position: relative;
        cursor: pointer;
    }
    .timeline-item-description .fa-edit {
        display: none;
    }
    .timeline-item.latest .timeline-item-description:hover .fa.fa-edit {
        display: block;
        position: absolute;
        top: -24px;
        right: 0;
        font-size: 24px;
    }
    #save_description {
        margin-top: 15px;
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12">

        <!-- The time line -->
        <ul class="timeline">
            <?php $c = 0; ?>
            @foreach($task as $rev)
            <li class="time-label">
                <span class="bg-{{ generate_status_class(strtolower($rev->getPriority->name)) }}">
                    {{ $rev->created_at->format('jS-M-Y') }}
                </span>
            </li>
            <li>
                <i class="fa fa-ticket bg-{{ generate_status_class(strtolower($rev->getPriority->name)) }}"></i>
                @if($c == count($task) - 1)
                <div class="timeline-item latest">
                    @else
                    <div class="timeline-item">
                        @endif
                        <span class="time"><i class="fa fa-clock-o"></i> {{ timeSince($rev->created_at)  }} ago</span>
                        <h3 class="timeline-header">
                            @if($rev->revision != 0)
                            <a href="#">{{ $rev->getAuthorUpdated->name }}</a> updated the ticket
                            @else
                            <a href="#">{{ $rev->getAuthor->name }}</a> created the ticket
                            @endif
                        </h3>

                        <div class="timeline-body">

                            @if($c == 0)
                            <h2><a href="{{ url("/") }}/task/{{ $rev->uid }}" class="success">{!! $rev->subject !!}</a></h2>
                            <h4>Task #{{ $rev->uid }}</h4>
                            <hr>
                            @endif

                            @if($rev->description)
                            <p clas="text">{!! $rev->description !!}</p>
                            @endif

                            @if(count($rev->attachments) > 0)
                            <hr>
                            <h3 class="timeline-header"><a href="#">Media</a></h3>
                            @foreach($rev->attachments as $attachment)
                            <a href="{{ url("/") }}/uploads/{!! $attachment->name !!}" target="_blank" class="attachment">
                                <img class="margin" width="150" height="" src="{{ url("/") }}/uploads/{!! $attachment->name !!}" alt="{!! $attachment->name; !!}" />
                            </a>
                            @endforeach
                            @endif
                        </div>

                        @if(Auth::user()->can('edit.task'))
                        <div class="timeline-footer">
                            @if(Auth::user()->can('view.task'))
                            @if($c == count($task) - 1)
                            @if($rev->description)
                            <hr>
                            @endif
                            <h3>Edit</h3>
                            <form class="form-horizontal" action="{!! URL::to('/'); !!}/task/{!! $rev->id !!}/update" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                                <div class="form-group col-md-12">
                                    <textarea
                                        name="update-task-description"
                                        id="task_description" 
                                        class="form-control textarea task-description" 
                                        placeholder="Put your comments here" 
                                        style="width: 100%; 
                                        height: 200px; 
                                        font-size: 14px; 
                                        line-height: 18px; 
                                        border: 1px solid #dddddd; 
                                        padding: 10px;"></textarea>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <label class="pull-right">Status:</label>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <select class="form-control" name="update-task-status">
                                            @foreach($status::all() as $status)
                                            <option value="{!! $status->id !!}" @if($status::find($rev->status)->name == $status->name) selected @endif >{!! $status->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <label class="pull-right">Priority:</label>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <select class="form-control" name="update-task-priority">
                                            @foreach($priority::all() as $priority)
                                            <option value="{!! $priority->id !!}" @if($priority::find($rev->priority)->name == $priority->name) selected @endif >{!! $priority->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <label class="pull-right">Asignee:</label>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-6">
                                        <select class="form-control" name="update-task-assignee">
                                            <option value="">No Assignee</option>
                                            @foreach($project::find($rev->project_id)->members as $member)
                                            <option value="{!! $member->id !!}" @if($rev->assigned_to == $member->id) selected @endif > {!! $member->name !!} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <label class="pull-right"><b>Type:</b></label>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <span class="pull-left">{!! $rev->type !!}</span>
                                        <input type="hidden" name="update-task-type" value="{{ $rev->type }}" />
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <label class="pull-right"><b>Project:</b></label>    
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <a href="{!! URL::to('/') !!}/project/{!! $rev->project_id !!}">{!! $rev->getProject->name !!}</a>
                                    </div>
                                </div>
                                <hr>
                                @if($rev->deadline)
                                <div class="form-group">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label>Task deadline:</label>
                                        <div class="input-group date">
                                            @if(Auth::user()->can('edit.deadline'))
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                            <input type="text" class="form-control pull-right" name="task-deadline" value="{{ $rev->deadline }}" id="datepicker">
                                            @else
                                            <div class="">{!! $rev->deadline !!}</div>
                                            @endif
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        <label>Attach files:</label>
                                        <input type="file" multiple class="form-control" value="Add attachments" name="task-attachments[]" />
                                    </div>
                                </div>
                                @endif
                                <input type="hidden" name="project_id" value="{{ $rev->project_id }}" />
                                <input type="hidden" name="task-uid" value="{{ $rev->uid }}" />
                                <input type="hidden" name="task-subject" value="{!! $rev->subject !!}" />
                                <input class="btn btn-success" type="submit" value="Update" />
                            </form>     
                            @endif
                            @endif
                        </div>
                        @endif
                    </div>
            </li>
            <?php $c++; ?>
            @endforeach
        </ul>
    </div>
</div>


@stop