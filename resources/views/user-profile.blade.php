@extends('master')
@section('content')
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="{{ $user->profile_image }}" alt="User profile picture">
                @if($user->id == Auth::user()->id || Auth::user()->can('manage.users'))
                <a href="" class="btn btn-info btn-md center" id="edit-image">Change image</a>
                @endif
                <h3 class="profile-username text-center">{{ $user->name }}</h3>
                <p class="text-muted text-center">{{ $user->roles }}</p>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Member since</b> <a class="pull-right">{{ $user->created_at->format('jS-M-Y') }}</a>
                    </li>
                    <li class="list-group-item">
                        <b>Projects Involved</b>
                        <div class="">
                            @foreach($user->projects as $project)
                            <a target="_blank" href="{{ URL::to('/project/'.$project->id) }}">{{ $project->name }}</a>
                            <br>
                            @endforeach
                        </div>
                    </li>
                    <li class="list-group-item">
                        <b>Roles</b> <a class="pull-right">{{ $user->userRoles() }}</a>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-mail margin-r-5"></i> Email address</strong>
                @if($user->id == Auth::user()->id || Auth::user()->can('manage.users'))
                <a href="" class="btn btn-info btn-md pull-right" id="edit-email">Edit</a>
                @endif
                <p class="text-muted">
                    <a id="user-email" target="_blank" href="mailto:{!! $user->email !!}">{!! $user->email !!}</a>
                <div id="user-email-input" style="display: none;">
                    <input class="form-control" type="email" value="{!! $user->email !!}">
                    <a style="margin-top: 10px;" href="" class="btn btn-success btn-md" id="save">Save</a>
                </div>
                </p>
                <hr>
                <strong><i class="fa fa-mail margin-r-5"></i> Personal information </strong>
                @if($user->id == Auth::user()->id || Auth::user()->can('manage.users'))
                <a href="" class="btn btn-info btn-md pull-right" id="edit-description">Edit</a>
                @endif
                <div class="text-muted" id="user-description">
                    @if($user->description)
                    {!! $user->description !!}
                    @else
                    Add short information about yourself
                    @endif
                </div>
                <div style="display: none; padding-top: 50px;" id="user-description-textarea">
                    <textarea rows="8" cols="20" class="textarea">{!! $user->description !!}</textarea>
                    <a href="" class="btn btn-success btn-md pull-right" id="save">Save</a>
                </div>
                <hr>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if(Auth::user()->can('manage.users'))
                <li class=""><a href="#settings" data-toggle="tab">Settings</a></li>
                @endif
                <li class="active"><a href="#last-activity" data-toggle="tab">Last activity</a></li>
                @if(Auth::user()->id == $user->id || Auth::user()->can('manage.users'))
                @if(Auth::user()->id !== $user->id)
                <li class=""><a href="#personal-settings" data-toggle="tab">User Personal Settings</a></li>
                @else
                <li class=""><a href="#personal-settings" data-toggle="tab">Personal Settings</a></li>
                @endif
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane" id="settings">
                    <div class="single-user-form-container">
                        @if(Auth::user()->can('manage.users'))
                        <h3><b>{!! $user->name !!}</b> has the following roles: </h3>
                        <form action="#" method="post" class="form-horizontal">
                            @for ($i = 0; $i < count($all_roles); $i++)
                            <div class="form-group">
                                <label for="role-{!! $all_roles[$i]->id !!}" class="col-sm-2 col-md-2 col-lg-2">
                                    <a target="_blank" href="{{ URL::to('/management/role/'.$all_roles[$i]->id) }}">{!! $all_roles[$i]->name !!}</a>
                                </label>
                                <label class="col-sm-2 col-md-2 col-lg-2">
                                    <input type="checkbox" class="minimal" id="role-{!! $all_roles[$i]->id !!}" name="roles[]" value="{!! $all_roles[$i]->id; !!}" @if(in_array($all_roles[$i]->id, $user_roles)) checked @endif  />
                                </label>
                                <p class="text-muted">{!! $all_roles[$i]->description !!}</p>
                            </div>
                            @endfor
                            <div class="form-row">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                            </div>
                            <div class="form-row">
                                <input type="submit" class="btn btn-success btn-md" value="Save" />
                            </div>
                        </form>
                        @endif
                    </div>
                </div>


                <div class="tab-pane active" id="last-activity">
                    <p>List here the last activity of this user</p>
                </div>

                @if(Auth::user()->id == $user->id || Auth::user()->can('manage.users'))
                <div class="tab-pane" id="personal-settings">
                    <h3>Change your personal preferences</h3>
                    <form action="{{ url('/management/user/'.$user->id.'/settings') }}" method="POST" class="form-horizontal">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Screen Lock Idle Time: </label>
                            </div>
                            <div class="col-md-6">
                                <input min="60" name="lock_iddle_time" class="form-control" value="{{ (isset($settings[0]))?$settings[0]["value"]:"" }}" type="number" />
                            </div>
                            <div class="col-md-2">
                                <small>in seconds <br>
                                    (min 15 seconds)</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn btn-info btn-md" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@stop
