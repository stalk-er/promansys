@extends('master')
@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">{!! $role->name !!}</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <form action="{!! URL::to('/') !!}/management/role/{!! $role->id !!}/edit" method="post">
            @for ($i = 0; $i < count($all_permissions); $i++)
            <div class="form-group">
                <label for="permission-{!! $all_permissions[$i]->id !!}" class="col-sm-2 col-md-2 col-lg-2">{!! $all_permissions[$i]->name !!}</label>
                <label class="col-sm-2 col-md-2 col-lg-2">
                    <input class="minimal" type="checkbox" id="permission-{!! $all_permissions[$i]->id !!}" name="permissions[]" value="{!! $all_permissions[$i]->id; !!}" @if(in_array($all_permissions[$i]->id,$role_permissions)) checked @endif  />
                </label>
                <p class="text-muted">{!! $all_permissions[$i]->description !!}</p>
            </div>
            @endfor
            <br>
            <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
            <input type="submit" class="btn btn-md btn-success pull-right" value="Save" />
        </form>
    </div>
    <!-- /.box-body -->
</div>
@stop
