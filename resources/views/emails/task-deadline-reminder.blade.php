<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Hello, {{ $task->getAssignee()->first()->name }}.</p>

        <p>
            We wanted to let you know that <a href="{{ url('/') . '/task/' . $task->uid }}">task #{{ $task->uid }} {{ $task->subject }}</a> will hit it's deadline in a few days.
        </p>

        <p>
            If this task is already resolved but just not assigned make sure you take care of it.
        </p>
        <br> 
        <p>Thank you for your patience!</p>
        <a href="{{ url('/') }}">Promansys</a>
    </body>
</html>
