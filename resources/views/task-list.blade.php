@extends('master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <h1>{!! $project->name !!}</h1>
        @foreach($tasks as $task)
        <div class="task">

            <div class="col-md-2">{!! $task->subject !!}</div>
            <div class="col-md-2">Status: @foreach($statuses->where('id', $task->status) as $status){!! $status->name !!}@endforeach</div>
            <div class="col-md-2">Priority: @foreach($priorities->where('id', $task->priority) as $priority){!! $priority->name !!}@endforeach</div>
            <div class="col-md-2">Type: {!! $task->type !!}</div>
            <div class="col-md-2">Created by: @foreach($users->where('id', $task->created_by) as $user){!! $user->name !!}@endforeach</div>
            <div class="col-md-2">Assigned to: @foreach($users->where('id', $task->assigned_to) as $asignee){!! $asignee->name !!}@endforeach</div>
            <a href="{!! URL::to('/') !!}/task/{!! $task->id !!}">Details</a>
        </div>
        <hr>
        @endforeach
    </div>
</div>

@stop