@extends('master')
@section('content')

<div class="row">
    <div class="management-container">
        <div class="col-md-12 col-lg-12 col-sm-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    @if(Auth::user()->can('delete.project'))
                    <li class="active">
                        <a href="#manage-projects" data-toggle="tab" aria-expanded="true">Manage Projects</a>
                    </li>
                    @endif
                    @if(Auth::user()->can('manage.roles'))
                    <li class="">
                        <a href="#manage-roles" data-toggle="tab" aria-expanded="false">Manage Roles</a>
                    </li>
                    @endif
                    @if(Auth::user()->can('manage.users'))
                    <li class="">
                        <a href="#manage-users" data-toggle="tab" aria-expanded="false">Manage Users</a>
                    </li>
                    @endif
                    <li class="pull-left header"><i class="fa fa-th"></i> Management panel</li>
                </ul>

                @if(Auth::user()->can('delete.project'))
                <div class="tab-content">
                    <div class="tab-pane active" id="manage-projects">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body  no-padding">
                                <table class="table table-hover data-table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>User</th>
                                            <th>Date Started</th>
                                            <th>Status</th>
                                        </tr>
                                        @foreach($projects as $project)
                                        <tr>
                                            <td>{{ $project->id }}</td>
                                            <td><a href="{!! URL::to('/') !!}/project/{!! $project->id !!}">{{ $project->name }}</a></td>
                                            <td>{{ $project->created_at->format('jS-M-Y') }}</td>
                                            <td><span class="label label-success">Closed</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </div>
                    @endif
                    <!-- /.tab-pane -->

                    @if(Auth::user()->can('manage.roles'))
                    <div class="tab-pane" id="manage-roles">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover data-table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($roles as $role)
                                        <tr>
                                            <td>{{ $role->id }}</td>
                                            <td>
                                                <a href="{!! URL::to('/') !!}/management/role/{!! $role->id !!}">
                                                    {{ $role->name }}
                                                </a>
                                            </td>
                                            <td>{{ $role->description }}</td>
                                            <td>{{ $role->created_at->format('jS-M-Y') }}</td>
                                            <td><button class="btn btn-danger btn-mg">Delete</button></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- /.tab-pane -->

                    @if(Auth::user()->can('manage.users'))
                    <div class="tab-pane" id="manage-users">
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover data-table">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>User</th>
                                            <th>Member Since</th>
                                            <th>Projects Invloved</th>
                                            <th>Role</th>
                                        </tr>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td><a href="{!! URL::to('/') !!}/management/user/{!! $user->id !!}">{{ $user->name }}</a></td>
                                            <td>{{ $user->created_at->format('jS-M-Y') }}</td>
                                            <td>
                                                @if(count($user->projects) > 0)
                                                @foreach($user->projects as $up)
                                                <a href="{{ url('/') }}/project/{{ $up->id }}">{{ $up->name }}</a>
                                                @if($user->projects->last() !== $up)
                                                ,
                                                @endif
                                                @endforeach
                                                @else
                                                <span class="label label-warning">Still not involved</span>
                                                @endif
                                            </td>
                                            <td><span class="label label-success">{{ $user->roles }}</span></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    @endif
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
    </div>
</div>
@stop
