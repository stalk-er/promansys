<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Lockscreen</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition lockscreen">
        <!-- Automatic element centering -->
        <div class="lockscreen-wrapper">
            <div class="lockscreen-logo">
                <a href="../../index2.html"><b>Proman</b>Sys</a>
            </div>
            <!-- User name -->
            <div class="lockscreen-name">{{ Auth::user()->name }}</div>

            <!-- START LOCK SCREEN ITEM -->
            <div class="lockscreen-item">
                <!-- lockscreen image -->
                <div class="lockscreen-image">
                    <img src="{{ Auth::user()->profile_image }}" alt="{{ Auth::user()->name }}">
                </div>
                <!-- /.lockscreen-image -->

                <!-- lockscreen credentials (contains the form) -->

                <form class="lockscreen-credentials" method="post" action="{{ url('/unlock') }}">
                    <input name="_token" type="hidden" value="{{ Session::getToken() }}" />

                    <div class="input-group">
                        <input required="" type="password" name="password" class="form-control" placeholder="password">

                        <div class="input-group-btn">
                            <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                        </div>
                    </div>
                </form>
                <!-- /.lockscreen credentials -->

            </div>
            <!-- /.lockscreen-item -->
            <div class="help-block text-center">
                @if(session('incorrect_password'))
                <p class="text-red">{{ session('incorrect_password') }}</p>
                @endif
                Enter your password to retrieve your session
                To change the lock time visit your profile <a href="{{ url('/management/user/' . Auth::user()->id) }}">personal settings</a> after you unlock your account
            </div>
            <div class="text-center">
                <a href="{{url('/logout') }}">Or sign in as a different user</a>
            </div>
        </div>
        <!-- /.center -->
        
        @if(session('incorrect_password'))
        <!-- jQuery 2.2.3 -->
        <script src="{{ asset('/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
        <!-- jQuery UI -->
        <script src="{{ asset('/dist/js/jquery-ui.js') }}"></script>
        <script>
        $('body').effect('bounce', 'slow');
        </script>
        @endif
    </body>
</html>
