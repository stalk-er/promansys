@extends('master')

@section('content')

<div class="row">
    <div class="all-projects-container">
        <div class="column normal-12">
            <h1>All Projects</h1>
        </div>
        @foreach($all_projects as $project)
            <div class="project">
            <div class="column normal-4">
                <div class="project-row">
                    <a href="{!! URL::to('/') !!}/project/{!! $project->id !!}">{!! $project->name !!}</a>
                </div>
            </div>
            <div class="column normal-4">
                <div class="project-row">{!! $project->slug !!}</div>
            </div>
            <div class="column normal-4">
                <div class="project-row"> 
                    <a href="{!! URL::to('/') !!}/project/{!!$project->id !!}">Details<span class="icon-question-circle"></a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@stop