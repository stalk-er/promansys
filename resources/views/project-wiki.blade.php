@extends('master')
@section('content')
<script type="text/javascript">

    $(document).ready(function () {
        $('.fill-wiki-button').css("cursor", "pointer");
        $(".fill-wiki-data").ckeditor();
        $(".fill-wiki-button").bind("click", function () {
            $(".wiki-page-content").hide();
            $(".wiki-page-form").show();
        });
    });

</script>
<div class="row">
    <div class="col-md-12">
        <h1>Wiki</h1>
        <div>
         
            <div class="wiki-page-content">{!! $wiki->wiki !!}<i class="fa fa-edit" style="font-size: 25px;"></i></div>

            <form method="post" action="{!! URL::to('/') !!}/project-{!! $project->id !!}/wiki" style="display: none;" class="wiki-page-form">
               <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <span class="close"><a href="{!! URL::to('/') !!}/project-{!! $project->id !!}/wiki"> x </a> </span>
                <textarea name="wiki-data" class="fill-wiki-data" style="display: none;">{!! $wiki->wiki !!}</textarea>
                <input type="submit" name="submit-wiki-data" />
            </form>
            
        </div>
    </div>
</div>


@stop