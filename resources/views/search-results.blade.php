@extends('master')

@section('content')

<h1>Search results</h1>
<div class="row">
    @foreach($results as $key=>$value)
    <div class="column normal-12">
        <a href="{!! URL::to('/') !!}/{!! $key !!}">{!! $value !!}</a>
    </div>
        @endforeach
    
</div>
<script type="text/javascript">
    $(window).load(function () {
        $("body *").highlight("{!! $term !!}", "term-highlight");
    });
</script>
@stop