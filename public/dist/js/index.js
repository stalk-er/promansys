$(document).ready(function () {

    var initial;

    $(document).on('click', "#saveDate", saveDate);
    $("#datepicker").on('change', () => {
        if ($(this).val() != initial)
            $("#saveDate").show();
        else
            $("#saveDate").hide();
    });

    // Project deadline date picker, task deadline date picker
    $('#datepicker, #task-deadline').datepicker({
        autoclose: true,
        onLoad: function () {
            initial = $(this).val();
        },
        onSelect: function (dateText) {
            console.log($(this));
            if ($(this).val() != initial)
                $(this).closest(".form-group").find("#saveDate").show();
            else
                $(this).closest(".form-group").find("#saveDate").hide();
        }
    });



    // Saves project deadline on demand
    function saveDate(e) {

        var theDate = $('#datepicker');
        var data = {
            _token: $("meta[name='_token']").attr('content'),
            "deadline": $("#datepicker").val(),
        };
        $.ajax({
            type: 'POST',
            url: window.location.origin + '/project/' + $("#project_id").val() + '/deadline/edit',
            data: data,
            success: () => {
                $("#saveDate").hide();
            }
        });
    }



    // Setting project tickets pie chart
    function setPieData(data, elementId) {
        data = JSON.parse(data);

        $('.chart-responsive').find(`#${elementId}`).remove();
        $('.chart-responsive').append($(`<canvas id="${elementId}" height="150"></canvas>`));

        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 1,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 10, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 70,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            //String - A tooltip template
            tooltipTemplate: "<%=value %> <%=label%>"
        };
        var pieChartCanvas = $(`#${elementId}`).get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        var project_id = Number($("#project_id").val());
        var PieData;
        if (data.closed === 0 && data.feedback === 0 && data.inprogress === 0 && data.resolved === 0 && data.new === 0) {
            PieData = [{
                    value: 100,
                    color: "#acacac",
                    highlight: "#adadad",
                    label: "No tasks"
                }];
            pieChart.Doughnut(PieData, pieOptions);
            return false;
        }


        PieData = [
            {
                value: data.closed,
                color: "#f56954",
                highlight: "#f56955",
                label: "Closed"
            },
            {
                value: data.feedback,
                color: "#00a65a",
                highlight: "#00a65b",
                label: "Feedback"
            },
            {
                value: data.inprogress,
                color: "#f39c12",
                highlight: "#f39c13",
                label: "In progress"
            },
            {
                value: data.resolved,
                color: "#00c0ef",
                highlight: "#00c0ee",
                label: "Resolved"
            },
            {
                value: data.new,
                color: "#0073b7",
                highlight: "#168ace",
                label: "New"
            }
        ];
        console.log(PieData);
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);
    }

    function setTasksCount(data) {
        data = JSON.parse(data);

        for (var key in data)
            $("#task-statistics").find(`#${key}Count`).text(data[key]);

        $("#task-statistics").find('.total').text(Object.keys(data)
                .reduce(function (sum, key) {
                    return sum + parseFloat(data[key]);
                }, 0));
    }

    if ($("#task-statistics").length > 0)
        $.ajax({
            url: window.location.origin + "/project/" + ($("#project_id").val()) + "/tasks",
            type: "get",
            data: {},
            success: function (data) {
                setPieData(data, "tasks_chart");
                setTasksCount(data)
            }
        });

    $(".timeline-item.latest .timeline-item-description").click(function (e) {
        $("#save_description").unbind("click");

        var timeline_description = $(this);
        $(this).hide();
        $(".timeline-item.latest").find("#task_description").show();
        $("#save_description").show();

        $("#save_description").click(function () {
            var text = $(".timeline-item.latest").find("#task_description").val();
            timeline_description.find("div").text(text);
            timeline_description.show();
            $(".timeline-item.latest form input[name='task-description']").val(text);
            $(".timeline-item.latest").find("#task_description").hide();
            $(this).hide();
        });
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    /*
     * Autocomplete search
     */
    jQuery.fn.highlight = function (str, className) {
        var regex = new RegExp(str, "gi");
        return this.each(function () {
            $(this).contents().filter(function () {
                return this.nodeType == 3 && regex.test(this.nodeValue);
            }).replaceWith(function () {
                return (this.nodeValue || "").replace(regex, function (match) {
                    return "<span class=\"" + className + "\">" + match + "</span>";
                });
            });
        });
    };

    $(".q").closest('form').on('submit', function (event) {
        event.preventDefault();
    });
    $(".q").autocomplete({
        type: "GET",
        source: window.location.origin + "/search/autocomplete?term=" + $('#q').val(),
        minLength: 3,
        select: function (event, ui) {
            var q = $('#q').val(ui.item.value);
            window.location = ui.item.url;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var $a = $("<a></a>").text(item.label);
        if (item.value) {
            return $("<li>")
                    .data("ui-autocomplete-item", item)
                    .append(
                            $("<li>").append($a).append(
                            $("<span>").text(" | ")
                            .append(
                                    $("<strong>").text(item.type)
                                    )
                            )
                            ).appendTo(ul);
        }
    };

    // Add member to project
    function addMemberToProject(e, dialog) {

        var btn = $(e.target);
        var userToAdd = dialog.find('select').val();
        var _token = $('meta[name="_token"]').attr('content');
        var data = {
            _token: _token
        };
        data['user-id'] = userToAdd;


        $.ajax({
            type: 'POST',
            url: window.location.origin + '/project/' + $('#project_id').val() + '/new-member',
            data: data,
            success: (user) => {
                dialog.modal('hide');
                if (typeof user == "string")
                {
                    push_notification('The selected member is already a part of this project', 'warning');
                    return false;
                }

                var li =
                        `
          <li class="list-group-item">
          <b>${user.name}</b>
          <button class="btn btn-danger btn-sm removeProjectMember" data-member="${user.id}">Remove</button>
          <a class="pull-right">${user.roles}</a>
          </li>
          `;
                $('#projectMembers').append(li);
                dialog.find('#addMember').unbind('click');

                push_notification('The selected member was successfully added to this project.', 'success');
            }
        });
    }

    // Add Task to the Project
    $(document).on('click', '#addTask', () => {
        var dialog = $('#new_task');
        dialog.modal('show');
        dialog.find('#newTaskForm').unbind('submit').on('submit', addTaskToProject);
    });

    // Add task to project
    function addTaskToProject(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: window.location.origin + '/project/' + $('#project_id').val() + '/new-task',
            data: formData,
            headers: {'X-CSRF-TOKEN': $("meta[name='_token']").attr("content")},
            cache: false,
            processData: false,
            contentType: false,
            error: (error) => {
                push_notification('Make sure the files upload does not exceeds the available file size limit', 'danger');
            },
            success: (task) => {
                task = JSON.parse(task);

                var tr =
                        `
          <tr>
          <td>${task.uid}</td>
          <td><a href="${window.location.origin + '/task/' + task.uid}">${task.subject}</a></td>
          <td>${task.status}</td>
          <td><span class="label label-label label-${task.status_class}">${task.priority}</span></td>
          <td>${task.type}</td>
          <td>${task.created_by}</td>
          <td>${task.created_at}</td>
          <td>${task.updated_at}</td>
          </tr>
          `;

                $('.modal').modal('hide');
                push_notification('You successfully created a task', 'success');

                // Render chart data again
                $.ajax({
                    url: window.location.origin + "/project/" + $("#project_id").val() + "/tasks",
                    type: "get",
                    data: {},
                    success: function (data) {
                        setPieData(data, "tasks_chart");
                        setTasksCount(data)
                    }
                });

                // Append the newly created task only if you are on the first page
                var paged = window.location.search.match(/([\d]+)/gm);
                if (paged && Number(paged[0]) > 1)
                    return false;

                $('.tasks-table tbody').prepend(tr);
            }
        });
    }

    // Add Project Member
    $(document).on('click', '#addProjectMember', () => {

        var dialog = $('#memberToProjectDialog');
        dialog.modal('show');

        dialog.find('#addMember').on('click', (e) => {
            addMemberToProject(e, dialog);
        });
    });

    // Add Project Member
    $(document).on('click', '.removeProjectMember', (e) => {

        var dialog = $('#confirmation');
        dialog.find('#yes').unbind('click');
        var memberToRemove = $(e.target).attr('data-member');
        dialog.modal('show');
        dialog.find('#yes').on('click', () => {
            removeMember(dialog, memberToRemove);
        });
    });

    // Remove Member
    function removeMember(dialog, memberToRemove) {

        var _token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: window.location.origin + '/project/' + $('#project_id').val() + '/remove-member',
            data: {
                _token: _token,
                "user-id": memberToRemove
            },
            success: () => {
                dialog.modal('hide');
                dialog.find('#yes').unbind('click');
                $('#projectMembers').find(`*[data-member="${memberToRemove}"]`).closest('li').fadeOut(() => {
                    $(this).remove();
                });
                push_notification('The selected member was successfully removed from this project', 'info');
            }
        });

    }


    // Initialize textareas
    $(".textarea").wysihtml5();
    $("#search-projects").select2({
        closeOnSelect: true,
        openOnEnter: true
    });

    $(".search-projects").on("click", function (e) {
        $("#search-projects").select2("open");
    });
    $("#search-projects").on("select2:select", function (data, options) {
        window.location = $(this).val();
    });

    // Edit wiki
    (function () {

        $("#edit-wiki").on("click", function () {
            $(".wiki-page-content").hide();
            $(".wiki-textarea-area").show();
            $("#save-wiki").show();
        });

        $("#wiki #save-wiki").on("click", function () {

            var html = $(".wiki-textarea[name='wiki']")[0].value;
            $(".wiki-page-content div:first-of-type").html(html);

            var projectId = $("#project_id").val();
            $.ajax({
                url: window.location.origin + "/project/" + projectId + "/wiki",
                type: "post",
                data: {
                    _token: $("meta[name='_token']").attr("content"),
                    data: JSON.stringify(html)
                },
                success: function () {
                    $(".wiki-page-content").show();
                    $("#wiki").find("#save-wiki").show();
                    $(".wiki-textarea-area").hide();
                    $("#wiki").find("#save-wiki").hide();
                }
            });
        });

    })();

    var handleFileSelect = function (evt, el) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

            reader.onload = function (readerEvt) {
                var binaryString = readerEvt.target.result;

                var src = "data:application/octet-stream;base64," + btoa(binaryString);
                var data = {
                    _token: $("meta[name='_token']").attr("content"),
                    profile_image: src
                };
                $.ajax({
                    url: window.location.origin + "/user/" + $("#user-id").val() + "/details",
                    data: {
                        _token: $("meta[name='_token']").attr("content"),
                        data: data
                    },
                    method: "POST",
                    success: function (r) {
                        $(".profile-user-img").attr("src", src);
                    }
                });
            };

            reader.readAsBinaryString(file);
        }
    };

    // Edit user information
    (function () {

        // Email
        $("#edit-email").on("click", function (e) {
            e.preventDefault();
            $("#user-email-input").show();
            $("#user-email").hide();
        });

        var emailValidationPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        $("#user-email-input").find("#save").on("click", function (e) {
            e.preventDefault();

            if (!$("#user-email-input").find("input[type='email']").val().match(emailValidationPattern)) {
                $("#user-email-input").find("input[type='email']").focus();
                return false;
            }

            var data = {
                email: $("#user-email-input").find("input[type='email']").val()
            };
            $.ajax({
                url: window.location.origin + "/user/" + $("#user-id").val() + "/details",
                data: {
                    _token: $("meta[name='_token']").attr("content"),
                    data: data
                },
                method: "POST",
                success: function (r) {
                    $("#user-email").text(
                            $("#user-email-input").find("input[type='email']").val()
                            );
                }
            });

            $("#user-email-input").hide();
            $("#user-email").show();
        });

        // Description
        $("#edit-description").on("click", function (e) {
            e.preventDefault();
            $("#user-description").hide();
            $("#user-description-textarea").show();
        });

        $("#user-description-textarea").find("#save").on("click", function (e) {
            e.preventDefault();

            var data = {
                _token: $("meta[name='_token']").attr("content"),
                description: $("#user-description-textarea").find(".textarea")[0].value
            };

            $.ajax({
                url: window.location.origin + "/user/" + $("#user-id").val() + "/details",
                data: {
                    _token: $("meta[name='_token']").attr("content"),
                    data: data
                },
                method: "POST",
                success: function (r) {
                    $("#user-description").empty();
                    $("#user-description").html(data.description)
                }
            });

            $("#user-description").show();
            $("#user-description-textarea").hide();
        });

        $("#edit-image").on("click", function (e) {
            e.preventDefault();
            $("#user-image-input").trigger("click");
        });

        $("#change-profile-image").on("click", function (e) {
            e.preventDefault();
            $("#user-image-input").trigger("click");
        });

        $("input[type='file']#user-image-input").change(function (e) {
            handleFileSelect(e, $(this));
        });

    })();

    $(function () {
        // Javascript to enable link to tab
        var hash = document.location.hash;
        if (hash) {

            if ($('.nav-tabs a[href="' + hash + '"]').length > 0)
                $('.nav-tabs a[href="' + hash + '"]').tab('show');

            if ($("button[data-target='" + hash + "']").length > 0)
                $("button[data-target='" + hash + "']").click();
        }

        // Change hash for page-reload
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        });

        $(".treeview-menu a").on("click", function () {
            if ($(this).attr("href").match(/(\#)/g)) {
                console.log(123)
                location.href = $(this).attr("href");
                location.reload();
            }
        })
    });
});


// Initialize data tables
$(function () {
    $(".data-table").DataTable({
        paging: false,
        searching: false,
        info: false,
        ordering: true,
        //autoWidth: true,

    });
});


function push_notification(message, type, timeStay) {

    // Available types: danger, info, warning, success
    var notificationTimeBeing = (timeStay) ? timeStay : 3 * 1000;
    var notification = "";
    notification += '<div class="alert alert-' + type + ' alert-dismissible col-md-4">';
    notification += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
    notification += '<h4><i class="icon fa fa-ban"></i> Alert!</h4>';
    notification += message;
    notification += '</div>';
    notification = $(notification);


    $('body').prepend(notification);

    window.setTimeout(function () {
        notification.fadeOut('slow', function () {
            $(this).remove();
        });
    }, notificationTimeBeing);
}




function getInactivityTime() {
    var userId = $("meta#userId").attr("content");
    var url = window.location.origin + "/management/user/" + userId + "/settings/json/";
    console.log(url);
    return $.ajax({
        type: "get",
        url: url
    });
}

$(document).ready(function () {
    /**
     * flashMessage is a global variable set throught the Blade views
     * If the variable is not Null or Undefined, we show the flash message
     * once the document is ready
     * */
    if (typeof flashMessage !== 'undefined' && flashMessage) {
        push_notification(flashMessage.message, flashMessage.type);
    }

    $('body').hide().fadeIn();


    // Screen lock functionality
    var timeout = null;
    var inactivityTimeout;
    getInactivityTime().done(function (settings) {
        for (var s in settings) {
            if (settings[s].key === "lock_iddle_time") {
                inactivityTimeout = settings[s].value;
            }
        }

        if (!inactivityTimeout) {
            var inactivityTimeNotSet = sessionStorage.getItem('inactivityTimeNotSet');
            if (!inactivityTimeNotSet) {
                push_notification("The inactivity time is still not defined. Go to your <a href='" + window.location.origin + "/management/user/" + $("#userId").attr("content") + "'>personal settings</a>", "warning", 5000);
                sessionStorage.setItem('inactivityTimeNotSet', true);
            }
        } else {
            sessionStorage.deleteItem('inactivityTimeNotSet');
        }
    }).error(function () {

        var inactivityTimeNotSet = sessionStorage.getItem('inactivityTimeNotSet');
        if (!inactivityTimeNotSet) {
            push_notification("The inactivity time is still not defined. Go to your <a href='" + window.location.origin + "/management/user/" + $("#userId").attr("content") + "'>personal settings</a>", "warning", 5000);
            sessionStorage.setItem('inactivityTimeNotSet', true);
        }
    });

    function screenLock() {

        if (timeout !== null) {
            clearTimeout(timeout);
        }
        console.log(inactivityTimeout);
        if (inactivityTimeout)
            timeout = setTimeout(function () {
                window.location = window.location.origin + "/locked";
            }, inactivityTimeout * 1000);
    }

    // Lock the screen after certain amount of time
    setTimeout(function () {
        $(document).on('mousemove', screenLock);
    }, 300);
});

$(window).bind('popstate', function () {
// Do something, inspect History.getState() to decide what
    console.log(window.history);
    return false;
});

